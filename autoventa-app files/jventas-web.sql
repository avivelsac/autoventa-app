/*
Navicat MySQL Data Transfer

Source Server         : ThinkPad
Source Server Version : 50642
Source Host           : localhost:3306
Source Database       : jventas-web

Target Server Type    : MYSQL
Target Server Version : 50642
File Encoding         : 65001

Date: 2019-04-27 08:13:30
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for groups
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(50) NOT NULL,
  `USERNAME` varchar(45) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_GROUPS_PERSON` (`USERNAME`),
  CONSTRAINT `FK_GROUPS_PERSON` FOREIGN KEY (`USERNAME`) REFERENCES `person` (`USERNAME`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of groups
-- ----------------------------
INSERT INTO `groups` VALUES ('1', 'admins', 'andre');
INSERT INTO `groups` VALUES ('2', 'users', 'andre');
INSERT INTO `groups` VALUES ('3', 'users', 'edison');

-- ----------------------------
-- Table structure for person
-- ----------------------------
DROP TABLE IF EXISTS `person`;
CREATE TABLE `person` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FIRSTNAME` varchar(50) NOT NULL,
  `LASTNAME` varchar(100) NOT NULL,
  `USERNAME` varchar(45) NOT NULL,
  `PASSWORD` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UQ_PERSON_USERNAME` (`USERNAME`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of person
-- ----------------------------
INSERT INTO `person` VALUES ('1', 'André', 'Bedregal', 'andre', SHA2('avivel', 256));
INSERT INTO `person` VALUES ('2', 'Edison', 'Chumpitaz', 'edison', SHA2('avivel', 256));
