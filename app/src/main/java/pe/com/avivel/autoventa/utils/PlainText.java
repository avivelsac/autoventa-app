package pe.com.avivel.autoventa.utils;

public class PlainText {

    public static String insert(String caracter, int numeroOcurrencias) {
        String espacios = "";
        for (int i = 0; i < numeroOcurrencias; i++) {
            espacios += caracter;
        }
        return espacios;
    }

    public static String alinearIzquierda(String cadena, int numeroCaracteres, String caracter) {
        if (cadena.length() > numeroCaracteres) {
            return cadena.substring(0, numeroCaracteres);
        } else {
            return cadena + insert(caracter, numeroCaracteres - cadena.length());
        }
    }

    public static String alinearIzquierda(String cadena, int numeroCaracteres) {
        return alinearIzquierda(cadena, numeroCaracteres, " ");
    }

    public static String alinearDerecha(String cadena, int numeroCaracteres, String caracter) {
        if (cadena.length() > numeroCaracteres) {
            return insert("#", numeroCaracteres);
        } else {
            return insert(caracter, numeroCaracteres - cadena.length()) + cadena;
        }
    }

    public static String alinearDerecha(String cadena, int numeroCaracteres) {
        return alinearDerecha(cadena, numeroCaracteres, " ");
    }

    public static String centrar(String cadena, int numeroCaracteres) {
        String espaciosBlanco = PlainText.insert(" ", (numeroCaracteres - cadena.length()) / 2);
        return espaciosBlanco + cadena + PlainText.insert(" ", numeroCaracteres - espaciosBlanco.length() - cadena.length());
    }

}
