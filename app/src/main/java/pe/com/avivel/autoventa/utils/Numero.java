package pe.com.avivel.autoventa.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class Numero {

    private static final DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
    private static final DecimalFormat decimalFormat = new DecimalFormat("###,##0");

    private static final DecimalFormat[] DECIMAL_FORMATS = new DecimalFormat[]{new DecimalFormat("###,##0.0"), new DecimalFormat("###,##0.00"), new DecimalFormat("###,##0.000"), new DecimalFormat("###,##0.0000"), new DecimalFormat("###,##0.00000"), new DecimalFormat("###,##0.000000")};

    static {
        decimalFormatSymbols.setDecimalSeparator('.');
        decimalFormatSymbols.setGroupingSeparator(',');
        decimalFormat.setDecimalFormatSymbols(decimalFormatSymbols);
        decimalFormat.setRoundingMode(RoundingMode.HALF_UP);

        for (DecimalFormat df : DECIMAL_FORMATS) {
            df.setDecimalFormatSymbols(decimalFormatSymbols);
            df.setRoundingMode(RoundingMode.HALF_UP);
        }
    }

    /**
     * Representación de un BigDecimal con formato
     *
     * @param bd        Número a formatear
     * @param decimales Número de decimales
     * @return String con el número formateado
     */
    public static String formatear(BigDecimal bd, int decimales) {
        if (decimales != 0) {
            return DECIMAL_FORMATS[decimales - 1].format(bd);
        } else {
            return DECIMAL_FORMATS[1].format(bd);
        }
    }

    /**
     * Representación de un BigDecimal con formato a 2 decimales
     *
     * @param bd Número a formatear
     * @return String con el número formateado
     */
    public static String formatear(BigDecimal bd) {
        return formatear(bd, 2);
    }

    /**
     * Redondea un número usando la clase BigDecimal.
     *
     * @param numero El número decimal que se quiere redondear
     * @param escala La escala (el número de decimales)
     * @return El número redondeado en tipo Double
     */
    public static double redondear(String numero, int escala) {
        BigDecimal bigDecimal = new BigDecimal(numero);
        return bigDecimal.setScale(escala, RoundingMode.HALF_UP).doubleValue();
    }

}

