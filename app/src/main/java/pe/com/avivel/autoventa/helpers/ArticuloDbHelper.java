package pe.com.avivel.autoventa.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import pe.com.avivel.autoventa.exception.DbHelperException;
import pe.com.avivel.autoventa.model.Articulo;
import pe.com.avivel.autoventa.model.UnidadMedida;

public class ArticuloDbHelper extends FacturacionHelper {

    final static String TABLE_NAME = "articulo";
    final static String COLUMN_NAME_IDXXXX = "articulo_id";
    final static String COLUMN_NAME_CODIGO = "articulo_codigo";
    final static String COLUMN_NAME_DESCRI = "articulo_descripcion";
    final static String COLUMN_NAME_UNIMED = "articulo_unidadmedida";
    private static ArticuloDbHelper instance;

    private ArticuloDbHelper(Context context) {
        super(context);
    }

    public static synchronized ArticuloDbHelper getInstance(Context context) {
        if (instance == null) {
            instance = new ArticuloDbHelper(context);
        }
        return instance;
    }

    /**
     * Crea tabla articulo
     *
     * @param db base de datos
     */
    static void createTable(SQLiteDatabase db) {
        String query = "create table " + TABLE_NAME + "( " +
                COLUMN_NAME_IDXXXX + " integer primary key AUTOINCREMENT, " +
                COLUMN_NAME_CODIGO + " text null, " +
                COLUMN_NAME_DESCRI + " text null, " +
                COLUMN_NAME_UNIMED + " text null " +
                ")";
        db.execSQL(query);
    }

    static void dropTable(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    }

    public long insert(Articulo articulo) throws DbHelperException {
        long insert;
        SQLiteDatabase db = null;
        ContentValues values;

        try {
            db = getWritableDatabase();
            values = new ContentValues();

            values.put(COLUMN_NAME_CODIGO, articulo.getCodigo());
            values.put(COLUMN_NAME_DESCRI, articulo.getDescripcion());
            values.put(COLUMN_NAME_UNIMED, String.valueOf(articulo.getUnidadMedida()));

            insert = db.insert(TABLE_NAME, null, values);
        } catch (Exception ex) {
            Log.e("DATABASE", ex.getLocalizedMessage(), ex);
            throw new DbHelperException(ex.getLocalizedMessage(), ex);
        } finally {
            if (db != null && db.isOpen()) {
                db.close();
            }
        }
        return insert;
    }

    public long updateArticulo(Articulo articulo) throws DbHelperException {
        long update;
        SQLiteDatabase db = null;
        ContentValues values;

        try {
            db = getWritableDatabase();
            values = new ContentValues();

            values.put(COLUMN_NAME_CODIGO, articulo.getCodigo());
            values.put(COLUMN_NAME_DESCRI, articulo.getDescripcion());
            values.put(COLUMN_NAME_UNIMED, String.valueOf(articulo.getUnidadMedida()));

            update = db.update(TABLE_NAME, values, "articulo_id = ?", new String[]{String.valueOf(articulo.getId())});
        } catch (Exception ex) {
            Log.e("DATABASE", ex.getLocalizedMessage(), ex);
            throw new DbHelperException(ex.getLocalizedMessage(), ex);
        } finally {
            if (db != null && db.isOpen()) {
                db.close();
            }
        }
        return update;
    }

    public int deleteById(long id) throws DbHelperException {
        int delete;
        SQLiteDatabase db = null;
        try {
            db = getWritableDatabase();
            delete = db.delete(TABLE_NAME, "articulo_id = ?", new String[]{String.valueOf(id)});
        } catch (Exception ex) {
            Log.e("DATABASE", ex.getLocalizedMessage(), ex);
            throw new DbHelperException(ex.getLocalizedMessage(), ex);
        } finally {
            if (db != null && db.isOpen()) {
                db.close();
            }
        }
        return delete;
    }

    private Articulo getArticulo(String where, String[] params) throws DbHelperException {
        Articulo articulo = null;
        SQLiteDatabase db;
        Cursor mCursor = null;

        try {
            db = this.getWritableDatabase();
            String[] cols = new String[]{COLUMN_NAME_IDXXXX, COLUMN_NAME_CODIGO, COLUMN_NAME_DESCRI, COLUMN_NAME_UNIMED};
            mCursor = db.query(TABLE_NAME, cols, where, params, null, null, null, null);

            if (mCursor != null && mCursor.getCount() > 0) {
                mCursor.moveToFirst();
                articulo = new Articulo();
                articulo.setId(mCursor.getInt(mCursor.getColumnIndex(COLUMN_NAME_IDXXXX)));
                articulo.setCodigo(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_CODIGO)));
                articulo.setDescripcion(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_DESCRI)));
                articulo.setUnidadMedida(UnidadMedida.valueOf(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_UNIMED))));
            }
        } catch (Exception ex) {
            Log.e("DATABASE", ex.getLocalizedMessage(), ex);
            throw new DbHelperException(ex.getLocalizedMessage(), ex);
        } finally {
            if (mCursor != null) {
                mCursor.close();
            }
        }
        return articulo;
    }

    public Articulo getArticuloById(long id) throws DbHelperException {
        return getArticulo("articulo_id=?", new String[]{String.valueOf(id)});
    }

    public Articulo getArticuloByCodigo(String codigo) throws DbHelperException {
        return getArticulo("articulo_codigo=?", new String[]{codigo});
    }

    public List<Articulo> getAll() throws DbHelperException {
        Articulo articulo;
        List<Articulo> list = new ArrayList<>();
        SQLiteDatabase db;
        Cursor mCursor = null;
        try {
            db = this.getWritableDatabase();
            String[] cols = new String[]{COLUMN_NAME_IDXXXX, COLUMN_NAME_CODIGO, COLUMN_NAME_DESCRI, COLUMN_NAME_UNIMED};
            mCursor = db.query(true, TABLE_NAME, cols, null, null, null, null, null, null);
            mCursor.moveToFirst();
            list = new ArrayList<>();
            while (!mCursor.isAfterLast()) {
                articulo = new Articulo();
                articulo.setId(mCursor.getInt(mCursor.getColumnIndex(COLUMN_NAME_IDXXXX)));
                articulo.setCodigo(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_CODIGO)));
                articulo.setDescripcion(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_DESCRI)));
                articulo.setUnidadMedida(UnidadMedida.valueOf(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_UNIMED))));
                list.add(articulo);
                mCursor.moveToNext();
            }
        } catch (Exception ex) {
            Log.e("DATABASE", ex.getLocalizedMessage(), ex);
            throw new DbHelperException(ex.getLocalizedMessage(), ex);
        } finally {
            if (mCursor != null) {
                mCursor.close();
            }
        }
        return list;
    }

}
