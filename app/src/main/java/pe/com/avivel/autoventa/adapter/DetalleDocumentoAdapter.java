package pe.com.avivel.autoventa.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import pe.com.avivel.autoventa.R;
import pe.com.avivel.autoventa.activity.DocumentoActivity;
import pe.com.avivel.autoventa.model.DetalleDocumento;
import pe.com.avivel.autoventa.model.Moneda;
import pe.com.avivel.autoventa.utils.Numero;

public class DetalleDocumentoAdapter extends ArrayAdapter<DetalleDocumento> {

    private final StringBuilder stringBuilder = new StringBuilder();
    private Button btnEliminarItem;
    private Context mContext;

    public DetalleDocumentoAdapter(Context context, ArrayList<DetalleDocumento> detalleDocumentoArrayList) {
        super(context, 0, detalleDocumentoArrayList);
        this.mContext = context;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        final DetalleDocumento detalleDocumento = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_documento_detalle, parent, false);
        }

        btnEliminarItem = convertView.findViewById(R.id.btnEliminarItem);
        btnEliminarItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder adb = new AlertDialog.Builder(getContext());
                adb.setTitle("Atención");
                adb.setMessage("¿Está seguro que desea borrar el ítem seleccionado?");
                adb.setNegativeButton("Cancelar", null);
                adb.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        remove(detalleDocumento);
                        notifyDataSetChanged();
                        ((DocumentoActivity) mContext).actualizarTotales();
                    }
                });
                adb.show();
            }
        });

        //descripcion
        TextView tvItemDetDocDescripcion = convertView.findViewById(R.id.tvItemResDiaNumero);
        stringBuilder.setLength(0);
        stringBuilder.append(detalleDocumento.getArticulo().getDescripcion());
        tvItemDetDocDescripcion.setText(stringBuilder.toString());

        // cantidad x precio
        TextView tvItemDetDocCantidad = convertView.findViewById(R.id.tvItemDetDocCantidad);
        stringBuilder.setLength(0);
        stringBuilder.append(Numero.formatear(detalleDocumento.getCantidad()));
        stringBuilder.append(" ");
        stringBuilder.append(detalleDocumento.getArticulo().getUnidadMedida().name());
        stringBuilder.append(" x ");
        stringBuilder.append(Moneda.MN.getSimbolo());
        stringBuilder.append(Numero.formatear(detalleDocumento.getPrecio()));
        tvItemDetDocCantidad.setText(stringBuilder.toString());

        // importe
        TextView tvItemDetDocImporte = convertView.findViewById(R.id.tvItemDetDocImporte);
        stringBuilder.setLength(0);
        stringBuilder.append(Moneda.MN.getSimbolo());
        stringBuilder.append(Numero.formatear(detalleDocumento.getImporte()));
        tvItemDetDocImporte.setText(stringBuilder.toString());
        return convertView;
    }

}
