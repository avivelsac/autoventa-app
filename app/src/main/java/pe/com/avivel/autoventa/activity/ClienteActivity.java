package pe.com.avivel.autoventa.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import pe.com.avivel.autoventa.R;
import pe.com.avivel.autoventa.helpers.ClienteDbHelper;
import pe.com.avivel.autoventa.helpers.UbigeoDbHelper;
import pe.com.avivel.autoventa.model.Cliente;
import pe.com.avivel.autoventa.model.TipoDocumentoIdentidad;


public class ClienteActivity extends AppCompatActivity {

    Context mContext;
    ClienteDbHelper clienteDbHelper;
    UbigeoDbHelper ubigeoDbHelper;

    Spinner spinnerTipoDocumento;
    EditText txtNumeroDocumento, txtRazonSocial, txtDepartamento, txtProvincia, txtDistrito, txtDireccion, txtCorreo;
    ImageButton btnMapa, btnSave, btnDelete;

    ArrayAdapter<TipoDocumentoIdentidad> tipoDocumentoIdentidadAdapter;

    long id = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cliente);

        mContext = this;

        //data
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            id = bundle.getLong("idCliente");
        }

        //pe.com.avivel.autoventa.helpers
        clienteDbHelper = ClienteDbHelper.getInstance(this);
        ubigeoDbHelper = UbigeoDbHelper.getInstance(this);

        //botones
        btnMapa = findViewById(R.id.btnMapa);
        btnSave = findViewById(R.id.btnSave);
        btnDelete = findViewById(R.id.btnDelete);

        //campos
        spinnerTipoDocumento = findViewById(R.id.spinnerTipoDocumento);
        txtNumeroDocumento = findViewById(R.id.txtNumeroDocumento);
        txtRazonSocial = findViewById(R.id.txtRazonSocial);
        txtDepartamento = findViewById(R.id.txtDepartamento);
        txtProvincia = findViewById(R.id.txtProvincia);
        txtDistrito = findViewById(R.id.txtDistrito);
        txtDireccion = findViewById(R.id.txtDireccionCliente);
        txtCorreo = findViewById(R.id.txtCorreoEmpresa);

        // Input en mayúsculas
        txtRazonSocial.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        txtDepartamento.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        txtProvincia.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        txtDistrito.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        txtDireccion.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

        tipoDocumentoIdentidadAdapter = new ArrayAdapter<>(this, R.layout.spinner_layout);
        tipoDocumentoIdentidadAdapter.setDropDownViewResource(R.layout.spinner_layout);
        spinnerTipoDocumento.setAdapter(tipoDocumentoIdentidadAdapter);

        tipoDocumentoIdentidadAdapter.addAll(TipoDocumentoIdentidad.values());

        //listeners
        btnMapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putLong("idCliente", id);
                Intent clie = new Intent(mContext, MapClienteActivity.class);
                clie.putExtras(bundle);
                startActivity(clie);
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validar()) {
                    Cliente cliente = new Cliente();
                    cliente.setId((int) id);
                    cliente.setTipoDocumentoIdentidad((TipoDocumentoIdentidad) spinnerTipoDocumento.getSelectedItem());
                    cliente.setNumeroDocumento(txtNumeroDocumento.getText().toString());
                    cliente.setRazonSocial(txtRazonSocial.getText().toString());
                    cliente.setDepartamento(txtDepartamento.getText().toString());
                    cliente.setProvincia(txtProvincia.getText().toString());
                    cliente.setDistrito(txtDistrito.getText().toString());
                    cliente.setDireccion(txtDireccion.getText().toString());
                    cliente.setCorreo(txtCorreo.getText().toString());

                    if (id > -1) {
                        long update;
                        try {
                            update = clienteDbHelper.updateCliente(cliente);
                            if (update > 0) {
                                Toast.makeText(mContext, "Datos guardados existosamente.", Toast.LENGTH_LONG).show();
                                Intent returnIntent = new Intent();
                                setResult(Activity.RESULT_OK, returnIntent);
                                finish();
                            } else {
                                Toast.makeText(mContext, "Se produjo un error al guardar los datos.", Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            Toast.makeText(mContext, "Se produjo un error al guardar los datos.", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        long insert;
                        try {
                            insert = clienteDbHelper.insertCliente(cliente);
                            if (insert > 0) {
                                Toast.makeText(mContext, "Datos guardados existosamente.", Toast.LENGTH_LONG).show();
                                Intent returnIntent = new Intent();
                                setResult(Activity.RESULT_OK, returnIntent);
                                finish();
                            } else {
                                Toast.makeText(mContext, "Se produjo un error al guardar los datos.", Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            Toast.makeText(mContext, "Se produjo un error al guardar los datos.", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(ClienteActivity.this);
                alertDialog.setTitle("Clientes");
                alertDialog.setMessage("¿Desea eliminar el cliente?");
                alertDialog.setIcon(android.R.drawable.ic_delete);
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            int delete = clienteDbHelper.deleteById(id);
                            if (delete > 0) {
                                Toast.makeText(getApplicationContext(), "Cliente eliminado existosamente", Toast.LENGTH_SHORT).show();
                                Intent returnIntent = new Intent();
                                setResult(Activity.RESULT_OK, returnIntent);
                                finish();
                            } else {
                                Toast.makeText(mContext, "Se produjo un error al borrar los datos.", Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            Toast.makeText(mContext, "Se produjo un error al borrar los datos.", Toast.LENGTH_LONG).show();
                        }
                    }
                });
                alertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        });

        //propiedades
        if (id > -1) {
            btnMapa.setEnabled(true);
            btnDelete.setEnabled(true);
        } else {
            btnMapa.setEnabled(false);
            btnDelete.setEnabled(false);
        }

        getData();
    }

    private boolean validar() {
        boolean valido = false;
        try {
            TipoDocumentoIdentidad tipoDoc = (TipoDocumentoIdentidad) spinnerTipoDocumento.getSelectedItem();
            if (txtNumeroDocumento.getText().length() == 0) {
                Toast.makeText(mContext, "Debe indicar el número de documento.", Toast.LENGTH_LONG).show();
                txtNumeroDocumento.requestFocus();
            } else if (txtNumeroDocumento.getText().length() != 8 && tipoDoc == TipoDocumentoIdentidad.DNI) {
                Toast.makeText(mContext, "El DNI debe tener 8 dígitos.", Toast.LENGTH_LONG).show();
                txtNumeroDocumento.requestFocus();
            } else if (txtNumeroDocumento.getText().length() != 11 && tipoDoc == TipoDocumentoIdentidad.RUC) {
                Toast.makeText(mContext, "El RUC debe tener 11 dígitos", Toast.LENGTH_LONG).show();
                txtNumeroDocumento.requestFocus();
            } else if (clienteDbHelper.getClienteByNumeroDocumento(txtNumeroDocumento.getText().toString()) != null) {
                Toast.makeText(mContext, "Ya existe un cliente con este número de documento.", Toast.LENGTH_LONG).show();
                txtNumeroDocumento.requestFocus();
            } else if (txtRazonSocial.getText().length() == 0) {
                Toast.makeText(mContext, "Debe indicar el nombre o razón social.", Toast.LENGTH_LONG).show();
                txtRazonSocial.requestFocus();
            } else {
                valido = true;
            }
        } catch (Exception ex) {
            Toast.makeText(mContext, "Se produjo un error al consultar los datos.", Toast.LENGTH_LONG).show();
        }
        return valido;
    }

    private void getData() {
        Cliente cliente;
        try {
            cliente = clienteDbHelper.getClienteById(id);
            if (cliente != null) {
                id = cliente.getId();
                spinnerTipoDocumento.setSelection(tipoDocumentoIdentidadAdapter.getPosition(cliente.getTipoDocumentoIdentidad()));
                txtNumeroDocumento.setText(cliente.getNumeroDocumento());
                txtRazonSocial.setText(cliente.getRazonSocial());
                txtDepartamento.setText(cliente.getDepartamento());
                txtProvincia.setText(cliente.getProvincia());
                txtDistrito.setText(cliente.getDistrito());
                txtDireccion.setText(cliente.getDireccion());
                txtCorreo.setText(cliente.getCorreo());
            }
        } catch (Exception e) {
            Toast.makeText(mContext, "Se produjo un error al consultar los datos.", Toast.LENGTH_LONG).show();
        }
    }
}
