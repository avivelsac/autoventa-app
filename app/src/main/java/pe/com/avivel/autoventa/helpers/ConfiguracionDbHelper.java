package pe.com.avivel.autoventa.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import pe.com.avivel.autoventa.model.Configuracion;

public class ConfiguracionDbHelper extends FacturacionHelper {

    private static ConfiguracionDbHelper instance;

    private static final String TABLE_NAME = "configuracion";
    private static final String COLUMN_NAME_IDXXXX = "configuracion_id";
    private static final String COLUMN_NAME_FECTRA = "configuracion_fechatrabajo";
    private static final String COLUMN_NAME_URLSER = "configuracion_urlservidor";
    private static final String COLUMN_NAME_SERBOL = "configuracion_serieboleta";
    private static final String COLUMN_NAME_SERFAC = "configuracion_seriefactura";

    private ConfiguracionDbHelper(Context context) {
        super(context);
    }

    public static ConfiguracionDbHelper getInstance(Context context) {
        if (instance == null) {
            instance = new ConfiguracionDbHelper(context);
        }
        return instance;
    }

    static void createTable(SQLiteDatabase db) {
        String query = "create table " + TABLE_NAME + "( " +
                COLUMN_NAME_IDXXXX + " integer primary key AUTOINCREMENT, " +
                COLUMN_NAME_FECTRA + " text null, " +
                COLUMN_NAME_URLSER + " text null, " +
                COLUMN_NAME_SERBOL + " text null, " +
                COLUMN_NAME_SERFAC + " text null " +
                ")";
        db.execSQL(query);
    }

    static void dropTable(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    }

    public Configuracion getConfiguracion() {
        Configuracion configuracion = null;
        SQLiteDatabase db = this.getWritableDatabase();
        String[] cols = new String[]{COLUMN_NAME_IDXXXX, COLUMN_NAME_FECTRA, COLUMN_NAME_URLSER, COLUMN_NAME_SERBOL, COLUMN_NAME_SERFAC};
        Cursor mCursor = db.query(true, TABLE_NAME, cols, null, null, null, null, null, null);
        if (mCursor != null && mCursor.getCount() > 0) {
            mCursor.moveToFirst();
            configuracion = new Configuracion();
            configuracion.setId(mCursor.getInt(mCursor.getColumnIndex(COLUMN_NAME_IDXXXX)));
            configuracion.setFechaTrabajo(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_FECTRA)));
            configuracion.setUrlServidor(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_URLSER)));
            configuracion.setSerieBoleta(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_SERBOL)));
            configuracion.setSerieFactura(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_SERFAC)));
        }
        mCursor.close();
        return configuracion;
    }

    public long insertConfiguracion(Configuracion configuracion) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME_FECTRA, configuracion.getFechaTrabajo());
        values.put(COLUMN_NAME_URLSER, configuracion.getUrlServidor());
        values.put(COLUMN_NAME_SERBOL, configuracion.getSerieBoleta());
        values.put(COLUMN_NAME_SERFAC, configuracion.getSerieFactura());
        long insert = db.insert(TABLE_NAME, null, values);
        db.close();
        return insert;
    }

    public void deleteAllConfiguracion() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, null, null);
        db.close();
    }

}
