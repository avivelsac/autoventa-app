package pe.com.avivel.autoventa.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import pe.com.avivel.autoventa.R;
import pe.com.avivel.autoventa.adapter.DetalleDocumentoAdapter;
import pe.com.avivel.autoventa.exception.DbHelperException;
import pe.com.avivel.autoventa.fe.FacturadorEfact;
import pe.com.avivel.autoventa.helpers.ArticuloDbHelper;
import pe.com.avivel.autoventa.helpers.ClienteDbHelper;
import pe.com.avivel.autoventa.helpers.ConfiguracionDbHelper;
import pe.com.avivel.autoventa.helpers.DocumentoDbHelper;
import pe.com.avivel.autoventa.model.Articulo;
import pe.com.avivel.autoventa.model.Cliente;
import pe.com.avivel.autoventa.model.Configuracion;
import pe.com.avivel.autoventa.model.DetalleDocumento;
import pe.com.avivel.autoventa.model.Documento;
import pe.com.avivel.autoventa.model.Moneda;
import pe.com.avivel.autoventa.model.TipoDocumento;
import pe.com.avivel.autoventa.model.TipoDocumentoIdentidad;
import pe.com.avivel.autoventa.model.TipoOperacion;
import pe.com.avivel.autoventa.service.EfactIntentService;
import pe.com.avivel.autoventa.service.EfactResultReceiver;
import pe.com.avivel.autoventa.utils.Fecha;
import pe.com.avivel.autoventa.utils.Numero;
import pe.com.avivel.autoventa.ws.SincronizadorAvivel;

import static pe.com.avivel.autoventa.service.EfactIntentService.STATUS_ERROR;
import static pe.com.avivel.autoventa.service.EfactIntentService.STATUS_FINISHED;
import static pe.com.avivel.autoventa.service.EfactIntentService.STATUS_RUNNING;

public class DocumentoActivity extends AppCompatActivity implements EfactResultReceiver.Receiver {

    private final BigDecimal PCT_IGV = new BigDecimal("0.18");

    Context mContext;
    //helpers
    DocumentoDbHelper documentoDbHelper;
    ClienteDbHelper clienteDbHelper;
    ArticuloDbHelper articuloDbHelper;
    ConfiguracionDbHelper configuracionDbHelper;
    //views
    //AutoCompleteTextView spinnerCliente2;

    SearchableSpinner spinnerCliente1, spinnerCliente2, spinnerTipoDocumento, spinnerArticulo;
    //Spinner spinnerCliente1, spinnerCliente2, spinnerTipoDocumento, spinnerArticulo;
    ListView listViewDetalleDocumento;
    TextView tvAgregarDetalle, txSubtotal, txIgv, txTotal;
    EditText txSerieDocumento, txNumeroDocumento, txUnidad, txCantidad, txPrecio;
    Button btnGuardarDocumento, btnAnularDocumento;
    //adapters
    ArrayAdapter<TipoDocumento> tipoDocumentoAdapter;
    ArrayAdapter<Cliente> cliente1Adapter, cliente2Adapter;
    ArrayAdapter<Articulo> articuloAdapter;
    DetalleDocumentoAdapter detalleDocumentoAdapter;
    //data
    long id = 0;
    Documento documento;
    Configuracion configuracion;
    BigDecimal subtotal, igv, total = BigDecimal.ZERO;
    ArrayList<DetalleDocumento> detalleDocumentoList;
    //service
    private Intent efactIntentService;
    //Listas
    private List<Cliente> clienteList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_documento);
        mContext = this;

        //pe.com.avivel.autoventa.helpers
        documentoDbHelper = DocumentoDbHelper.getInstance(this);
        clienteDbHelper = ClienteDbHelper.getInstance(this);
        articuloDbHelper = ArticuloDbHelper.getInstance(this);
        configuracionDbHelper = ConfiguracionDbHelper.getInstance(this);

        //data
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            id = bundle.getLong("idDocumento");
            if (id > 0) {
                try {
                    documento = documentoDbHelper.getDocumentoById((int) id);
                } catch (Exception e) {
                    Toast.makeText(mContext, "Se produjo un error al guardar los datos.", Toast.LENGTH_LONG).show();
                }
            } else {
                documento = null;
            }
        }
        detalleDocumentoList = new ArrayList<>();
        configuracion = ConfiguracionDbHelper.getInstance(mContext).getConfiguracion();

        //campos
        spinnerTipoDocumento = findViewById(R.id.spTipoDocumento);
        spinnerCliente1 = findViewById(R.id.spCliente1);
        spinnerCliente2 = findViewById(R.id.spCliente2);
        spinnerArticulo = findViewById(R.id.spinnerArticulo);
        listViewDetalleDocumento = findViewById(R.id.listViewDetalleDocumento);
        tvAgregarDetalle = findViewById(R.id.tvAgregarDetalle);
        txUnidad = findViewById(R.id.txUnidad);
        txCantidad = findViewById(R.id.txCantidad);
        txPrecio = findViewById(R.id.txPrecio);
        txSubtotal = findViewById(R.id.txSubtotal);
        txIgv = findViewById(R.id.txIgv);
        txTotal = findViewById(R.id.txTotal);
        btnGuardarDocumento = findViewById(R.id.btnGuardarDocumento);
        btnAnularDocumento = findViewById(R.id.btnAnularDocumento);
        txSerieDocumento = findViewById(R.id.txSerieDocumento);
        txNumeroDocumento = findViewById(R.id.txNumeroDocumento);

        // ajustes
        txUnidad.setSelectAllOnFocus(true);
        txCantidad.setSelectAllOnFocus(true);
        txPrecio.setSelectAllOnFocus(true);
        spinnerCliente1.setTitle("Cliente real");
        spinnerCliente2.setTitle("Cliente en comprobante");
        spinnerTipoDocumento.setTitle("Tipo de documento");
        spinnerArticulo.setTitle("Artículo");

        // adapters
        tipoDocumentoAdapter = new ArrayAdapter<>(this, R.layout.spinner_layout);
        tipoDocumentoAdapter.setDropDownViewResource(R.layout.spinner_layout);
        spinnerTipoDocumento.setAdapter(tipoDocumentoAdapter);
        tipoDocumentoAdapter.addAll(TipoDocumento.values());

        // adapter cliente real
        cliente2Adapter = new ArrayAdapter<>(this, R.layout.spinner_layout);
        cliente2Adapter.setDropDownViewResource(R.layout.spinner_layout);
        spinnerCliente2.setAdapter(cliente2Adapter);
        try {
            clienteList = clienteDbHelper.getAllClientes();
            cliente2Adapter.addAll(clienteList);
        } catch (Exception e) {
            Toast.makeText(mContext, "Se produjo un error al consultar los datos.", Toast.LENGTH_LONG).show();
        }

        // adapter cliente comprobante
        cliente1Adapter = new ArrayAdapter<>(this, R.layout.spinner_layout);
        cliente1Adapter.setDropDownViewResource(R.layout.spinner_layout);
        spinnerCliente1.setAdapter(cliente1Adapter);

        articuloAdapter = new ArrayAdapter<>(this, R.layout.spinner_layout);
        articuloAdapter.setDropDownViewResource(R.layout.spinner_layout);
        spinnerArticulo.setAdapter(articuloAdapter);
        try {
            articuloAdapter.addAll(articuloDbHelper.getAll());
        } catch (Exception e) {
            Toast.makeText(mContext, "Se produjo un error al consultar los datos.", Toast.LENGTH_LONG).show();
        }

        detalleDocumentoAdapter = new DetalleDocumentoAdapter(this, detalleDocumentoList);
        listViewDetalleDocumento.setAdapter(detalleDocumentoAdapter);

        EfactResultReceiver mReceiver = new EfactResultReceiver(new Handler());
        mReceiver.setReceiver(this);

        efactIntentService = new Intent(Intent.ACTION_SYNC, null, this, EfactIntentService.class);
        efactIntentService.putExtra("receiver", mReceiver);

        tvAgregarDetalle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BigDecimal cantidad = new BigDecimal(txCantidad.getText().toString());
                BigDecimal precio = new BigDecimal(txPrecio.getText().toString());
                if (cantidad.compareTo(BigDecimal.ZERO) <= 0) {
                    Toast.makeText(mContext, "Debe indicar una cantidad válida (mayor que cero).", Toast.LENGTH_LONG).show();
                    txCantidad.requestFocus();
                } else if (precio.compareTo(BigDecimal.ZERO) <= 0) {
                    Toast.makeText(mContext, "Debe indicar un precio válido (mayor que cero).", Toast.LENGTH_LONG).show();
                    txPrecio.requestFocus();
                } else {
                    // Añadir detalle
                    DetalleDocumento detalleDocumento = new DetalleDocumento();
                    detalleDocumento.setArticulo((Articulo) spinnerArticulo.getSelectedItem());
                    detalleDocumento.setUnidades(new BigDecimal(txUnidad.getText().toString()));
                    detalleDocumento.setCantidad(cantidad);
                    detalleDocumento.setPrecio(precio);
                    detalleDocumento.setImporte((detalleDocumento.getCantidad().multiply(detalleDocumento.getPrecio())).setScale(2, RoundingMode.HALF_UP));

                    detalleDocumentoAdapter.add(detalleDocumento);

                    limpiarPanelDetalle();

                    actualizarTotales();
                }
            }
        });

        btnGuardarDocumento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (validar()) {
                        if (id > 0) {
                            // update documento
                            switch (documento.getSituacion()) {
                                case FacturadorEfact.SITUACION_NO_ENVIADO:
                                case FacturadorEfact.SITUACION_ERROR:
                                case FacturadorEfact.SITUACION_DESCONOCIDO:
                                    documento.setCliente((Cliente) spinnerCliente1.getSelectedItem());
                                    documento.setClienteDestino((Cliente) spinnerCliente2.getSelectedItem());
                                    //documento.setClienteDestino(clienteList.get(spinnerCliente2.getListSelection()));
                                    documento.setTipoDocumento((TipoDocumento) spinnerTipoDocumento.getSelectedItem());
                                    documento.setSerie(txSerieDocumento.getText().toString());
                                    documento.setNumero(txNumeroDocumento.getText().toString());
                                    documento.setBaseImponible(subtotal);
                                    documento.setIgv(igv);
                                    documento.setImporte(total);
                                    documento.setDetalleDocumentoList(detalleDocumentoList);

                                    long update = documentoDbHelper.update(documento);
                                    // envía a EFACT
                                    if (update > 0) {
                                        enviarDocumento(documento);
                                        limpiarVentana();
                                    } else {
                                        Toast.makeText(mContext, "Ha ocurrido un error al grabar los datos.", Toast.LENGTH_LONG).show();
                                    }
                                    break;
                                default:
                                    // no grabar ni enviar
                                    Toast.makeText(mContext, "La situación del comprobante no permite modificaciones.", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            documento = new Documento();

                            documento.setFechaEmision(Fecha.toDate(configuracion.getFechaTrabajo()));
                            documento.setMoneda(Moneda.MN);
                            documento.setCliente((Cliente) spinnerCliente1.getSelectedItem());
                            documento.setClienteDestino((Cliente) spinnerCliente2.getSelectedItem());
                            //documento.setClienteDestino(clienteList.get(spinnerCliente2.getListSelection()));
                            documento.setTipoDocumento((TipoDocumento) spinnerTipoDocumento.getSelectedItem());
                            documento.setSerie(txSerieDocumento.getText().toString());
                            documento.setNumero(txNumeroDocumento.getText().toString());
                            documento.setPorcentajeIgv(PCT_IGV);
                            documento.setBaseImponible(subtotal);
                            documento.setOperacionesInafectas(BigDecimal.ZERO);
                            documento.setOperacionesExoneradas(BigDecimal.ZERO);
                            documento.setOperacionesGratuitas(BigDecimal.ZERO);
                            documento.setIgv(igv);
                            documento.setImporte(total);
                            documento.setImpreso(true);
                            documento.setTipoOperacion(TipoOperacion._0101);
                            documento.setSituacion(FacturadorEfact.SITUACION_NO_ENVIADO);
                            documento.setSituacionAvivel(SincronizadorAvivel.SITUACION_NO_ENVIADO);
                            documento.setDetalleDocumentoList(detalleDocumentoList);

                            long insert = documentoDbHelper.insert(documento);

                            if (insert > 0) {
                                enviarDocumento(documento);
                                limpiarVentana();
                            } else {
                                Toast.makeText(mContext, "Ha ocurrido un error al grabar los datos xd.", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(mContext, "Se produjo un error al guardar los datos ee.", Toast.LENGTH_LONG).show();
                }
            }
        });

        btnAnularDocumento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // si documento existe
                if (id > 0) {
                    // Se puede anular en caso que no haya sido exitosamente a AVIVEL
                    if (documento.getSituacionAvivel().equals(SincronizadorAvivel.SITUACION_NO_ENVIADO) || documento.getSituacionAvivel().equals("Error.")) {
                        // Se puede anular en caso que esté en situación ENVIADO Y ACEPTADO
                        switch (documento.getSituacion()) {
                            case FacturadorEfact.SITUACION_ENVIADO:
                            case FacturadorEfact.SITUACION_ENVIADO_VALIDACION:
                                Toast.makeText(mContext, "Para anular, la situación del documento debe ser \"Enviado y aceptado.\" o \"Error.\".", Toast.LENGTH_LONG).show();
                                break;
                            case FacturadorEfact.SITUACION_ENVIADO_ACEPTADO:
                            case FacturadorEfact.SITUACION_ERROR:
                                anular();
                                break;
                            default:
                                Toast.makeText(mContext, "No es necesario anular este comprobante. En su lugar, puede modificar los datos.", Toast.LENGTH_LONG).show();
                                break;
                        }
                    } else {
                        Toast.makeText(mContext, "Este documento no se puede anular, ya fue enviado a AVIVEL: " + documento.getSituacionAvivel(), Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        // listener cliente real
        AdapterView.OnItemSelectedListener onItemSelectedListener = spinnerCliente2.getOnItemSelectedListener();
        Log.i("DATABASE", "onItemSelectedListener1: " + (onItemSelectedListener == null));
        spinnerCliente2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                cargarSpinnerCliente1();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
        AdapterView.OnItemSelectedListener onItemSelectedListener2 = spinnerCliente2.getOnItemSelectedListener();
        Log.i("DATABASE", "onItemSelectedListener2: " + (onItemSelectedListener2 == null));

        spinnerTipoDocumento.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                completarSerie();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        txSerieDocumento.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                TipoDocumento tipoDocumento = (TipoDocumento) spinnerTipoDocumento.getSelectedItem();
                String serie = txSerieDocumento.getText().toString();
                String numeroDisponibleByTipoAndSerie;
                try {
                    // caso updateArticulo
                    if (id > 0) {
                        if (documento != null) {
                            if (!serie.equals(documento.getSerie())) {
                                numeroDisponibleByTipoAndSerie = documentoDbHelper.getNumeroDisponibleByTipoAndSerie(tipoDocumento, serie);
                                txNumeroDocumento.setText(numeroDisponibleByTipoAndSerie);
                            } else {
                                txNumeroDocumento.setText(documento.getNumero());
                            }
                        }
                    } else {
                        numeroDisponibleByTipoAndSerie = documentoDbHelper.getNumeroDisponibleByTipoAndSerie(tipoDocumento, serie);
                        txNumeroDocumento.setText(numeroDisponibleByTipoAndSerie);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private boolean validar() {
        boolean valido = false;
        try {
            TipoDocumento tipoDoc = (TipoDocumento) spinnerTipoDocumento.getSelectedItem();
            Cliente clienteComprobante = (Cliente) spinnerCliente1.getSelectedItem();
            if (txSerieDocumento.getText().length() == 0) {
                Toast.makeText(mContext, "Debe indicar la serie del comprobante.", Toast.LENGTH_LONG).show();
                txSerieDocumento.requestFocus();
            } else if (txNumeroDocumento.getText().length() == 0) {
                Toast.makeText(mContext, "Debe indicar el número correlativo del comprobante.", Toast.LENGTH_LONG).show();
                txNumeroDocumento.requestFocus();
            } else if (id == 0 && documentoDbHelper.getDocumentoByTipoAndSerieAndNumero(tipoDoc, txSerieDocumento.getText().toString(), txNumeroDocumento.getText().toString()) != null) {
                Toast.makeText(mContext, "Ya existe el comprobante con ese número correlativo.", Toast.LENGTH_LONG).show();
                txNumeroDocumento.requestFocus();
            }else if (tipoDoc == TipoDocumento.FT && clienteComprobante.getTipoDocumentoIdentidad() != TipoDocumentoIdentidad.RUC) {
                Toast.makeText(mContext, "Para generar una FACTURA, el cliente debe tener RUC.", Toast.LENGTH_LONG).show();
                spinnerCliente1.requestFocus();
            } else if (detalleDocumentoList.isEmpty()) {
                Toast.makeText(mContext, "Debe añadir al menos un ítem al comprobante", Toast.LENGTH_LONG).show();
                txUnidad.requestFocus();
            } else {
                valido = true;
            }
        } catch (Exception ex) {
            Toast.makeText(mContext, "Se produjo un error al consultar los datos.", Toast.LENGTH_LONG).show();
        }
        return valido;
    }

    private void enviarDocumento(final Documento documento) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(DocumentoActivity.this);
        alertDialog.setTitle("Comprobante grabado satisfactoriamente.");
        alertDialog.setMessage("¿Desea enviar el comprobante a EFACT?");
        alertDialog.setIcon(R.drawable.ic_menu_send);
        alertDialog.setPositiveButton("Enviar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                efactIntentService.putExtra("id", documento.getId());
                efactIntentService.putExtra("command", "sendxml");
                startService(efactIntentService);
            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent listDocumentoActivity = new Intent(mContext, ListDocumentoActivity.class);
                startActivity(listDocumentoActivity);
            }
        });
        alertDialog.show();
    }

    private void anular() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(DocumentoActivity.this);
        alertDialog.setTitle("Atención");
        alertDialog.setMessage("¿Desea anular el comprobante?");
        alertDialog.setIcon(R.drawable.ic_error_outline);
        alertDialog.setPositiveButton("Anular", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // carga
                Cliente clienteAnulado = new Cliente();
                clienteAnulado.setId(0);

                documento.setCliente(clienteAnulado);
                documento.setClienteDestino(clienteAnulado);

                for (DetalleDocumento detalleDocumento : documento.getDetalleDocumentoList()) {
                    detalleDocumento.setCantidad(0);
                    detalleDocumento.setUnidades(0);
                    detalleDocumento.setPrecio(0);
                    detalleDocumento.setImporte(0);
                }

                documento.setBaseImponible(0);
                documento.setIgv(0);
                documento.setImporte(0);

                // modifica
                long update;
                try {
                    update = documentoDbHelper.update(documento);
                    // modifica
                    if (update > 0) {
                        Toast.makeText(mContext, "El documento fue anulado satisfactoriamente.", Toast.LENGTH_LONG).show();
                        Intent clie = new Intent(mContext, ListDocumentoActivity.class);
                        startActivity(clie);
                    } else {
                        Toast.makeText(mContext, "Ha ocurrido un error al grabar los datos.", Toast.LENGTH_LONG).show();
                    }
                } catch (DbHelperException e) {
                    Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
        alertDialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent clie = new Intent(mContext, ListDocumentoActivity.class);
                startActivity(clie);
            }
        });
        alertDialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getData();
    }

    private void cargarSpinnerCliente1() {
        Cliente clienteReal = (Cliente) spinnerCliente2.getSelectedItem();

        //int listSelection = spinnerCliente2.getListSelection();
        //Cliente clienteReal = clienteList.get(listSelection);
        List<Cliente> clienteListByClienteReal;
        try {
            clienteListByClienteReal = clienteDbHelper.getClienteListByClienteReal(clienteReal);
            cliente1Adapter.clear();
            cliente1Adapter.addAll(clienteListByClienteReal);
            if (id > 0) {
                if (documento != null) {
                    spinnerCliente1.setSelection(cliente1Adapter.getPosition(documento.getCliente()));
                }
            }
        } catch (Exception e) {
            Toast.makeText(mContext, "Se produjo un error al consultar los datos.", Toast.LENGTH_LONG).show();
        }
    }

    private void completarSerie() {
        TipoDocumento tipoDocumento = (TipoDocumento) spinnerTipoDocumento.getSelectedItem();
        String serieBoleta = configuracion.getSerieBoleta();
        String serieFactura = configuracion.getSerieFactura();
        if (tipoDocumento == TipoDocumento.BV) {
            txSerieDocumento.setText(serieBoleta);
        } else {
            txSerieDocumento.setText(serieFactura);
        }
    }

    private void limpiarVentana() {
        spinnerCliente1.setSelection(0);
        spinnerCliente2.setSelection(0);
        spinnerTipoDocumento.setSelection(0);
        //txSerieDocumento.getText().clear();
        //txNumeroDocumento.getText().clear();
        completarSerie();
        detalleDocumentoAdapter.clear();
        actualizarTotales();
    }

    private void limpiarPanelDetalle() {
        spinnerArticulo.setSelection(0);
        txUnidad.setText(R.string.hint_zero);
        txCantidad.setText(R.string.hint_zero);
        txPrecio.setText(R.string.hint_zero);
    }

    public void actualizarTotales() {
        total = BigDecimal.ZERO;
        int i = 1;
        for (DetalleDocumento detalleDocumento : detalleDocumentoList) {
            detalleDocumento.setSecuencia(i);
            total = total.add(detalleDocumento.getImporte());
            i++;
        }
        total = total.setScale(2, RoundingMode.HALF_UP);
        subtotal = total.divide(new BigDecimal("1.18"), 2, RoundingMode.HALF_UP);
        igv = total.subtract(subtotal);

        txSubtotal.setText(Numero.formatear(subtotal));
        txIgv.setText(Numero.formatear(igv));
        txTotal.setText(Numero.formatear(total));
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        Intent clie = new Intent(mContext, ListDocumentoActivity.class);
        switch (resultCode) {
            case STATUS_RUNNING:
                //show progress
                Toast running = Toast.makeText(this, "Enviando el comprobante a EFACT...", Toast.LENGTH_SHORT);
                running.show();
                break;
            case STATUS_FINISHED:
                Toast.makeText(this, "El comprobante se envió a EFACT satisfactoriamente.", Toast.LENGTH_SHORT).show();
                startActivity(clie);
                break;
            case STATUS_ERROR:
                // handle the error;
                String e = resultData.getString(Intent.EXTRA_TEXT);
                Toast.makeText(this, "Hubo un problema al enviar el comprobante: " + e, Toast.LENGTH_SHORT).show();
                startActivity(clie);
                break;
        }
    }

    private void getData() {
        if (documento != null) {
            id = documento.getId();

            spinnerCliente2.setSelection(cliente2Adapter.getPosition(documento.getClienteDestino()));
            spinnerTipoDocumento.setSelection(tipoDocumentoAdapter.getPosition(documento.getTipoDocumento()));
            txSerieDocumento.setText(documento.getSerie());
            txNumeroDocumento.setText(documento.getNumero());

            detalleDocumentoList.clear();
            detalleDocumentoList.addAll(documento.getDetalleDocumentoList());

            actualizarTotales();

            btnAnularDocumento.setEnabled(true);
            btnAnularDocumento.setBackgroundResource(R.drawable.button_enabled);

        } else {
            btnAnularDocumento.setEnabled(false);
            btnAnularDocumento.setBackgroundResource(R.drawable.button_disabled);
        }
    }
}
