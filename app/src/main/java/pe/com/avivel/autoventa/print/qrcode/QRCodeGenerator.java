package pe.com.avivel.autoventa.print.qrcode;

import android.graphics.Bitmap;
import android.graphics.Color;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.ByteMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import java.util.Hashtable;

import pe.com.avivel.autoventa.print.exceptions.QRCodeException;

public class QRCodeGenerator {

    public Bitmap generate(String textValue) throws QRCodeException {
        return generate(textValue, 150);
    }

    public Bitmap generate(String textValue, int size) throws QRCodeException {
        try {

            Hashtable<EncodeHintType, Object> hintMap = setEncodingbehavior();

            ByteMatrix bm = getByteMatrix(textValue, size, hintMap);

            return getImage(bm);

        } catch (WriterException e) {
            throw new QRCodeException("QRCode generation error", e);
        }
    }

    private Hashtable<EncodeHintType, Object> setEncodingbehavior() {
        Hashtable<EncodeHintType, Object> hintMap = new Hashtable<>();
        hintMap.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.Q);
        return hintMap;
    }


    private ByteMatrix getByteMatrix(String textValue, int size, Hashtable<EncodeHintType, Object> hintMap) throws WriterException {
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        return qrCodeWriter.encode(textValue, BarcodeFormat.QR_CODE, size, size, hintMap);
    }

    private Bitmap getImage(ByteMatrix byteMatrix) {
        int width = byteMatrix.width();
        Bitmap bitMap = Bitmap.createBitmap(width, width, Bitmap.Config.RGB_565);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < width; y++) {
                bitMap.setPixel(y, x, byteMatrix.get(x, y) == 0 ? Color.BLACK : Color.WHITE);
            }
        }
        return bitMap;
    }

}
