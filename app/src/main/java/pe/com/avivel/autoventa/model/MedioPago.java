package pe.com.avivel.autoventa.model;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum MedioPago {

    DEPOSITO(1);

    private Integer id;

    MedioPago(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

}
