package pe.com.avivel.autoventa.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import pe.com.avivel.autoventa.R;
import pe.com.avivel.autoventa.adapter.ClienteAdapter;
import pe.com.avivel.autoventa.helpers.ClienteDbHelper;
import pe.com.avivel.autoventa.model.Cliente;


public class ListClienteActivity extends AppCompatActivity {

    private Context mContext;
    private ClienteDbHelper clienteDbHelper;
    private ClienteAdapter dataAdapterCliente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_cliente);

        mContext = this;
        clienteDbHelper = ClienteDbHelper.getInstance(this);

        FloatingActionButton fab = findViewById(R.id.btnAddCliente);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent clie = new Intent(mContext, ClienteActivity.class);
                startActivityForResult(clie, 1);
            }
        });

        ArrayList<Cliente> clienteArrayList = new ArrayList<>();
        dataAdapterCliente = new ClienteAdapter(this, clienteArrayList);

        // Attach the data adapter to a ListView
        ListView listView = findViewById(R.id.listClientes);
        listView.setAdapter(dataAdapterCliente);

        cargarListViewCliente();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                long idCliente = Long.parseLong(((TextView) view.findViewById(R.id.idCliente)).getText().toString());
                Bundle bundle = new Bundle();
                bundle.putLong("idCliente", idCliente);
                Intent clie = new Intent(mContext, ClienteActivity.class);
                clie.putExtras(bundle);
                startActivityForResult(clie, 1);
            }
        });
    }

    private void cargarListViewCliente() {
        List<Cliente> clienteList;
        try {
            clienteList = clienteDbHelper.getAllClientes();

            dataAdapterCliente.clear();
            dataAdapterCliente.addAll(clienteList);
        } catch (Exception e) {
            Toast.makeText(mContext, "Se produjo un error al consultar los datos.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == RESULT_OK) {
            cargarListViewCliente();
        }
    }
}
