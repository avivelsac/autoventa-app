package pe.com.avivel.autoventa.fe;

import android.util.Log;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import pe.com.avivel.autoventa.exception.FEException;
import pe.com.avivel.autoventa.model.DetalleDocumento;
import pe.com.avivel.autoventa.model.Documento;
import pe.com.avivel.autoventa.utils.ConvertidorNumeroToLetras;
import pe.com.avivel.autoventa.utils.Fecha;
import pe.com.avivel.autoventa.utils.Numero;
import pe.com.avivel.autoventa.utils.Utils;

import static pe.com.avivel.autoventa.fe.FacturadorEfact.UNSIGNED_XML_PATH;

class FacturaBoletaElectronica {

    static void generarXML(Documento documento) throws FEException {
        String pathXMLFile;
        try {
            pathXMLFile = UNSIGNED_XML_PATH + documento.getNombreArchivoFE() + ".xml";
            File xmlFile = new File(pathXMLFile);

            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(true);
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.newDocument();

            Element invoice = doc.createElementNS("urn:oasis:names:specification:ubl:schema:xsd:Invoice-2", "Invoice");
            invoice.setAttribute("xmlns:cac", "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2");
            invoice.setAttribute("xmlns:cbc", "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2");
            invoice.setAttribute("xmlns:ext", "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2");
            doc.appendChild(invoice);

            // cbc:UBLVersionID
            Element uBLVersionID = doc.createElement("cbc:UBLVersionID");
            invoice.appendChild(uBLVersionID);
            uBLVersionID.appendChild(doc.createTextNode("2.1"));

            // cbc:CustomizationID
            Element customizationID = doc.createElement("cbc:CustomizationID");
            invoice.appendChild(customizationID);
            customizationID.appendChild(doc.createTextNode("2.0"));

            // cbc:ID
            Element id = doc.createElement("cbc:ID");
            invoice.appendChild(id);
            id.appendChild(doc.createTextNode(documento.getNumeroConSerie()));

            // cbc:IssueDate
            Element issueDate = doc.createElement("cbc:IssueDate");
            invoice.appendChild(issueDate);
            issueDate.appendChild(doc.createTextNode(Fecha.toStringMySQL(documento.getFechaEmision())));

            // cbc:DueDate
            if (documento.getFechaVencimiento() != null) {
                Element dueDate = doc.createElement("cbc:DueDate");
                invoice.appendChild(dueDate);
                dueDate.appendChild(doc.createTextNode(Fecha.toStringMySQL(documento.getFechaVencimiento())));
            }

            // cbc:InvoiceTypeCode
            Element invoiceTypeCode = doc.createElement("cbc:InvoiceTypeCode");
            invoiceTypeCode.setAttribute("listID", documento.getTipoOperacion().getCodigo());
            invoiceTypeCode.setAttribute("listAgencyName", "PE:SUNAT");
            invoiceTypeCode.setAttribute("listName", "Tipo de Documento");
            invoiceTypeCode.setAttribute("listSchemeURI", "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo51");
            invoiceTypeCode.setAttribute("listURI", "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo01");
            invoiceTypeCode.setAttribute("name", "Tipo de Operacion");
            invoice.appendChild(invoiceTypeCode);
            invoiceTypeCode.appendChild(doc.createTextNode(documento.getTipoDocumento().getCodigoSunat()));

            // cbc:Note
            Element note = doc.createElement("cbc:Note");
            note.setAttribute("languageLocaleID", "1000");
            invoice.appendChild(note);
            note.appendChild(doc.createTextNode(ConvertidorNumeroToLetras.convierte(Numero.formatear(documento.getImporte(), 2), ConvertidorNumeroToLetras.Tipo.Pronombre, documento.getMoneda().getCodigo())));
            if (documento.isItinerante()) {
                Element noteDetraccion = doc.createElement("cbc:Note");
                noteDetraccion.setAttribute("languageLocaleID", "2005");
                invoice.appendChild(noteDetraccion);
                noteDetraccion.appendChild(doc.createTextNode("Venta realizada por emisor itinerante"));
            }
            if (documento.isSujetoSpot()) {
                Element noteDetraccion = doc.createElement("cbc:Note");
                noteDetraccion.setAttribute("languageLocaleID", "2006");
                invoice.appendChild(noteDetraccion);
                noteDetraccion.appendChild(doc.createTextNode("Operación sujeta a detracción"));
            }

            // cbc:DocumentCurrencyCode
            Element documentCurrencyCode = doc.createElement("cbc:DocumentCurrencyCode");
            documentCurrencyCode.setAttribute("listAgencyName", "United Nations Economic Commission for Europe");
            documentCurrencyCode.setAttribute("listID", "ISO 4217 Alpha");
            documentCurrencyCode.setAttribute("listName", "Currency");
            invoice.appendChild(documentCurrencyCode);
            documentCurrencyCode.appendChild(doc.createTextNode(documento.getMoneda().getCodigoISO()));

            // cbc:LineCountNumeric
            Element lineCountNumeric = doc.createElement("cbc:LineCountNumeric");
            invoice.appendChild(lineCountNumeric);
            lineCountNumeric.appendChild(doc.createTextNode(String.valueOf(documento.getDetalleDocumentoList().size())));

            // cac:OrderReference
            if (documento.getOrdenCompra() != null && !documento.getOrdenCompra().isEmpty()) {
                Element orderReference = doc.createElement("cac:OrderReference");
                Element idOrderReference = doc.createElement("cbc:ID");
                idOrderReference.appendChild(doc.createTextNode(documento.getOrdenCompra()));
                orderReference.appendChild(idOrderReference);
                invoice.appendChild(orderReference);
            }

            /*
             * 22. Tipo y Número de la guía de remisión relacionada
             */
            //Invoice/cac:DespatchDocumentReference/cbc:ID (Número de documento)
            if (!Utils.isBlank(documento.getGuiaRemision())) {
                Element despatchDocumentReference = doc.createElement("cac:DespatchDocumentReference");
                invoice.appendChild(despatchDocumentReference);
                Element idDespatchDocumentReference = doc.createElement("cbc:ID");
                despatchDocumentReference.appendChild(idDespatchDocumentReference);
                idDespatchDocumentReference.appendChild(doc.createTextNode("0" + documento.getGuiaRemision()));
                Element documentTypeCode = doc.createElement("cbc:DocumentTypeCode");
                despatchDocumentReference.appendChild(documentTypeCode);
                documentTypeCode.setAttribute("listAgencyName", "PE:SUNAT");
                documentTypeCode.setAttribute("listName", "Tipo de Documento");
                documentTypeCode.setAttribute("listURI", "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo01");
                documentTypeCode.appendChild(doc.createTextNode("09"));
            }

            // cac:Signature
            Element signature = doc.createElement("cac:Signature");
            invoice.appendChild(signature);
            // cac:Signature/cbc:ID
            Element idSignature = doc.createElement("cbc:ID");
            signature.appendChild(idSignature);
            idSignature.appendChild(doc.createTextNode("IDSignKG"));
            // cac:Signature/cac:SignatoryParty
            Element signatoryParty = doc.createElement("cac:SignatoryParty");
            signature.appendChild(signatoryParty);
            // cac:Signature/cac:SignatoryParty/cac:PartyIdentification
            Element partyIdentification = doc.createElement("cac:PartyIdentification");
            signatoryParty.appendChild(partyIdentification);
            // cac:Signature/cac:SignatoryParty/cac:PartyIdentification/cbc:ID
            Element idPartyIdentification = doc.createElement("cbc:ID");
            partyIdentification.appendChild(idPartyIdentification);
            idPartyIdentification.appendChild(doc.createTextNode("20524088810"));
            // cac:Signature/cac:SignatoryParty/cac:PartyName
            Element partyName = doc.createElement("cac:PartyName");
            signatoryParty.appendChild(partyName);
            // cac:Signature/cac:SignatoryParty/cac:PartyName/cbc:Name
            Element name = doc.createElement("cbc:Name");
            partyName.appendChild(name);
            name.appendChild(doc.createTextNode("AVIVEL SAC"));
            // cac:Signature/cac:DigitalSignatureAttachment
            Element digitalSignatureAttachment = doc.createElement("cac:DigitalSignatureAttachment");
            signature.appendChild(digitalSignatureAttachment);
            // cac:Signature/cac:DigitalSignatureAttachment/cac:ExternalReference
            Element externalReference = doc.createElement("cac:ExternalReference");
            digitalSignatureAttachment.appendChild(externalReference);
            // cac:Signature/cac:DigitalSignatureAttachment/cac:ExternalReference/cbc:URI
            Element uri = doc.createElement("cbc:URI");
            externalReference.appendChild(uri);
            uri.appendChild(doc.createTextNode("#SignST"));

            //Invoice/cac:AccountingSupplierParty/cac:Party/cac:PartyIdentification/cbc:ID (Número de RUC)
            Element accountingSupplierParty = doc.createElement("cac:AccountingSupplierParty");
            invoice.appendChild(accountingSupplierParty);
            Element party = doc.createElement("cac:Party");
            accountingSupplierParty.appendChild(party);
            Element partyIdentificationSupplier = doc.createElement("cac:PartyIdentification");
            party.appendChild(partyIdentificationSupplier);
            Element idAccountingSupplier = doc.createElement("cbc:ID");
            partyIdentificationSupplier.appendChild(idAccountingSupplier);
            idAccountingSupplier.setAttribute("schemeID", "6");
            idAccountingSupplier.setAttribute("schemeName", "Documento de Identidad");
            idAccountingSupplier.setAttribute("schemeAgencyName", "PE:SUNAT");
            idAccountingSupplier.setAttribute("schemeURI", "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo06");
            idAccountingSupplier.appendChild(doc.createTextNode("20524088810"));

            //Invoice/cac:AccountingSupplierParty/cac:Party/cac:PartyLegalEntity/cbc:RegistrationName
            Element partyLegalEntity = doc.createElement("cac:PartyLegalEntity");
            party.appendChild(partyLegalEntity);
            Element registrationName = doc.createElement("cbc:RegistrationName");
            partyLegalEntity.appendChild(registrationName);
            registrationName.appendChild(doc.createTextNode("AVIVEL SAC"));

            //Invoice/cac:AccountingSupplierParty/cac:Party/cac:PartyLegalEntity/cbc:RegistrationAddress
            Element registrationAddress = doc.createElement("cac:RegistrationAddress");
            partyLegalEntity.appendChild(registrationAddress);
            Element idRegistrationAddress = doc.createElement("cbc:ID");
            registrationAddress.appendChild(idRegistrationAddress);
            idRegistrationAddress.setAttribute("schemeAgencyName", "PE:INEI");
            idRegistrationAddress.setAttribute("schemeName", "Ubigeos");
            idRegistrationAddress.appendChild(doc.createTextNode("150123"));

            Element addressTypeCode = doc.createElement("cbc:AddressTypeCode");
            registrationAddress.appendChild(addressTypeCode);
            addressTypeCode.setAttribute("listAgencyName", "PE:SUNAT");
            addressTypeCode.setAttribute("listName", "Establecimientos anexos");
            addressTypeCode.appendChild(doc.createTextNode("0000"));

            Element citySubdivisionName = doc.createElement("cbc:CitySubdivisionName");
            registrationAddress.appendChild(citySubdivisionName);
            citySubdivisionName.appendChild(doc.createTextNode("URB. HUERTOS DE VILLENA"));

            Element cityName = doc.createElement("cbc:CityName");
            registrationAddress.appendChild(cityName);
            cityName.appendChild(doc.createTextNode("LIMA"));

            Element countrySubentity = doc.createElement("cbc:CountrySubentity");
            registrationAddress.appendChild(countrySubentity);
            countrySubentity.appendChild(doc.createTextNode("LIMA"));

            Element district = doc.createElement("cbc:District");
            registrationAddress.appendChild(district);
            district.appendChild(doc.createTextNode("PACHACAMAC"));

            Element addressLine = doc.createElement("cac:AddressLine");
            registrationAddress.appendChild(addressLine);
            Element line = doc.createElement("cbc:Line");
            addressLine.appendChild(line);
            line.appendChild(doc.createTextNode("AV. LOS NOGALES MZA. F LOTE. 14"));

            Element country = doc.createElement("cac:Country");
            registrationAddress.appendChild(country);
            Element identificationCode = doc.createElement("cbc:IdentificationCode");
            country.appendChild(identificationCode);
            identificationCode.setAttribute("listAgencyName", "United Nations Economic Commission for Europe");
            identificationCode.setAttribute("listID", "ISO 3166-1");
            identificationCode.setAttribute("listName", "Country");
            identificationCode.appendChild(doc.createTextNode("PE"));

            //Invoice/cac:AccountingCustomerParty/cac:Party/cac:PartyIdentification/cbc:ID
            Element accountingCustomerParty = doc.createElement("cac:AccountingCustomerParty");
            invoice.appendChild(accountingCustomerParty);
            Element partyCustomer = doc.createElement("cac:Party");
            accountingCustomerParty.appendChild(partyCustomer);
            Element partyIdentificationCustomer = doc.createElement("cac:PartyIdentification");
            partyCustomer.appendChild(partyIdentificationCustomer);
            Element idAccountingCustomer = doc.createElement("cbc:ID");
            partyIdentificationCustomer.appendChild(idAccountingCustomer);
            idAccountingCustomer.setAttribute("schemeID", documento.getCliente().getTipoDocumentoIdentidad().getCodigoSunat());
            idAccountingCustomer.setAttribute("schemeName", "Documento de Identidad");
            idAccountingCustomer.setAttribute("schemeAgencyName", "PE:SUNAT");
            idAccountingCustomer.setAttribute("schemeURI", "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo06");
            idAccountingCustomer.appendChild(doc.createTextNode(documento.getCliente().getNumeroDocumentoIdentidad()));

            //Invoice/cac:AccountingCustomerParty/cac:Party/cac:PartyLegalEntity/cbc:RegistrationName
            Element partyLegalEntityCustomer = doc.createElement("cac:PartyLegalEntity");
            partyCustomer.appendChild(partyLegalEntityCustomer);
            Element registrationNameCustomer = doc.createElement("cbc:RegistrationName");
            partyLegalEntityCustomer.appendChild(registrationNameCustomer);
            registrationNameCustomer.appendChild(doc.createTextNode(documento.getCliente().getRazonSocial()));

            //Invoice/cac:AccountingCustomerParty/cac:Party/cac:PartyLegalEntity/cac:RegistrationAddress
            Element registrationAddressCustomer = doc.createElement("cac:RegistrationAddress");
            partyLegalEntityCustomer.appendChild(registrationAddressCustomer);
            Element idRegistrationAddressCustomer = doc.createElement("cbc:ID");
            registrationAddressCustomer.appendChild(idRegistrationAddressCustomer);
            idRegistrationAddressCustomer.setAttribute("schemeAgencyName", "PE:INEI");
            idRegistrationAddressCustomer.setAttribute("schemeName", "Ubigeos");
            idRegistrationAddressCustomer.appendChild(doc.createTextNode("-"));

            Element citySubdivisionNameCustomer = doc.createElement("cbc:CitySubdivisionName");
            registrationAddressCustomer.appendChild(citySubdivisionNameCustomer);
            citySubdivisionNameCustomer.appendChild(doc.createTextNode("-")); // Urbanización

            Element cityNameCustomer = doc.createElement("cbc:CityName");
            registrationAddressCustomer.appendChild(cityNameCustomer);
            cityNameCustomer.appendChild(doc.createTextNode(Utils.ifNull(documento.getCliente().getProvincia(), "-")));

            Element countrySubentityCustomer = doc.createElement("cbc:CountrySubentity");
            registrationAddressCustomer.appendChild(countrySubentityCustomer);
            countrySubentityCustomer.appendChild(doc.createTextNode(Utils.ifNull(documento.getCliente().getDepartamento(), "-")));

            Element districtCustomer = doc.createElement("cbc:District");
            registrationAddressCustomer.appendChild(districtCustomer);
            districtCustomer.appendChild(doc.createTextNode(Utils.ifNull(documento.getCliente().getDistrito(), "-")));

            Element addressLineCustomer = doc.createElement("cac:AddressLine");
            registrationAddressCustomer.appendChild(addressLineCustomer);
            Element lineCustomer = doc.createElement("cbc:Line");
            addressLineCustomer.appendChild(lineCustomer);
            lineCustomer.appendChild(doc.createTextNode(documento.getCliente().getDireccionCompleta()));

            Element countryCustomer = doc.createElement("cac:Country");
            registrationAddressCustomer.appendChild(countryCustomer);
            Element identificationCodeCustomer = doc.createElement("cbc:IdentificationCode");
            countryCustomer.appendChild(identificationCodeCustomer);
            identificationCodeCustomer.setAttribute("listAgencyName", "United Nations Economic Commission for Europe");
            identificationCodeCustomer.setAttribute("listID", "ISO 3166-1");
            identificationCodeCustomer.setAttribute("listName", "Country");
            identificationCodeCustomer.appendChild(doc.createTextNode("PE"));

//            if (documento.getCliente().getEmail() != null) {
//                Element contactCustomer = doc.createElement("cac:Contact");
//                partyCustomer.appendChild(contactCustomer);
//                Element electronicMail = doc.createElement("cbc:ElectronicMail");
//                contactCustomer.appendChild(electronicMail);
//                electronicMail.appendChild(doc.createTextNode(documento.getCliente().getEmail()));
//            }

            /*
             * BEGIN - Detraccion
             */
            if (documento.isSujetoSpot()) {
                /*
                 * 59. Número de cta. en el Banco de la Nación
                 */
                //Invoice/cac:PaymentMeans
                Element paymentMeans = doc.createElement("cac:PaymentMeans");
                invoice.appendChild(paymentMeans);
                //Invoice/cac:PaymentMeans/cbc:PaymentMeansCode (Medio de pago)
                Element paymentMeansCode = doc.createElement("cbc:PaymentMeansCode");
                paymentMeans.appendChild(paymentMeansCode);
                paymentMeansCode.setAttribute("listAgencyName", "PE:SUNAT");
                paymentMeansCode.setAttribute("listName", "Medio de pago");
                paymentMeansCode.setAttribute("listURI", "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo59");
                paymentMeansCode.appendChild(doc.createTextNode("003"));
                //Invoice/cac:PaymentMeans/cac:PayeeFinancialAccount
                Element payeeFinancialAccount = doc.createElement("cac:PayeeFinancialAccount");
                paymentMeans.appendChild(payeeFinancialAccount);
                //Invoice/cac:PaymentMeans/cac:PayeeFinancialAccount/cbc:ID (Número de cuenta)
                Element idPayeeFinancialAccount = doc.createElement("cbc:ID");
                payeeFinancialAccount.appendChild(idPayeeFinancialAccount);
                idPayeeFinancialAccount.setAttribute("schemeAgencyName", "PE:SUNAT");
                idPayeeFinancialAccount.setAttribute("schemeName", "Medio de pago");
                idPayeeFinancialAccount.setAttribute("schemeURI", "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo59");
                idPayeeFinancialAccount.appendChild(doc.createTextNode("0000529587"));

                /*
                 * 58. Código del Bien o Servicio Sujeto a Detracción
                 */
                //Invoice/cac:PaymentTerms
                Element paymentTerms = doc.createElement("cac:PaymentTerms");
                invoice.appendChild(paymentTerms);

                //Invoice/cac:PaymentTerms/cbc:PaymentMeansID (Código de bien o servicio)
                Element paymentMeansID = doc.createElement("cbc:PaymentMeansID");
                paymentTerms.appendChild(paymentMeansID);
                paymentMeansID.setAttribute("schemeAgencyName", "PE:SUNAT");
                paymentMeansID.setAttribute("schemeName", "Codigo de detraccion");
                paymentMeansID.setAttribute("schemeURI", "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo54");
                paymentMeansID.appendChild(doc.createTextNode(documento.getCodigoDetraccion()));

                /*
                 * 60. Monto y Porcentaje de la detracción
                 */
                //Invoice/cac:PaymentTerms/cbc:PaymentPercent (Tasa o porcentaje de detracción)
                Element paymentPercent = doc.createElement("cbc:PaymentPercent");
                paymentTerms.appendChild(paymentPercent);
                paymentPercent.appendChild(doc.createTextNode(documento.getPorcentajeDetraccion().toPlainString()));

                //Invoice/cac:PaymentTerms/cbc:Amount (Monto de detracción y Moneda)
                Element amountPaymentTerms = doc.createElement("cbc:Amount");
                paymentTerms.appendChild(amountPaymentTerms);
                amountPaymentTerms.setAttribute("currencyID", documento.getMoneda().getCodigoISO());
                amountPaymentTerms.appendChild(doc.createTextNode(documento.getImporteDetraccion().toPlainString()));
            }
            /*
             * END - Detraccion
             */

            /*
             * 40. Total de impuestos
             */
            //Invoice/cac:TaxTotal/cbc:TaxAmount @currencyID
            Element taxTotalInvoice = doc.createElement("cac:TaxTotal");
            invoice.appendChild(taxTotalInvoice);
            Element taxAmountInvoice = doc.createElement("cbc:TaxAmount");
            taxTotalInvoice.appendChild(taxAmountInvoice);
            taxAmountInvoice.setAttribute("currencyID", documento.getMoneda().getCodigoISO());
            taxAmountInvoice.appendChild(doc.createTextNode(documento.getIgv().toPlainString()));

            /*
             * INICIO 42. Total valor de venta inafectas
             */
            if (documento.getOperacionesInafectas().compareTo(BigDecimal.ZERO) > 0) {
                //Invoice/cac:TaxTotal/cac:TaxSubtotal
                Element taxSubtotalInvoice = doc.createElement("cac:TaxSubtotal");
                taxTotalInvoice.appendChild(taxSubtotalInvoice);

                //Invoice/cac:TaxTotal/cac:TaxSubtotal/cbc:TaxableAmount @currencyID (Total valor de venta gravadas IGV o IVAP y Moneda )
                Element taxableAmountInvoice = doc.createElement("cbc:TaxableAmount");
                taxSubtotalInvoice.appendChild(taxableAmountInvoice);
                taxableAmountInvoice.setAttribute("currencyID", documento.getMoneda().getCodigoISO());
                taxableAmountInvoice.appendChild(doc.createTextNode(documento.getOperacionesInafectas().toPlainString()));

                //Invoice/cac:TaxTotal/cac:TaxSubtotal/cbc:TaxAmount @currencyID (Importe total del IGV o IVAP, según corresponda)
                Element taxAmountSubtotalInvoice = doc.createElement("cbc:TaxAmount");
                taxSubtotalInvoice.appendChild(taxAmountSubtotalInvoice);
                taxAmountSubtotalInvoice.setAttribute("currencyID", documento.getMoneda().getCodigoISO());
                taxAmountSubtotalInvoice.appendChild(doc.createTextNode("0.00"));

                //Invoice/cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory
                Element taxCategorySubtotalInvoice = doc.createElement("cac:TaxCategory");
                taxSubtotalInvoice.appendChild(taxCategorySubtotalInvoice);

                //Invoice/cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory/cac:TaxScheme
                Element taxSchemeSubtotalInvoice = doc.createElement("cac:TaxScheme");
                taxCategorySubtotalInvoice.appendChild(taxSchemeSubtotalInvoice);

                //Invoice/cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory/cac:TaxScheme/cbc:ID (Código de tributo)
                Element idTaxSchemeSubtotalInvoice = doc.createElement("cbc:ID");
                taxSchemeSubtotalInvoice.appendChild(idTaxSchemeSubtotalInvoice);
                idTaxSchemeSubtotalInvoice.setAttribute("schemeAgencyName", "PE:SUNAT");
                idTaxSchemeSubtotalInvoice.setAttribute("schemeName", "Codigo de tributos");
                idTaxSchemeSubtotalInvoice.setAttribute("schemeURI", "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo05");
                idTaxSchemeSubtotalInvoice.appendChild(doc.createTextNode("9998"));

                //Invoice/cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory/cac:TaxScheme/cbc:Name (Nombre de tributo)
                Element nameTaxSchemeSubtotalInvoice = doc.createElement("cbc:Name");
                taxSchemeSubtotalInvoice.appendChild(nameTaxSchemeSubtotalInvoice);
                nameTaxSchemeSubtotalInvoice.appendChild(doc.createTextNode("INA"));

                //Invoice/cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory/cac:TaxScheme/cbc:TaxTypeCode (Código internacional de tributo)
                Element typeCodeTaxSchemeSubtotalInvoice = doc.createElement("cbc:TaxTypeCode");
                taxSchemeSubtotalInvoice.appendChild(typeCodeTaxSchemeSubtotalInvoice);
                typeCodeTaxSchemeSubtotalInvoice.appendChild(doc.createTextNode("FRE"));
            }
            /*
             * END 42. Total valor de venta inafectas
             */

            /*
             * INICIO 43. Total valor de venta exoneradas
             */
            if (documento.getOperacionesExoneradas().compareTo(BigDecimal.ZERO) > 0) {
                //Invoice/cac:TaxTotal/cac:TaxSubtotal
                Element taxSubtotalInvoice = doc.createElement("cac:TaxSubtotal");
                taxTotalInvoice.appendChild(taxSubtotalInvoice);

                //Invoice/cac:TaxTotal/cac:TaxSubtotal/cbc:TaxableAmount @currencyID (Total valor de venta gravadas IGV o IVAP y Moneda )
                Element taxableAmountInvoice = doc.createElement("cbc:TaxableAmount");
                taxSubtotalInvoice.appendChild(taxableAmountInvoice);
                taxableAmountInvoice.setAttribute("currencyID", documento.getMoneda().getCodigoISO());
                taxableAmountInvoice.appendChild(doc.createTextNode(documento.getOperacionesExoneradas().toPlainString()));

                //Invoice/cac:TaxTotal/cac:TaxSubtotal/cbc:TaxAmount @currencyID (Importe total del IGV o IVAP, según corresponda)
                Element taxAmountSubtotalInvoice = doc.createElement("cbc:TaxAmount");
                taxSubtotalInvoice.appendChild(taxAmountSubtotalInvoice);
                taxAmountSubtotalInvoice.setAttribute("currencyID", documento.getMoneda().getCodigoISO());
                taxAmountSubtotalInvoice.appendChild(doc.createTextNode("0.00"));

                //Invoice/cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory
                Element taxCategorySubtotalInvoice = doc.createElement("cac:TaxCategory");
                taxSubtotalInvoice.appendChild(taxCategorySubtotalInvoice);

                //Invoice/cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory/cac:TaxScheme
                Element taxSchemeSubtotalInvoice = doc.createElement("cac:TaxScheme");
                taxCategorySubtotalInvoice.appendChild(taxSchemeSubtotalInvoice);

                //Invoice/cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory/cac:TaxScheme/cbc:ID (Código de tributo)
                Element idTaxSchemeSubtotalInvoice = doc.createElement("cbc:ID");
                taxSchemeSubtotalInvoice.appendChild(idTaxSchemeSubtotalInvoice);
                idTaxSchemeSubtotalInvoice.setAttribute("schemeAgencyName", "PE:SUNAT");
                idTaxSchemeSubtotalInvoice.setAttribute("schemeName", "Codigo de tributos");
                idTaxSchemeSubtotalInvoice.setAttribute("schemeURI", "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo05");
                idTaxSchemeSubtotalInvoice.appendChild(doc.createTextNode("9997"));

                //Invoice/cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory/cac:TaxScheme/cbc:Name (Nombre de tributo)
                Element nameTaxSchemeSubtotalInvoice = doc.createElement("cbc:Name");
                taxSchemeSubtotalInvoice.appendChild(nameTaxSchemeSubtotalInvoice);
                nameTaxSchemeSubtotalInvoice.appendChild(doc.createTextNode("EXO"));

                //Invoice/cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory/cac:TaxScheme/cbc:TaxTypeCode (Código internacional de tributo)
                Element typeCodeTaxSchemeSubtotalInvoice = doc.createElement("cbc:TaxTypeCode");
                taxSchemeSubtotalInvoice.appendChild(typeCodeTaxSchemeSubtotalInvoice);
                typeCodeTaxSchemeSubtotalInvoice.appendChild(doc.createTextNode("VAT"));
            }
            /*
             * END 43. Total valor de venta exoneradas
             */

            /*
             * INICIO 44. Total valor de venta gratuitas
             */
            if (documento.getOperacionesGratuitas().compareTo(BigDecimal.ZERO) > 0) {
                //Invoice/cac:TaxTotal/cac:TaxSubtotal
                Element taxSubtotalInvoice = doc.createElement("cac:TaxSubtotal");
                taxTotalInvoice.appendChild(taxSubtotalInvoice);

                //Invoice/cac:TaxTotal/cac:TaxSubtotal/cbc:TaxableAmount @currencyID (Total valor de venta gravadas IGV o IVAP y Moneda )
                Element taxableAmountInvoice = doc.createElement("cbc:TaxableAmount");
                taxSubtotalInvoice.appendChild(taxableAmountInvoice);
                taxableAmountInvoice.setAttribute("currencyID", documento.getMoneda().getCodigoISO());
                taxableAmountInvoice.appendChild(doc.createTextNode(documento.getOperacionesGratuitas().toPlainString()));

                //Invoice/cac:TaxTotal/cac:TaxSubtotal/cbc:TaxAmount @currencyID (Importe total del IGV o IVAP, según corresponda)
                Element taxAmountSubtotalInvoice = doc.createElement("cbc:TaxAmount");
                taxSubtotalInvoice.appendChild(taxAmountSubtotalInvoice);
                taxAmountSubtotalInvoice.setAttribute("currencyID", documento.getMoneda().getCodigoISO());
                taxAmountSubtotalInvoice.appendChild(doc.createTextNode(documento.getImporteTributoVentaGratuita().toPlainString()));

                //Invoice/cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory
                Element taxCategorySubtotalInvoice = doc.createElement("cac:TaxCategory");
                taxSubtotalInvoice.appendChild(taxCategorySubtotalInvoice);

                //Invoice/cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory/cac:TaxScheme
                Element taxSchemeSubtotalInvoice = doc.createElement("cac:TaxScheme");
                taxCategorySubtotalInvoice.appendChild(taxSchemeSubtotalInvoice);

                //Invoice/cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory/cac:TaxScheme/cbc:ID (Código de tributo)
                Element idTaxSchemeSubtotalInvoice = doc.createElement("cbc:ID");
                taxSchemeSubtotalInvoice.appendChild(idTaxSchemeSubtotalInvoice);
                idTaxSchemeSubtotalInvoice.setAttribute("schemeAgencyName", "PE:SUNAT");
                idTaxSchemeSubtotalInvoice.setAttribute("schemeName", "Codigo de tributos");
                idTaxSchemeSubtotalInvoice.setAttribute("schemeURI", "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo05");
                idTaxSchemeSubtotalInvoice.appendChild(doc.createTextNode("9996"));

                //Invoice/cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory/cac:TaxScheme/cbc:Name (Nombre de tributo)
                Element nameTaxSchemeSubtotalInvoice = doc.createElement("cbc:Name");
                taxSchemeSubtotalInvoice.appendChild(nameTaxSchemeSubtotalInvoice);
                nameTaxSchemeSubtotalInvoice.appendChild(doc.createTextNode("GRA"));

                //Invoice/cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory/cac:TaxScheme/cbc:TaxTypeCode (Código internacional de tributo)
                Element typeCodeTaxSchemeSubtotalInvoice = doc.createElement("cbc:TaxTypeCode");
                taxSchemeSubtotalInvoice.appendChild(typeCodeTaxSchemeSubtotalInvoice);
                typeCodeTaxSchemeSubtotalInvoice.appendChild(doc.createTextNode("FRE"));
            }
            /*
             * END 44. Total valor de venta gratuitas
             */

            /*
             * INICIO 45. Total valor de venta gravadas IGV o IVAP
             */
            if (documento.getBaseImponible().compareTo(BigDecimal.ZERO) > 0) {
                //Invoice/cac:TaxTotal/cac:TaxSubtotal
                Element taxSubtotalInvoice = doc.createElement("cac:TaxSubtotal");
                taxTotalInvoice.appendChild(taxSubtotalInvoice);

                //Invoice/cac:TaxTotal/cac:TaxSubtotal/cbc:TaxableAmount @currencyID (Total valor de venta gravadas IGV o IVAP y Moneda )
                Element taxableAmountInvoice = doc.createElement("cbc:TaxableAmount");
                taxSubtotalInvoice.appendChild(taxableAmountInvoice);
                taxableAmountInvoice.setAttribute("currencyID", documento.getMoneda().getCodigoISO());
                taxableAmountInvoice.appendChild(doc.createTextNode(documento.getBaseImponible().toPlainString()));

                //Invoice/cac:TaxTotal/cac:TaxSubtotal/cbc:TaxAmount @currencyID (Importe total del IGV o IVAP, según corresponda)
                Element taxAmountSubtotalInvoice = doc.createElement("cbc:TaxAmount");
                taxSubtotalInvoice.appendChild(taxAmountSubtotalInvoice);
                taxAmountSubtotalInvoice.setAttribute("currencyID", documento.getMoneda().getCodigoISO());
                taxAmountSubtotalInvoice.appendChild(doc.createTextNode(documento.getIgv().toPlainString()));

                //Invoice/cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory
                Element taxCategorySubtotalInvoice = doc.createElement("cac:TaxCategory");
                taxSubtotalInvoice.appendChild(taxCategorySubtotalInvoice);

                //Invoice/cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory/cac:TaxScheme
                Element taxSchemeSubtotalInvoice = doc.createElement("cac:TaxScheme");
                taxCategorySubtotalInvoice.appendChild(taxSchemeSubtotalInvoice);

                //Invoice/cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory/cac:TaxScheme/cbc:ID (Código de tributo)
                Element idTaxSchemeSubtotalInvoice = doc.createElement("cbc:ID");
                taxSchemeSubtotalInvoice.appendChild(idTaxSchemeSubtotalInvoice);
                idTaxSchemeSubtotalInvoice.setAttribute("schemeAgencyName", "PE:SUNAT");
                idTaxSchemeSubtotalInvoice.setAttribute("schemeName", "Codigo de tributos");
                idTaxSchemeSubtotalInvoice.setAttribute("schemeURI", "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo05");
                idTaxSchemeSubtotalInvoice.appendChild(doc.createTextNode("1000"));

                //Invoice/cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory/cac:TaxScheme/cbc:Name (Nombre de tributo)
                Element nameTaxSchemeSubtotalInvoice = doc.createElement("cbc:Name");
                taxSchemeSubtotalInvoice.appendChild(nameTaxSchemeSubtotalInvoice);
                nameTaxSchemeSubtotalInvoice.appendChild(doc.createTextNode("IGV"));

                //Invoice/cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory/cac:TaxScheme/cbc:TaxTypeCode (Código internacional de tributo)
                Element typeCodeTaxSchemeSubtotalInvoice = doc.createElement("cbc:TaxTypeCode");
                taxSchemeSubtotalInvoice.appendChild(typeCodeTaxSchemeSubtotalInvoice);
                typeCodeTaxSchemeSubtotalInvoice.appendChild(doc.createTextNode("VAT"));
            }
            /*
             * END 45. Total valor de venta gravadas IGV o IVAP
             */

            //Invoice/cac:LegalMonetaryTotal
            Element legalMonetaryTotal = doc.createElement("cac:LegalMonetaryTotal");
            invoice.appendChild(legalMonetaryTotal);

            /*
             * 53. Total valor de venta
             */
            //Invoice/cac:LegalMonetaryTotal/cbc:LineExtensionAmount @currencyID (Total valor de venta y Moneda)
            Element lineExtensionAmount = doc.createElement("cbc:LineExtensionAmount");
            legalMonetaryTotal.appendChild(lineExtensionAmount);
            lineExtensionAmount.setAttribute("currencyID", documento.getMoneda().getCodigoISO());
            lineExtensionAmount.appendChild(doc.createTextNode((documento.getBaseImponible().add(documento.getOperacionesInafectas()).add(documento.getOperacionesExoneradas())).toPlainString()));

            /*
             * 54. Total precio de venta = 53 + impuestos
             */
            //Invoice/cac:LegalMonetaryTotal/cbc:TaxInclusiveAmount @currencyID (Total precio de venta y Moneda)
            Element taxInclusiveAmount = doc.createElement("cbc:TaxInclusiveAmount");
            legalMonetaryTotal.appendChild(taxInclusiveAmount);
            taxInclusiveAmount.setAttribute("currencyID", documento.getMoneda().getCodigoISO());
            taxInclusiveAmount.appendChild(doc.createTextNode(documento.getImporte().toPlainString()));

            /*
             * 52. IMPORTE TOTAL DEL COMPROBANTE = 54 + cargos - descuentos -
             * anticipos + redondeo
             */
            //Invoice/cac:LegalMonetaryTotal/cbc:PayableAmount @currencyID (Importe total del comprobante y Moneda)
            Element payableAmount = doc.createElement("cbc:PayableAmount");
            legalMonetaryTotal.appendChild(payableAmount);
            payableAmount.setAttribute("currencyID", documento.getMoneda().getCodigoISO());
            payableAmount.appendChild(doc.createTextNode(documento.getImporte().toPlainString()));

            int contadorDetalle = 0;
            for (DetalleDocumento detalle : documento.getDetalleDocumentoList()) {
                contadorDetalle++;
                /*
                 * 24. Número de orden del Ítem
                 */
                //Invoice/cac:InvoiceLine/cbc:ID
                Element invoiceLine = doc.createElement("cac:InvoiceLine");
                invoice.appendChild(invoiceLine);
                Element idInvoiceLine = doc.createElement("cbc:ID");
                invoiceLine.appendChild(idInvoiceLine);
                idInvoiceLine.appendChild(doc.createTextNode(String.valueOf(contadorDetalle)));

                /*
                 * 25. Unidad de medida y cantidad del ítem
                 */
                //Invoice/cac:InvoiceLine/cbc:Note (Descripción de la unidad)
                Element noteInvoiceLine = doc.createElement("cbc:Note");
                invoiceLine.appendChild(noteInvoiceLine);
                noteInvoiceLine.appendChild(doc.createTextNode(detalle.getArticulo().getUnidadMedida().getNombreCorto()));
                //Invoice/cac:InvoiceLine/cbc:InvoicedQuantity (Cantidad del item)
                Element invoicedQuantity = doc.createElement("cbc:InvoicedQuantity");
                invoiceLine.appendChild(invoicedQuantity);
                invoicedQuantity.setAttribute("unitCode", detalle.getArticulo().getUnidadMedida().getCodigoUNECE());
                invoicedQuantity.setAttribute("unitCodeListID", "UN/ECE rec 20");
                invoicedQuantity.setAttribute("unitCodeListAgencyName", "United Nations Economic Commission for Europe");
                invoicedQuantity.appendChild(doc.createTextNode(detalle.getCantidad().toPlainString()));

                /*
                 * 37. Valor de venta del ítem
                 */
                //Invoice/cac:InvoiceLine/cbc:LineExtensionAmount @currencyID (Valor de venta del ítem y Moneda)
                Element lineExtensionAmountInvoiceLine = doc.createElement("cbc:LineExtensionAmount");
                invoiceLine.appendChild(lineExtensionAmountInvoiceLine);
                lineExtensionAmountInvoiceLine.setAttribute("currencyID", documento.getMoneda().getCodigoISO());
                lineExtensionAmountInvoiceLine.appendChild(doc.createTextNode((detalle.getTipoAfectacionIGV().isGratuito() ? detalle.getImporte() : detalle.getImporte().divide(BigDecimal.ONE.add(detalle.getTipoAfectacionIGV().getPorcentaje()), 2, RoundingMode.HALF_UP)).toPlainString()));

                /*
                 * 31. Precio de venta unitario del ítem
                 */
                //Invoice/cac:InvoiceLine/cac:PricingReference/cac:AlternativeConditionPrice/cbc:PriceAmount @currencyID (Precio de venta unitario del ítem y Moneda)
                Element pricingReference = doc.createElement("cac:PricingReference");
                invoiceLine.appendChild(pricingReference);
                Element alternativeConditionPrice = doc.createElement("cac:AlternativeConditionPrice");
                pricingReference.appendChild(alternativeConditionPrice);
                Element priceAmountReference = doc.createElement("cbc:PriceAmount");
                alternativeConditionPrice.appendChild(priceAmountReference);
                priceAmountReference.setAttribute("currencyID", documento.getMoneda().getCodigoISO());
                priceAmountReference.appendChild(doc.createTextNode(detalle.getTipoAfectacionIGV().isGratuito() ? "0.00" : detalle.getPrecio().toPlainString()));
                Element priceTypeCode = doc.createElement("cbc:PriceTypeCode");
                alternativeConditionPrice.appendChild(priceTypeCode);
                priceTypeCode.setAttribute("listName", "Tipo de Precio");
                priceTypeCode.setAttribute("listAgencyName", "PE:SUNAT");
                priceTypeCode.setAttribute("listURI", "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo16");
                priceTypeCode.appendChild(doc.createTextNode("01"));

                if (detalle.getTipoAfectacionIGV().isGratuito()) {
                    Element alternativeConditionPriceGratuito = doc.createElement("cac:AlternativeConditionPrice");
                    pricingReference.appendChild(alternativeConditionPriceGratuito);
                    Element priceAmountGratuito = doc.createElement("cbc:PriceAmount");
                    alternativeConditionPriceGratuito.appendChild(priceAmountGratuito);
                    priceAmountGratuito.setAttribute("currencyID", documento.getMoneda().getCodigoISO());
                    priceAmountGratuito.appendChild(doc.createTextNode(detalle.getPrecio().toPlainString()));
                    Element priceTypeCodeGratuito = doc.createElement("cbc:PriceTypeCode");
                    alternativeConditionPriceGratuito.appendChild(priceTypeCodeGratuito);
                    priceTypeCodeGratuito.setAttribute("listName", "Tipo de Precio");
                    priceTypeCodeGratuito.setAttribute("listAgencyName", "PE:SUNAT");
                    priceTypeCodeGratuito.setAttribute("listURI", "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo16");
                    priceTypeCodeGratuito.appendChild(doc.createTextNode("02"));
                }

                /*
                 * 33. Monto total impuestos del ítem
                 */
                //Invoice/cac:InvoiceLine/cac:TaxTotal/cbc:TaxAmount @currencyID (Monto total impuestos del ítem y Moneda)
                Element taxTotal = doc.createElement("cac:TaxTotal");
                invoiceLine.appendChild(taxTotal);
                Element taxAmount = doc.createElement("cbc:TaxAmount");
                taxTotal.appendChild(taxAmount);
                taxAmount.setAttribute("currencyID", documento.getMoneda().getCodigoISO());
                taxAmount.appendChild(doc.createTextNode((detalle.getTipoAfectacionIGV().isGratuito() ? "0.00" : detalle.getImporte().subtract(detalle.getImporte().divide(BigDecimal.ONE.add(detalle.getTipoAfectacionIGV().getPorcentaje()), 2, RoundingMode.HALF_UP)).toPlainString())));

                /*
                 * 34. Monto del IGV o IVAP del ítem
                 */
                //Invoice/cac:InvoiceLine/cac:TaxTotal/cac:TaxSubtotal
                Element taxSubtotal = doc.createElement("cac:TaxSubtotal");
                taxTotal.appendChild(taxSubtotal);
                //Invoice/cac:InvoiceLine/cac:TaxTotal/cac:TaxSubtotal/cbc:TaxableAmount @currencyID (Monto base IGV o IVAP del item y Moneda)
                Element taxableAmount = doc.createElement("cbc:TaxableAmount");
                taxSubtotal.appendChild(taxableAmount);
                taxableAmount.setAttribute("currencyID", documento.getMoneda().getCodigoISO());
                taxableAmount.appendChild(doc.createTextNode((detalle.getTipoAfectacionIGV().isGratuito() ? detalle.getImporte() : detalle.getImporte().divide(BigDecimal.ONE.add(detalle.getTipoAfectacionIGV().getPorcentaje()), 2, RoundingMode.HALF_UP)).toPlainString()));
                //Invoice/cac:InvoiceLine/cac:TaxTotal/cac:TaxSubtotal/cbc:TaxAmount @currencyID (Monto del IGV o IVAP del ítem y Moneda)
                Element taxAmountSubtotal = doc.createElement("cbc:TaxAmount");
                taxSubtotal.appendChild(taxAmountSubtotal);
                taxAmountSubtotal.setAttribute("currencyID", documento.getMoneda().getCodigoISO());
                taxAmountSubtotal.appendChild(doc.createTextNode((detalle.getTipoAfectacionIGV().isGratuito() ? detalle.getImporte().multiply(detalle.getTipoAfectacionIGV().getPorcentaje()).setScale(2, RoundingMode.HALF_UP) : detalle.getImporte().subtract(detalle.getImporte().divide(BigDecimal.ONE.add(detalle.getTipoAfectacionIGV().getPorcentaje()), 2, RoundingMode.HALF_UP))).toPlainString()));

                //Invoice/cac:InvoiceLine/cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory
                Element taxCategory = doc.createElement("cac:TaxCategory");
                taxSubtotal.appendChild(taxCategory);

                //Invoice/cac:InvoiceLine/cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory/cbc:Percent (Porcentaje IGV o IVAP del item)
                Element percent = doc.createElement("cbc:Percent");
                taxCategory.appendChild(percent);
                percent.appendChild(doc.createTextNode((detalle.getTipoAfectacionIGV().getPorcentaje().multiply(new BigDecimal("100")).setScale(2, RoundingMode.UP)).toPlainString()));

                //Invoice/cac:InvoiceLine/cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory/cbc:TaxExemptionReasonCode ( Afectación IGV o IVAP del item)
                Element taxExemptionReasonCode = doc.createElement("cbc:TaxExemptionReasonCode");
                taxCategory.appendChild(taxExemptionReasonCode);
                taxExemptionReasonCode.setAttribute("listAgencyName", "PE:SUNAT");
                taxExemptionReasonCode.setAttribute("listName", "Afectacion del IGV");
                taxExemptionReasonCode.setAttribute("listURI", "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo07");
                taxExemptionReasonCode.appendChild(doc.createTextNode(detalle.getTipoAfectacionIGV().getCodigo()));
                //Invoice/cac:InvoiceLine/cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory/cac:TaxScheme
                Element taxScheme = doc.createElement("cac:TaxScheme");
                taxCategory.appendChild(taxScheme);
                //Invoice/cac:InvoiceLine/cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory/cac:TaxScheme/cbc:ID (Código de tributo del ítem)
                Element idTaxScheme = doc.createElement("cbc:ID");
                taxScheme.appendChild(idTaxScheme);
                idTaxScheme.setAttribute("schemeAgencyName", "PE:SUNAT");
                idTaxScheme.setAttribute("schemeName", "Codigo de tributos");
                idTaxScheme.setAttribute("schemeURI", "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo05");
                idTaxScheme.appendChild(doc.createTextNode(detalle.getTipoAfectacionIGV().getCodigoTributo()));
                //Invoice/cac:InvoiceLine/cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory/cac:TaxScheme/cbc:Name
                Element nameTaxScheme = doc.createElement("cbc:Name");
                taxScheme.appendChild(nameTaxScheme);
                nameTaxScheme.appendChild(doc.createTextNode(detalle.getTipoAfectacionIGV().getNombreTributo()));
                //Invoice/cac:InvoiceLine/cac:TaxTotal/cac:TaxSubtotal/cac:TaxCategory/cac:TaxScheme/cbc:TaxTypeCode (Código internacional de tributo)
                Element taxTypeCode = doc.createElement("cbc:TaxTypeCode");
                taxScheme.appendChild(taxTypeCode);
                taxTypeCode.appendChild(doc.createTextNode(detalle.getTipoAfectacionIGV().getCodigoTributoInternacional()));

                //Invoice/cac:InvoiceLine/cac:Item/cac:SellersItemIdentification/cbc:ID
                Element itemInvoiceLine = doc.createElement("cac:Item");
                invoiceLine.appendChild(itemInvoiceLine);

                /*
                 * 29. Descripcion del item
                 */
                //Invoice/cac:InvoiceLine/cac:Item/cbc:Description
                Element descriptionSellersItemIdentification = doc.createElement("cbc:Description");
                itemInvoiceLine.appendChild(descriptionSellersItemIdentification);
                descriptionSellersItemIdentification.appendChild(doc.createTextNode(detalle.getArticulo().getDescripcion()));

                /*
                 * 26. Código de producto
                 */
                //Invoice/cac:InvoiceLine/cac:Item/cac:SellersItemIdentification/cbc:ID
                Element sellersItemIdentification = doc.createElement("cac:SellersItemIdentification");
                itemInvoiceLine.appendChild(sellersItemIdentification);
                Element idSellersItemIdentification = doc.createElement("cbc:ID");
                sellersItemIdentification.appendChild(idSellersItemIdentification);
                idSellersItemIdentification.appendChild(doc.createTextNode(detalle.getArticulo().getCodigo()));

                /*
                 * 30. Valor unitario del ítem
                 */
                //Invoice/cac:InvoiceLine/cac:Price/cbc:PriceAmount @currencyID (Valor unitario del ítem y Moneda)
                Element price = doc.createElement("cac:Price");
                invoiceLine.appendChild(price);
                Element priceAmount = doc.createElement("cbc:PriceAmount");
                price.appendChild(priceAmount);
                priceAmount.setAttribute("currencyID", documento.getMoneda().getCodigoISO());
                priceAmount.appendChild(doc.createTextNode(detalle.getTipoAfectacionIGV().isGratuito() ? "0.00" : (detalle.getPrecio().divide(BigDecimal.ONE.add(detalle.getTipoAfectacionIGV().getPorcentaje()), 4, RoundingMode.HALF_UP)).toPlainString()));
            }

            FileOutputStream fileOutputStream = new FileOutputStream(xmlFile);
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

            StreamResult streamResult = new StreamResult(fileOutputStream);
            transformer.transform(new DOMSource(doc), streamResult);
            streamResult.getOutputStream().close();

        } catch (IOException | IllegalArgumentException | ParserConfigurationException | TransformerException | DOMException ex) {
            Log.e("EFACT", ex.getLocalizedMessage(), ex);
            throw new FEException("Ha ocurrido un error al generar el archivo XML.", ex);
        } catch (Exception ex) {
            Log.e("EFACT", ex.getLocalizedMessage(), ex);
            throw new FEException("Ha ocurrido un error inesperado al generar el archivo XML.", ex);
        }
    }

}
