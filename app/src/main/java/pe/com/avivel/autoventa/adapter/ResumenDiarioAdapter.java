package pe.com.avivel.autoventa.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import pe.com.avivel.autoventa.R;
import pe.com.avivel.autoventa.model.Documento;
import pe.com.avivel.autoventa.utils.Numero;

public class ResumenDiarioAdapter extends ArrayAdapter<Documento> {

    StringBuilder stringBuilder;

    public ResumenDiarioAdapter(final Context context, ArrayList<Documento> documentoArrayList) {
        super(context, 0, documentoArrayList);
        stringBuilder = new StringBuilder();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        final Documento documento = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_resumendiario, parent, false);
        }

        // número
        TextView tvItemDocNumero = convertView.findViewById(R.id.tvItemResDiaNumero);
        tvItemDocNumero.setText(documento.toString());

        // cliente
        TextView tvItemDocCliente = convertView.findViewById(R.id.tvItemResDiaCliente);
        String razonSocialFacturado = documento.getCliente().getRazonSocial();
        razonSocialFacturado = razonSocialFacturado.length() > 50 ? razonSocialFacturado.substring(0, 50) : razonSocialFacturado;
        tvItemDocCliente.setText(razonSocialFacturado);

        // importe
        TextView tvItemResDiaImporte = convertView.findViewById(R.id.tvItemResDiaImporte);
        stringBuilder.setLength(0);
        stringBuilder.append(documento.getMoneda().getSimbolo());
        stringBuilder.append(" ");
        stringBuilder.append(Numero.formatear(documento.getImporte()));
        tvItemResDiaImporte.setText(stringBuilder.toString());

        return convertView;
    }

}
