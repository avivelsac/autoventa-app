package pe.com.avivel.autoventa.activity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;

import pe.com.avivel.autoventa.R;
import pe.com.avivel.autoventa.helpers.ConfiguracionDbHelper;
import pe.com.avivel.autoventa.model.Configuracion;

public class ConfiguracionActivity extends AppCompatActivity {

    private EditText txtUtrl;
    private EditText txtFechaTrabajo;
    private EditText txSerieBoletas;
    private EditText txSerieFacturas;
    private Context context;

    private ConfiguracionDbHelper configuracionDbHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracion);
        context = this;

        configuracionDbHelper = ConfiguracionDbHelper.getInstance(this);

        Button btnGuardar = findViewById(R.id.btnGuardar);
        txtUtrl = findViewById(R.id.txtUrl);
        txtFechaTrabajo = findViewById(R.id.txtFechaTrabajo);
        txSerieBoletas = findViewById(R.id.txSerieBoletas);
        txSerieFacturas = findViewById(R.id.txSerieFacturas);

        getData();

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                configuracionDbHelper.deleteAllConfiguracion();

                Configuracion configuracion = new Configuracion();
                configuracion.setFechaTrabajo(txtFechaTrabajo.getText().toString());
                configuracion.setUrlServidor(txtUtrl.getText().toString());
                configuracion.setSerieBoleta(txSerieBoletas.getText().toString());
                configuracion.setSerieFactura(txSerieFacturas.getText().toString());

                long rs = configuracionDbHelper.insertConfiguracion(configuracion);

                if (rs > 0) {
                    Toast.makeText(context, "Datos guardados existosamente.", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(context, "Se produjo un error al guardar los datos.", Toast.LENGTH_LONG).show();
                }
            }
        });
        txtFechaTrabajo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FechaTrabajoDatePicker();
            }
        });
    }

    private void getData() {
        Configuracion configuracion = configuracionDbHelper.getConfiguracion();
        if (configuracion != null) {
            txtUtrl.setText(configuracion.getUrlServidor());
            txtFechaTrabajo.setText(configuracion.getFechaTrabajo());
            txSerieBoletas.setText(configuracion.getSerieBoleta());
            txSerieFacturas.setText(configuracion.getSerieFactura());
        }
    }

    private void FechaTrabajoDatePicker() {
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        final DatePickerDialog datepicture = new DatePickerDialog(ConfiguracionActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String _month = ("00" + (month + 1));
                String day = "00" + dayOfMonth;
                String fecha = day.substring(day.length() - 2) + "/" + _month.substring(_month.length() - 2) + "/" + year;
                txtFechaTrabajo.setText(fecha);
            }
        }, year, month, day);
        datepicture.setTitle("Seleccione una fecha");
        datepicture.setButton(DialogInterface.BUTTON_POSITIVE, "Aceptar", datepicture);
        datepicture.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                datepicture.dismiss();
            }
        });
        datepicture.show();
    }

}
