package pe.com.avivel.autoventa.model;

import org.simpleframework.xml.Element;

@Element
public class TipoDocumentoEntity {

    @Element
    private Integer id;

    public TipoDocumentoEntity(TipoDocumento tipoDocumento) {
        this.id = tipoDocumento.getId();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
