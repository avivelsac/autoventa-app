package pe.com.avivel.autoventa.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import pe.com.avivel.autoventa.R;
import pe.com.avivel.autoventa.fe.FacturadorEfact;
import pe.com.avivel.autoventa.model.Documento;
import pe.com.avivel.autoventa.print.GeneradorComprobante;
import pe.com.avivel.autoventa.service.EfactIntentService;
import pe.com.avivel.autoventa.service.EfactResultReceiver;
import pe.com.avivel.autoventa.utils.Fecha;
import pe.com.avivel.autoventa.utils.Numero;

import static pe.com.avivel.autoventa.service.EfactIntentService.STATUS_ERROR;
import static pe.com.avivel.autoventa.service.EfactIntentService.STATUS_FINISHED;
import static pe.com.avivel.autoventa.service.EfactIntentService.STATUS_RUNNING;

public class DocumentoAdapter extends ArrayAdapter<Documento> implements EfactResultReceiver.Receiver {

    final Intent efactIntentService;
    StringBuilder stringBuilder;
    private EfactResultReceiver mReceiver;

    public DocumentoAdapter(final Context context, ArrayList<Documento> documentoArrayList) {
        super(context, 0, documentoArrayList);
        stringBuilder = new StringBuilder();

        mReceiver = new EfactResultReceiver(new Handler());
        mReceiver.setReceiver(this);

        efactIntentService = new Intent(Intent.ACTION_SYNC, null, context, EfactIntentService.class);
        efactIntentService.putExtra("receiver", mReceiver);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        final Documento documento = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_documento, parent, false);
        }

        ImageButton imageButton = convertView.findViewById(R.id.imagePrint);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String situacion = documento.getSituacion();
                    switch (String.valueOf(situacion)) {
                        case FacturadorEfact.SITUACION_ENVIADO_ACEPTADO:
                            // Imprimir
                            Log.i("EFACT", "Imprime...");
                            if (GeneradorComprobante.getInstance().generar(documento)) {
                                Toast.makeText(getContext(), "El comprobante fue impreso satisfactoriamente.", Toast.LENGTH_SHORT).show();
                            }
                            break;
                        case FacturadorEfact.SITUACION_NO_ENVIADO:
                            Toast.makeText(getContext(), "El comprobante aún no se ha enviado.\nSituación: " + documento.getSituacion() + "\nObservación: " + documento.getObservaciones(), Toast.LENGTH_LONG).show();
                            break;
                        case FacturadorEfact.SITUACION_ENVIADO:
                        case FacturadorEfact.SITUACION_ENVIADO_VALIDACION:
                            Log.i("EFACT", "entra a enviado crea service...");
                            efactIntentService.putExtra("command", "getcdr&print");
                            efactIntentService.putExtra("id", documento.getId());
                            getContext().startService(efactIntentService);
                            break;
                        default:
                            Toast.makeText(getContext(), "No se puede tener obtener la representación impresa.\nSituación: " + documento.getSituacion() + "\nObservación: " + documento.getObservaciones(), Toast.LENGTH_LONG).show();
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        // número
        TextView tvItemDocNumero = convertView.findViewById(R.id.tvItemDocNumero);
        stringBuilder.setLength(0);
        stringBuilder.append(documento.toString());
        tvItemDocNumero.setText(stringBuilder.toString());

        // cliente
        TextView tvItemDocCliente = convertView.findViewById(R.id.tvItemDocCliente);
        stringBuilder.setLength(0);
        String razonSocialFacturado = documento.getCliente().getRazonSocial();
        razonSocialFacturado = razonSocialFacturado.length() > 50 ? razonSocialFacturado.substring(0, 50) : razonSocialFacturado;
        String razonSocialReal = documento.getClienteDestino().getRazonSocial();
        razonSocialReal = razonSocialReal.length() > 50 ? razonSocialReal.substring(0, 50) : razonSocialReal;
        stringBuilder.append(razonSocialReal.equals(razonSocialFacturado) ? razonSocialFacturado : razonSocialFacturado + " (" + razonSocialReal + ")");
        tvItemDocCliente.setText(stringBuilder.toString());

        // situacion
        TextView tvItemDocSituacion = convertView.findViewById(R.id.tvItemDocSituacion);
        stringBuilder.setLength(0);
        stringBuilder.append("Situación: ");
        stringBuilder.append(documento.getSituacion());
        stringBuilder.append(" ");
        stringBuilder.append(documento.getFechaEnvioXml() == null ? "" : Fecha.getTimeAgo(documento.getFechaEnvioXml().getTime()));
        tvItemDocSituacion.setText(stringBuilder.toString());

        // importe
        TextView tvItemDocImporte = convertView.findViewById(R.id.tvItemDocImporte);
        stringBuilder.setLength(0);
        stringBuilder.append(documento.getMoneda().getSimbolo());
        stringBuilder.append(" ");
        stringBuilder.append(Numero.formatear(documento.getImporte()));
        tvItemDocImporte.setText(stringBuilder.toString());

        // situacion2
        TextView tvItemDocSituacion2 = convertView.findViewById(R.id.tvItemDocSituacion2);
        stringBuilder.setLength(0);
        stringBuilder.append(documento.getSituacionAvivel() == null ? "" : documento.getSituacionAvivel());
        tvItemDocSituacion2.setText(stringBuilder.toString());

        TextView tvItemDocId = convertView.findViewById(R.id.tvItemDocId);
        tvItemDocId.setText(String.valueOf(documento.getId()));

        return convertView;
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case STATUS_RUNNING:
                //show progress
                Toast running = Toast.makeText(getContext(), "Preparando para imprimir...", Toast.LENGTH_SHORT);
                running.show();
                break;
            case STATUS_FINISHED:
                String command = resultData.getString("command");
                if (resultData.getBoolean("result")) {
                    Toast.makeText(getContext(), "El comprobante fue impreso satisfactoriamente.", Toast.LENGTH_SHORT).show();
                }
                break;
            case STATUS_ERROR:
                // handle the error;
                String e = resultData.getString(Intent.EXTRA_TEXT);
                Toast error = Toast.makeText(getContext(), "STATUS_ERROR: " + e, Toast.LENGTH_SHORT);
                error.show();
                break;
        }
    }
}
