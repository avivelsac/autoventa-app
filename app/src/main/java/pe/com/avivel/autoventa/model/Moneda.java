package pe.com.avivel.autoventa.model;

/**
 *
 * @author André Bedregal <andre.bedregal at avivel.com.pe>
 */
public enum Moneda {

    MN(1, "MN", "PEN", "S/ ", "NUEVOS SOLES"),
    US(2, "US", "USD", "US$", "DÓLARES AMERICANOS");

    private final Integer id;
    private final String codigo;
    private final String codigoISO;
    private final String simbolo;
    private final String descripcion;

    private Moneda(Integer id, String codigo, String codigoISO, String simbolo, String descripcion) {
        this.id = id;
        this.codigo = codigo;
        this.codigoISO = codigoISO;
        this.simbolo = simbolo;
        this.descripcion = descripcion;
    }

    public Integer getId() {
        return id;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getCodigoISO() {
        return codigoISO;
    }

    public String getSimbolo() {
        return simbolo;
    }

    public String getDescripcion() {
        return descripcion;
    }

}
