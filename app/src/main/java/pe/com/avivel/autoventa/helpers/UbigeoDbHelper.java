package pe.com.avivel.autoventa.helpers;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class UbigeoDbHelper extends FacturacionHelper {

    private static UbigeoDbHelper instance;

    public static String TABLE_NAME = "ubigeo";
    public static final String COLUMN_NAME_ID = "id";
    public static final String COLUMN_NAME_DISTRITO = "distrito";
    public static final String COLUMN_NAME_PROVINCIA = "provincia";
    public static final String COLUMN_NAME_DEPARTAMENTO = "departamento";

    private UbigeoDbHelper(Context context) {
        super(context);
    }

    public static UbigeoDbHelper getInstance(Context context) {
        if (instance == null) {
            instance = new UbigeoDbHelper(context);
        }
        return instance;
    }

    public static void createTable(SQLiteDatabase db) {
        String query = "create table "+ TABLE_NAME +"( "+
                COLUMN_NAME_ID+" integer primary key AUTOINCREMENT, "+
                COLUMN_NAME_DISTRITO+" text null, "+
                COLUMN_NAME_PROVINCIA+" text null, "+
                COLUMN_NAME_DEPARTAMENTO+" text null "+
                ")";
        db.execSQL(query);
    }

    public static void dropTable(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS "+ TABLE_NAME);
    }

    public static void populateTable() {
    }

    public List<String> getDepartamentos() {
        List<String> list = new ArrayList<>();
        open();
        Cursor cursor = getDatabase().rawQuery("SELECT DISTINCT ubigeo_departamento FROM ubigeo", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();
        close();
        return list;
    }

    public List<String> getProvinciasByDepartamento(String departamento) {
        List<String> list = new ArrayList<>();
        open();
        Cursor cursor = getDatabase().query(true,"ubigeo", new String[]{"ubigeo_provincia"}, "ubigeo_departamento = ?", new String[]{departamento}, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();
        close();
        return list;
    }

    public List<String> getDistritosByDepartamentoAndProvincia(String departamento, String provincia) {
        List<String> list = new ArrayList<>();
        open();
        Cursor cursor = getDatabase().query(true,"ubigeo", new String[]{"ubigeo_distrito"}, "ubigeo_departamento = ? AND ubigeo_provincia = ?", new String[]{departamento, provincia}, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            list.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();
        close();
        return list;
    }

}
