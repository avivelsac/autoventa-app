package pe.com.avivel.autoventa.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import pe.com.avivel.autoventa.R;
import pe.com.avivel.autoventa.model.Articulo;

public class ArticuloAdapter extends ArrayAdapter<Articulo> {

    private StringBuilder stringBuilder = new StringBuilder();

    public ArticuloAdapter(Context context, ArrayList<Articulo> articulos) {
        super(context, 0, articulos);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Articulo articulo = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_servicio, parent, false);
        }

        TextView tvCodigo = convertView.findViewById(R.id.tvCodigoArtLV);
        tvCodigo.setText(articulo.getCodigo());

        TextView tvName = convertView.findViewById(R.id.tvDescripcionArtLV);
        tvName.setText(articulo.getDescripcion());

        TextView tvUnidadesArtLV = convertView.findViewById(R.id.tvUnidadesArtLV);
        tvUnidadesArtLV.setText(articulo.getUnidadMedida().getDescripcion());

        TextView tvId = convertView.findViewById(R.id.txtId);
        tvId.setText(Integer.toString(articulo.getId()));

        return convertView;
    }
}
