package pe.com.avivel.autoventa.print;

import android.util.Log;

import java.math.BigDecimal;

import pe.com.avivel.autoventa.model.DetalleDocumento;
import pe.com.avivel.autoventa.model.Documento;
import pe.com.avivel.autoventa.print.exceptions.PrinterException;
import pe.com.avivel.autoventa.utils.ConvertidorNumeroToLetras;
import pe.com.avivel.autoventa.utils.Numero;
import pe.com.avivel.autoventa.utils.PlainText;
import pe.com.avivel.autoventa.utils.Utils;

public class GeneradorComprobante {

    private static GeneradorComprobante instance;
    private final int ANCHO_RECIBO = 48;
    private final Printer printer;
    private PrinterService printerService;

    private GeneradorComprobante() {
        printer = new BluetoothPrinter();
    }

    public static GeneradorComprobante getInstance() {
        if (instance == null) {
            instance = new GeneradorComprobante();
        }
        return instance;
    }

    public boolean generar(Documento documento) throws PrinterException {
        boolean success = false;
        String OPERACIONES_GRAVADAS = "Op. Gravadas  : ";
        String OPERACIONES_INAFECTAS = "Op. Inafectas : ";
        String OPERACIONES_EXONERADAS = "Op. Exoneradas: ";
        String OPERACIONES_GRATUITAS = "Op. Gratuitas : ";
        String IGV = "IGV           : ";
        String IMPORTE_TOTAL = "Importe total : ";

        try {

            printerService = PrinterService.getInstance(printer);

            printerService.lineBreak(2);

            printerService.setTextAlignCenter();

            printerService.setTextSize2H();

            printerService.printLn("AVIVEL SAC");

            printerService.setTextSizeNormal();
            printerService.printLn("RUC 20524088810");

            printerService.lineBreak();

            printerService.printLn("CA. LOS NOGALES MZ F LT 14");
            printerService.printLn("ASOC. HUERTOS DE VILLENA - PACHACÁMAC - LIMA");
            printerService.printLn("TELÉFONO: 660-2112");

            printerService.lineBreak();

            printerService.setTextTypeBold();
            printerService.printLn(documento.getTipoDocumento().getDescripcion());
            printerService.printLn(documento.getNumeroConSerie());
            printerService.setTextTypeNormal();

            printerService.setTextAlignLeft();

            printerService.lineBreak();

            printerService.printLn("Fecha  : " + Utils.fromDateToStringPrintable(documento.getFechaEmision()));
            printerService.printLn("Tip.Doc: " + documento.getCliente().getTipoDocumentoIdentidad().name());
            printerService.printLn("Num.Doc: " + documento.getCliente().getNumeroDocumento());
            printerService.printLn("Cliente: " + documento.getCliente().getRazonSocial());
            printerService.printLn(PlainText.insert("-", ANCHO_RECIBO));

            printerService.lineBreak();

            printerService.printLn("Código" + PlainText.insert(" ", 8) + "U.M  Artículo");
            printerService.printLn(PlainText.insert(" ", 22) + "Cantidad  Precio   Importe");

            printerService.lineBreak();

            for (DetalleDocumento detalle : documento.getDetalleDocumentoList()) {
                printerService.printLn(detalle.getArticulo().getCodigo() + "  " + detalle.getArticulo().getUnidadMedida().name() + "  " + detalle.getArticulo().getDescripcion() + " (" + detalle.getUnidades() + ") ");
                printerService.printLn(PlainText.insert(" ", 22) +
                        PlainText.alinearDerecha(Numero.formatear(detalle.getCantidad()), 8) + "  " +
                        PlainText.alinearDerecha(Numero.formatear(detalle.getPrecio()), 6) + "  " +
                        PlainText.alinearDerecha(Numero.formatear(detalle.getImporte()), 8));

                printerService.lineBreak();
            }

            printerService.printLn(OPERACIONES_GRAVADAS + PlainText.insert(" ", 37 - OPERACIONES_GRAVADAS.length()) + "S/ " + PlainText.alinearDerecha(Numero.formatear(documento.getBaseImponible()), 8));
            printerService.printLn(OPERACIONES_INAFECTAS + PlainText.insert(" ", 37 - OPERACIONES_INAFECTAS.length()) + "S/ " + PlainText.alinearDerecha(Numero.formatear(BigDecimal.ZERO), 8));
            printerService.printLn(OPERACIONES_EXONERADAS + PlainText.insert(" ", 37 - OPERACIONES_EXONERADAS.length()) + "S/ " + PlainText.alinearDerecha(Numero.formatear(BigDecimal.ZERO), 8));
            printerService.printLn(OPERACIONES_GRATUITAS + PlainText.insert(" ", 37 - OPERACIONES_GRATUITAS.length()) + "S/ " + PlainText.alinearDerecha(Numero.formatear(BigDecimal.ZERO), 8));
            printerService.printLn(IGV + PlainText.insert(" ", 37 - IGV.length()) + "S/ " + PlainText.alinearDerecha(Numero.formatear(documento.getIgv()), 8));
            printerService.printLn(IMPORTE_TOTAL + PlainText.insert(" ", 37 - IMPORTE_TOTAL.length()) + "S/ " + PlainText.alinearDerecha(Numero.formatear(documento.getImporte()), 8));

            printerService.lineBreak();

            printerService.printLn("Son: " + ConvertidorNumeroToLetras.convierte(Numero.formatear(documento.getImporte(), 2), ConvertidorNumeroToLetras.Tipo.Pronombre, documento.getMoneda().getCodigo()));
            printerService.printLn("VENTA REALIZADA POR EMISOR ITINERANTE");

            printerService.lineBreak();

            printerService.setTextAlignCenter();

            printerService.printQRCode(documento.getQrCode());

            printerService.lineBreak();

            printerService.printLn("Representación impresa de la");
            printerService.printLn(documento.getTipoDocumento().getDescripcion());
            printerService.printLn("Consulte el documento ingresando a");
            printerService.printLn("https://www.efact.pe/");
            printerService.printLn("Autorizado mediante resolución");
            printerService.printLn("Nro. 0340050004177/SUNAT");

            printerService.setTextAlignLeft();

            success = true;
        } catch (Exception ex) {
            Log.e("IMPRESORA", "Hubo un error al imprimir el comprobante.", ex);
            throw new PrinterException(ex.getLocalizedMessage(), ex);
        }
        return success;
    }


    public void close() {
        printerService.close();
    }

}
