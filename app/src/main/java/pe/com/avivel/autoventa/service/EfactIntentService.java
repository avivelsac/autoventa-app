package pe.com.avivel.autoventa.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import pe.com.avivel.autoventa.fe.FacturadorEfact;
import pe.com.avivel.autoventa.helpers.DocumentoDbHelper;
import pe.com.avivel.autoventa.model.Documento;
import pe.com.avivel.autoventa.print.GeneradorComprobante;
import pe.com.avivel.autoventa.ws.SincronizadorAvivel;

public class EfactIntentService extends IntentService {

    public static final int STATUS_RUNNING = 0;
    public static final int STATUS_FINISHED = 1;
    public static final int STATUS_ERROR = 2;

    public EfactIntentService() {
        super("EfactIntentService");
    }

    protected void onHandleIntent(Intent intent) {
        final ResultReceiver receiver = intent.getParcelableExtra("receiver");

        String command = intent.getStringExtra("command");
        int id = intent.getIntExtra("id", 0);

        receiver.send(STATUS_RUNNING, Bundle.EMPTY);
        Bundle bundle = new Bundle();
        bundle.putString("command", command);
        if (id > 0) {
            try {
                Documento documentoById = DocumentoDbHelper.getInstance(this).getDocumentoById(id);
                Boolean success;
                if (command.equals("sendxml")) {
                    success = FacturadorEfact.getInstance(this).enviarDocumento(documentoById);
                    bundle.putBoolean("result", success);
                    if (success) {
                        receiver.send(STATUS_FINISHED, bundle);
                    } else {
                        bundle.putString(Intent.EXTRA_TEXT, documentoById.getObservaciones());
                        receiver.send(STATUS_ERROR, bundle);
                    }
                } else if (command.equals("getcdr")) {
                    success = FacturadorEfact.getInstance(this).obtenerCDR(documentoById);
                    bundle.putBoolean("result", success);
                    if (!success) {
                        receiver.send(STATUS_ERROR, bundle);
                    }
                } else if (command.equals("getcdr&print")) {
                    success = FacturadorEfact.getInstance(this).obtenerCDR(documentoById);
                    bundle.putBoolean("result", success);
                    if (success) {
                        // Si se obtiene exitosamente el CDR, se procede a imprimir:
                        success = GeneradorComprobante.getInstance().generar(documentoById);
                        bundle.putBoolean("result", success);
                        if (success) {
                            // Se imprime exitosamente
                            receiver.send(STATUS_FINISHED, bundle);
                        } else {
                            receiver.send(STATUS_ERROR, bundle);
                        }
                    } else {
                        receiver.send(STATUS_ERROR, bundle);
                    }
                } else {
                    receiver.send(STATUS_ERROR, bundle);
                }
            } catch (Exception e) {
                e.printStackTrace();
                bundle.putString(Intent.EXTRA_TEXT, e.toString());
                receiver.send(STATUS_ERROR, bundle);
            }
        } else {
            if (command.equals("sync")) {
                //int responseCode = SincronizadorAvivel.getInstance(this).printJson(documentoById);
                //int responseCode = SincronizadorAvivel.getInstance(this).send(documentoById);
                try {
                    if (SincronizadorAvivel.getInstance(this).sync()) {
                        receiver.send(STATUS_FINISHED, bundle);
                    } else {
                        receiver.send(STATUS_ERROR, bundle);
                    }
                } catch (Exception e) {
                    bundle.putString(Intent.EXTRA_TEXT, e.toString());
                    receiver.send(STATUS_ERROR, bundle);
                }
            } else {
                receiver.send(STATUS_ERROR, bundle);
            }
        }
    }
}
