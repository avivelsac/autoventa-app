package pe.com.avivel.autoventa.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import pe.com.avivel.autoventa.R;
import pe.com.avivel.autoventa.model.Cliente;


public class ClienteAdapter extends ArrayAdapter<Cliente> {

    private StringBuilder stringBuilder;

    public ClienteAdapter(Context context, ArrayList<Cliente> clientes) {
        super(context, 0, clientes);
        stringBuilder = new StringBuilder();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Cliente cliente = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_cliente, parent, false);
        }
        TextView tvCodigoClienteLV = convertView.findViewById(R.id.tvCodigoClienteLV);
        stringBuilder.setLength(0);
        stringBuilder.append(cliente.getTipoDocumentoIdentidad().name());
        stringBuilder.append(": ");
        stringBuilder.append(cliente.getNumeroDocumentoIdentidad());
        tvCodigoClienteLV.setText(stringBuilder.toString());

        TextView tvNombreClienteLV = convertView.findViewById(R.id.tvNombreClienteLV);
        tvNombreClienteLV.setText(cliente.getRazonSocial());

        TextView tvDireccionClienteLV = convertView.findViewById(R.id.tvDireccionClienteLV);
        tvDireccionClienteLV.setText(cliente.getDireccion());

        TextView tvId = convertView.findViewById(R.id.idCliente);
        tvId.setText(String.valueOf(cliente.getId()));
        return convertView;
    }
}
