package pe.com.avivel.autoventa.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import pe.com.avivel.autoventa.R;
import pe.com.avivel.autoventa.adapter.ArticuloAdapter;
import pe.com.avivel.autoventa.helpers.ArticuloDbHelper;
import pe.com.avivel.autoventa.model.Articulo;

public class ListArticuloActivity extends AppCompatActivity {

    private Context mContext;
    private ArticuloDbHelper articuloDbHelper;
    private ArticuloAdapter dataAdapterArticulo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_articulo);

        mContext = this;
        articuloDbHelper = ArticuloDbHelper.getInstance(this);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent art = new Intent(mContext, ArticuloActivity.class);
                startActivityForResult(art, 1);
            }
        });

        ArrayList<Articulo> articuloList = new ArrayList<>();
        dataAdapterArticulo = new ArticuloAdapter(this, articuloList);

        // Attach the data adapter to a ListView
        ListView listView = findViewById(R.id.listServicios);
        listView.setAdapter(dataAdapterArticulo);

        cargarListViewArticulo();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                long idArticulo = Long.parseLong(((TextView) view.findViewById(R.id.txtId)).getText().toString());
                Bundle bundle = new Bundle();
                bundle.putLong("idArticulo", idArticulo);
                Intent art = new Intent(mContext, ArticuloActivity.class);
                art.putExtras(bundle);
                startActivityForResult(art, 1);
            }
        });
    }

    private void cargarListViewArticulo() {
        List<Articulo> articuloList;
        try {
            articuloList = articuloDbHelper.getAll();

            dataAdapterArticulo.clear();
            dataAdapterArticulo.addAll(articuloList);
        } catch (Exception e) {
            Toast.makeText(mContext, "Se produjo un error al consultar los datos.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == RESULT_OK) {
            cargarListViewArticulo();
        }
    }
}
