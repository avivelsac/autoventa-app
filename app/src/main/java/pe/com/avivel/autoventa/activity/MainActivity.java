package pe.com.avivel.autoventa.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.github.skyhacker2.sqliteonweb.SQLiteOnWeb;
import pe.com.avivel.autoventa.R;
import pe.com.avivel.autoventa.adapter.DetalleDocumentoResumenAdapter;
import pe.com.avivel.autoventa.helpers.ConfiguracionDbHelper;
import pe.com.avivel.autoventa.helpers.DocumentoDbHelper;
import pe.com.avivel.autoventa.model.Configuracion;
import pe.com.avivel.autoventa.model.DetalleDocumento;
import pe.com.avivel.autoventa.model.Moneda;
import pe.com.avivel.autoventa.model.TipoDocumento;
import pe.com.avivel.autoventa.print.GeneradorResumenVentasArticulo;
import pe.com.avivel.autoventa.print.GeneradorResumenVentasComprobante;
import pe.com.avivel.autoventa.utils.Fecha;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    ListView listViewDetalleDocumento;
    List<DetalleDocumento> detalleDocumentoList;
    DetalleDocumentoResumenAdapter detalleDocumentoAdapter;
    private TextView tvFecha;
    private TextView tvVenta;
    private TextView tvComprobantes;
    private TextView tvBoletas;
    private TextView tvFacturas;
    private Context context;
    private Configuracion configuracion;

    private DocumentoDbHelper documentoDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SQLiteOnWeb.init(this, 9000).start();

        ConfiguracionDbHelper configuracionDbHelper = ConfiguracionDbHelper.getInstance(this);
        configuracion = configuracionDbHelper.getConfiguracion();

        documentoDbHelper = DocumentoDbHelper.getInstance(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        context = this;

        FloatingActionButton floatingActionButton = findViewById(R.id.fab);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PostDatos();
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        tvFecha = findViewById(R.id.tvFechaTrabajo);
        tvVenta = findViewById(R.id.tvVenta);
        tvComprobantes = findViewById(R.id.tvMainComprobantes);
        tvBoletas = findViewById(R.id.tvMainBoletas);
        tvFacturas = findViewById(R.id.tvMainFacturas);

        listViewDetalleDocumento = findViewById(R.id.listViewDetalle);
        detalleDocumentoList = new ArrayList<>();
        detalleDocumentoAdapter = new DetalleDocumentoResumenAdapter(this, detalleDocumentoList);
        listViewDetalleDocumento.setAdapter(detalleDocumentoAdapter);

        actualizarData();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        switch (id) {
            case R.id.nav_configuracion:
                Intent config = new Intent(this, ConfiguracionActivity.class);
                startActivity(config);
                break;
            case R.id.nav_empresa:
                Intent empresa = new Intent(this, EmpresaActivity.class);
                startActivity(empresa);
                break;
            case R.id.nav_clientes:
                Intent cliente = new Intent(this, ListClienteActivity.class);
                startActivity(cliente);
                break;
            case R.id.nav_servicios:
                Intent servicio = new Intent(this, ListArticuloActivity.class);
                startActivity(servicio);
                break;
            case R.id.nav_recibos:
                //Intent recibo = new Intent(this, ListFacturacionActivity.class);
                //startActivity(recibo);
                Intent documento = new Intent(this, ListDocumentoActivity.class);
                startActivity(documento);
                break;
            case R.id.nav_ub_clientes:
                Intent mapClie = new Intent(this, ResumenDiarioActivity.class);
                startActivity(mapClie);
                break;
            default:
                break;
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void actualizarData() {
        try {
            if (configuracion != null) {
                String fecha = configuracion.getFechaTrabajo();
                if (!fecha.isEmpty()) {
                    Date fecha_ = Fecha.toDate(fecha);

                    tvFecha.setText(fecha);
                    tvVenta.setText(Moneda.MN.getSimbolo().concat(" ").concat(documentoDbHelper.getVentasPorDia(fecha_).toPlainString()));

                    if (!tvFecha.getText().toString().equals(Fecha.toString(Fecha.getFechaActual()))) {
                        tvFecha.setTextColor(Color.RED);
                    }

                    int boletas = documentoDbHelper.getComprobantes(fecha_, TipoDocumento.BV);
                    int facturas = documentoDbHelper.getComprobantes(fecha_, TipoDocumento.FT);
                    tvComprobantes.setText(String.valueOf(boletas + facturas));
                    tvBoletas.setText(String.valueOf(boletas));
                    tvFacturas.setText(String.valueOf(facturas));

                    detalleDocumentoList = documentoDbHelper.getDetalleListByFecha(fecha_);
                    detalleDocumentoAdapter.clear();
                    detalleDocumentoAdapter.addAll(detalleDocumentoList);
                } else {
                    tvFecha.setText("");
                    tvVenta.setText(Moneda.MN.getSimbolo().concat(" ").concat("0.00"));
                    tvComprobantes.setText(R.string.zero);
                    tvBoletas.setText(R.string.zero);
                    tvFacturas.setText(R.string.zero);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void PostDatos() {
        try {
            GeneradorResumenVentasArticulo.getInstance(this).generar(Fecha.toDate(configuracion.getFechaTrabajo()));
            Toast.makeText(context, "Reporte generado.", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(context, "Error: " + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        //mReceiver.setReceiver(null);
    }

}
