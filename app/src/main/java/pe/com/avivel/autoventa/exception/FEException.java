package pe.com.avivel.autoventa.exception;

public class FEException extends Exception {

    public FEException() {
    }

    public FEException(String message) {
        super(message);
    }

    public FEException(String message, Throwable cause) {
        super(message, cause);
    }
}
