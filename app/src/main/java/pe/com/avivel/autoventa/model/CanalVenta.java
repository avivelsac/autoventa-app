package pe.com.avivel.autoventa.model;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum CanalVenta {

    AUTOVENTA(9);

    private Integer id;

    CanalVenta(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

}
