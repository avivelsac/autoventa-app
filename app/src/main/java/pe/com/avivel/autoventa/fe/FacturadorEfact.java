package pe.com.avivel.autoventa.fe;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.common.util.IOUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import javax.net.ssl.HttpsURLConnection;

import pe.com.avivel.autoventa.exception.DbHelperException;
import pe.com.avivel.autoventa.exception.FEException;
import pe.com.avivel.autoventa.helpers.DocumentoDbHelper;
import pe.com.avivel.autoventa.model.Documento;
import pe.com.avivel.autoventa.utils.Fecha;


public final class FacturadorEfact {

    /*
    RUTAS
     */
    public static final String ROOT_PATH = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/fe";
    public static final String CDR_PATH = ROOT_PATH + "/cdr/";
    public static final String SIGNED_XML_PATH = ROOT_PATH + "/xml-firmado/";
    public static final String UNSIGNED_XML_PATH = ROOT_PATH + "/xml-sin-firmar/";
    public static final String PDF_PATH = ROOT_PATH + "/pdf/";
    /*
    SITUACIONES
     */
    public static final String SITUACION_NO_ENVIADO = "No enviado.";
    public static final String SITUACION_ENVIADO = "Enviado.";
    public static final String SITUACION_ENVIADO_VALIDACION = "Enviado, aún en proceso de validación.";
    public static final String SITUACION_ENVIADO_ACEPTADO = "Enviado y aceptado.";
    public static final String SITUACION_ERROR = "Error.";
    public static final String SITUACION_DESCONOCIDO = "Desconocido.";

    /*
    AMBIENTE DE PRUEBAS
    */

    /*public final String TOKEN_URL = "https://ose-gw1.efact.pe:443/api-efact-ose/oauth/token";
    private final String DOCUMENT_URL = "https://ose-gw1.efact.pe:443/api-efact-ose/v1/document";
    private final String CDR_URL = "https://ose-gw1.efact.pe:443/api-efact-ose/v1/cdr/";
    private final String ACCESS_KEY = "9b52a86f88aea865800d1136f9fe7d07688ae2f2304c4d824c14967e9c1bba12";*/


    /*
    AMBIENTE DE PRODUCCIÓN
     */
    private final String TOKEN_URL = "https://ose.efact.pe/api-efact-ose/oauth/token";
    private final String DOCUMENT_URL = "https://ose.efact.pe/api-efact-ose/v1/document";
    private final String CDR_URL = "https://ose.efact.pe/api-efact-ose/v1/cdr/";
    private final String ACCESS_KEY = "62a2d79394f66aaf0c68db1c0657bc72510c09d3cad89a07b42118d5257c74ae";

    private static FacturadorEfact instance;
    private Context mContext;

    private FacturadorEfact() {
    }

    public static synchronized FacturadorEfact getInstance(Context context) {
        if (instance == null) {
            instance = new FacturadorEfact();
            instance.mContext = context;
        }
        return instance;
    }

    /**
     * Obtiene el token de autorización para acceder al servidor de EFACT
     *
     * @return token de autorización
     */
    public String getToken() throws FEException {
        String accessToken;
        try {
            // Se establece el cliente POST para el servidor de autenticación
            URL url = new URL(TOKEN_URL);
            HttpsURLConnection httpsURLConnection;
            httpsURLConnection = (HttpsURLConnection) url.openConnection();
            httpsURLConnection.setRequestMethod("POST");
            httpsURLConnection.setDoInput(true);
            httpsURLConnection.setDoOutput(true);

            // Se codifica a base 64 las credenciasles de la aplicación cliente
            // Para las pruebas las credenciales son -> client:secret
            String credentials = Base64.encodeToString("client:secret".getBytes(StandardCharsets.UTF_8), Base64.DEFAULT);
            httpsURLConnection.setRequestProperty("Authorization", "Basic " + credentials);

            //Se agregan los datos de autenticación para la plataforma Efact OSE
            Uri.Builder builder = new Uri.Builder()
                    .appendQueryParameter("grant_type", "password")
                    .appendQueryParameter("username", "20524088810")
                    .appendQueryParameter("password", ACCESS_KEY);
            String query = builder.build().getEncodedQuery();

            OutputStream os = httpsURLConnection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, StandardCharsets.UTF_8));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();

            //Se envía la petición y se recibe el json con el token
            httpsURLConnection.connect();
            String json = null;

            int responseCode = httpsURLConnection.getResponseCode();

            Log.i("EFACT", "RESPONSE CODE: " + responseCode);

            switch (responseCode) {
                case 200:
                    BufferedReader br = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();
                    json = sb.toString();
                    Log.i("EFACT", "JSON: " + json);
                    break;
                case 400:
                case 401:
                    BufferedReader br2 = new BufferedReader(new InputStreamReader(httpsURLConnection.getErrorStream()));
                    StringBuilder sb2 = new StringBuilder();
                    String line2;
                    while ((line2 = br2.readLine()) != null) {
                        sb2.append(line2 + "\n");
                    }
                    br2.close();
                    json = sb2.toString();
                    Log.i("EFACT", "JSON: " + json);
                    break;
            }

            // En caso de enviar datos correcto se obtiene el token de autenticación
            if (json != null) {
                ObjectMapper mapper = new ObjectMapper();
                JsonNode rootNodeToker = mapper.readTree(json);
                accessToken = rootNodeToker.path("access_token").asText();

                Log.i("EFACT", "El token de autenticación se obtuvo satisfactoriamente.");
            } else {
                accessToken = "NO";
            }

        } catch (Exception ex) {
            Log.e("EFACT", ex.getLocalizedMessage(), ex);
            throw new FEException("Ha ocurrido un error inesperado al tratar de obtener el token de autenticación.", ex);
        }
        return accessToken;
    }

    public Boolean enviarDocumento(Documento documento) throws FEException, DbHelperException {
        boolean enviado = false;
        // Genera el archivo XML
        Log.i("EFACT", "Generando el archivo " + documento.getNombreArchivoFE() + ".xml ...");
        GeneradorXml.getInstance().generar(documento);
        documento.setFechaGeneracionXml(Fecha.getFechaActual());

        // Solicita el token de autenticación
        Log.i("EFACT", "Obteniendo el token de autenticación...");
        String accessToken = getToken();

        // Verifica si el token es válido
        if (accessToken != null) {
            // Envía el documento y almacena el ticket
            Log.i("EFACT", "Enviando el archivo " + documento.getNombreArchivoFE() + ".xml ...");
            String ticket = sendXml(accessToken, documento);

            // Verfica si el ticket es válido
            if (ticket != null) {
                enviado = true;
                // Se asigna el ticket al documento
                Log.i("EFACT", "Asignando el número de ticket [" + ticket + "] al documento " + documento.toString() + " ...");
            }
            // Se guardan los cambios
            DocumentoDbHelper.getInstance(mContext).update(documento);

        } else {
            throw new FEException("Ocurrió un error al solicitar el token de autenticación.");
        }
        return enviado;
    }

    private String sendXml(String accessToken, Documento documento) throws FEException {
        String ticket = null;
        DataOutputStream outputStream;
        String filepath = UNSIGNED_XML_PATH + "/" + documento.getNombreArchivoFE() + ".xml";
        String filefield = "file";
        String fileMimeType = "text/xml";

        String twoHyphens = "--";
        String boundary = "*****" + Long.toString(System.currentTimeMillis()) + "*****";
        String lineEnd = "\r\n";
        try {
            // Se establece el cliente POST para el servidor de autenticación
            URL url = new URL(DOCUMENT_URL);
            HttpsURLConnection httpsURLConnection;
            httpsURLConnection = (HttpsURLConnection) url.openConnection();
            httpsURLConnection.setDoInput(true);
            httpsURLConnection.setDoOutput(true);
            httpsURLConnection.setUseCaches(false);
            httpsURLConnection.setRequestMethod("POST");
            httpsURLConnection.setRequestProperty("Authorization", "Bearer " + accessToken);

            httpsURLConnection.setRequestMethod("POST");
            httpsURLConnection.setRequestProperty("Connection", "Keep-Alive");
            httpsURLConnection.setRequestProperty("User-Agent", "Android Multipart HTTP Client 1.0");
            httpsURLConnection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);

            File file = new File(filepath);
            FileInputStream fileInputStream = new FileInputStream(file);

            int bytesRead, bytesAvailable, bufferSize;
            byte[] buffer;
            int maxBufferSize = 1024 * 1024;

            String[] q = filepath.split("/");
            int idx = q.length - 1;

            outputStream = new DataOutputStream(httpsURLConnection.getOutputStream());
            outputStream.writeBytes(twoHyphens + boundary + lineEnd);
            outputStream.writeBytes("Content-Disposition: form-data; name=\"" + filefield + "\"; filename=\"" + q[idx] + "\"" + lineEnd);
            outputStream.writeBytes("Content-Type: " + fileMimeType + lineEnd);
            outputStream.writeBytes("Content-Transfer-Encoding: binary" + lineEnd);

            outputStream.writeBytes(lineEnd);

            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];

            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            while (bytesRead > 0) {
                outputStream.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }

            outputStream.writeBytes(lineEnd);
            outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            fileInputStream.close();
            outputStream.flush();
            outputStream.close();

            //Se envía la petición y se recibe el json con el token
            httpsURLConnection.connect();
            int responseCode = httpsURLConnection.getResponseCode();

            Log.i("EFACT", "STATUS_CODE: " + responseCode);

            String response;
            JsonNode rootNodeResponse;
            switch (responseCode) {
                case 200:
                    Log.i("EFACT", "Se envi\u00f3 el archivo " + documento.getNombreArchivoFE() + ".xml satisfactoriamente.");
                    response = getResponse(httpsURLConnection.getInputStream());
                    rootNodeResponse = new ObjectMapper().readTree(response);
                    ticket = rootNodeResponse.path("description").asText();
                    documento.setSituacion(SITUACION_ENVIADO);
                    documento.setFechaEnvioXml(Fecha.getFechaActual());
                    documento.setObservaciones("");
                    documento.setTicket(ticket);
                    break;
                case 412:
                    Log.i("EFACT", "No se pudo enviar el archivo " + documento.getNombreArchivoFE() + ".xml.");
                    response = getResponse(httpsURLConnection.getErrorStream());
                    rootNodeResponse = new ObjectMapper().readTree(response);
                    documento.setSituacion(SITUACION_ERROR);
                    documento.setObservaciones(rootNodeResponse.path("description").asText());
                    break;
                case 400:
                case 401:
                case 500:
                    Log.i("EFACT", "No se pudo enviar el archivo " + documento.getNombreArchivoFE() + ".xml.");
                    response = getResponse(httpsURLConnection.getErrorStream());
                    rootNodeResponse = new ObjectMapper().readTree(response);
                    documento.setSituacion(SITUACION_NO_ENVIADO);
                    documento.setObservaciones(rootNodeResponse.path("description").asText());
                    break;
            }
        } catch (Exception ex) {
            Log.e("EFACT", ex.getLocalizedMessage(), ex);
            throw new FEException("Ha ocurrido un error inesperadoal enviar el archivo XML.", ex);
        }
        return ticket;
    }

    public Boolean obtenerCDR(Documento documento) throws FEException, DbHelperException {
        Boolean cdrObtenido = false;

        // Solicita el token de autenticación
        Log.i("EFACT", "Solicitando el token de autenticación...");
        String accessToken = getToken();

        // Verifica si el token es válido
        if (accessToken != null) {
            // Envía el documento y almacena el ticket
            Log.i("EFACT", "Solicitando el CDR para el comprobante " + documento.toString() + " ...");
            Integer statusCode = getCDR(accessToken, documento);

            // Verfica si el error es nulo
            if (statusCode == 200) {
                // Lee el archivo XML y setea el digest value y el estado al documento
                Log.i("EFACT", "Leyendo el CDR del comprobante " + documento.toString() + " ...");
                LectorXml.leerCDR(documento);
                // Verifica si se ha seteado el digest value correctamente
                if (documento.getDigestValue() != null) {
                    // Actualiza el estado y el digest value del documento
                    Log.i("EFACT", "CDR obtenido y consultado satisfactoriamente.");
                    cdrObtenido = true;
                }
            }
            DocumentoDbHelper.getInstance(mContext).update(documento);
        } else {
            throw new FEException("Ocurrió un error al solicitar el token de autenticación.");
        }
        return cdrObtenido;
    }

    private Integer getCDR(String accessToken, Documento documento) throws FEException {
        Integer responseCode = null;
        try {
            URL url = new URL(CDR_URL + documento.getTicket());
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) url.openConnection();
            httpsURLConnection.setDoInput(true);
            httpsURLConnection.setDoOutput(false);
            httpsURLConnection.setRequestMethod("GET");
            httpsURLConnection.setRequestProperty("Authorization", "Bearer " + accessToken);

            //Se envía la petición
            httpsURLConnection.connect();
            responseCode = httpsURLConnection.getResponseCode();

            Log.i("EFACT", "STATUS_CODE: " + responseCode);

            // Si el código de estado es 200 (OK) entonces la respuesta contiene el CDR en formato XML
            switch (responseCode) {
                case 200:
                    File cdrFile = new File(CDR_PATH + "R" + documento.getNombreArchivoFE() + ".xml");
                    InputStream stream = httpsURLConnection.getInputStream();
                    if (stream != null) {
                        FileOutputStream outstream = new FileOutputStream(cdrFile);
                        //outstream.write(getResponse(stream).getBytes());
                        IOUtils.copyStream(stream, outstream);
                        Log.i("EFACT", "El CDR para el comprobante " + documento.toString() + " se obtuvo satisfactoriamente.");
                        documento.setSituacion(SITUACION_ENVIADO_ACEPTADO);
                    } else {
                        throw new FEException("Ha ocurrido un error al obtener el CDR del comprobante " + documento.toString() + ".");
                    }
                    break;
                case 202:
                    Log.i("EFACT", "El documento " + documento.toString() + " aún se encuentra en proceso de validación.");
                    documento.setSituacion(SITUACION_ENVIADO_VALIDACION);
                    documento.setObservaciones("Última consulta: " + Fecha.toStringFechaHora(Fecha.getFechaActual()));
                    break;
                case 412:
                    Log.i("EFACT", "Validación incorrecta del XML, el comprobante " + documento.toString() + " contiene errores.");
                    //Se obtiene el resultado del response
                    String jsonResponse = getResponse(httpsURLConnection.getErrorStream());
                    // Se transforma el Json y se obtiene el ticket
                    ObjectMapper mapper = new ObjectMapper();
                    JsonNode rootNodeResponse = mapper.readTree(jsonResponse);
                    String error = rootNodeResponse.path("description").asText();
                    documento.setSituacion(SITUACION_ERROR);
                    documento.setObservaciones(error);
                    break;
                default:
                    Log.i("EFACT", "No se puedo obtener el CDR, el ticket '" + documento.getTicket() + "' no existe.");
                    documento.setSituacion(SITUACION_DESCONOCIDO);
                    documento.setObservaciones("El ticket no existe.");
                    break;
            }
        } catch (Exception ex) {
            Log.e("EFACT", ex.getLocalizedMessage(), ex);
            throw new FEException("Ha ocurrido un error al consultar el CDR del comprobante " + documento.toString() + ".", ex);
        }
        return responseCode;
    }

    private String getResponse(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader;
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line).append("\n");
        }
        bufferedReader.close();
        String response = stringBuilder.toString();
        Log.e("EFACT", "RESPONSE: " + response);
        return response;
    }

    public void sendJson(Documento documento) throws FEException {
        OutputStreamWriter outputStream;
        try {
            // Se establece el cliente POST para el servidor de autenticación
            URL url = new URL("http://10.10.10.10:8080/jws/api/documento");
            HttpsURLConnection httpsURLConnection;
            httpsURLConnection = (HttpsURLConnection) url.openConnection();
            httpsURLConnection.setDoInput(true);
            httpsURLConnection.setDoOutput(true);
            httpsURLConnection.setUseCaches(false);
            httpsURLConnection.setRequestMethod("POST");
            // httpsURLConnection.setRequestProperty("Authorization", "Bearer " + accessToken);

            httpsURLConnection.setRequestProperty("Content-Type", "application/json");

            outputStream = new OutputStreamWriter(httpsURLConnection.getOutputStream());

            ObjectMapper om = new ObjectMapper();
            String jsonString = om.writeValueAsString(documento);

            outputStream.write(jsonString);

            outputStream.flush();
            outputStream.close();

            //Se envía la petición y se recibe el json con el token
            httpsURLConnection.connect();
            int responseCode = httpsURLConnection.getResponseCode();

            Log.i("EFACT", "STATUS_CODE: " + responseCode);
        } catch (Exception ex) {
            Log.e("EFACT", ex.getLocalizedMessage(), ex);
            throw new FEException("Ha ocurrido un error inesperadoal enviar el archivo XML.", ex);
        }
    }


}
