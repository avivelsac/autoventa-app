package pe.com.avivel.autoventa.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import pe.com.avivel.autoventa.model.DetalleDocumento;
import pe.com.avivel.autoventa.model.Documento;
import pe.com.avivel.autoventa.utils.Utils;

public class DetalleDocumentoDbHelper extends FacturacionHelper {

    private static DetalleDocumentoDbHelper instance;

    final static String TABLE_NAME = "detalle_documento";
    final static String COLUMN_NAME_IDXXXX = "detalledocumento_id";
    final static String COLUMN_NAME_DOCUME = "documento_id";
    final static String COLUMN_NAME_SECUEN = "detalledocumento_secuencia";
    final static String COLUMN_NAME_ARTICU = "articulo_id";
    final static String COLUMN_NAME_CANTID = "detalledocumento_cantidad";
    final static String COLUMN_NAME_UNIDAD = "detalledocumento_unidades";
    final static String COLUMN_NAME_PRECIO = "detalledocumento_precio";
    final static String COLUMN_NAME_IMPORT = "detalledocumento_importe";

    private DetalleDocumentoDbHelper(Context context) {
        super(context);
    }

    public static DetalleDocumentoDbHelper getInstance(Context context) {
        if (instance == null) {
            instance = new DetalleDocumentoDbHelper(context);
        }
        return instance;
    }

    /**
     * Crea tabla articulo
     *
     * @param db base de datos
     */
    static void createTable(SQLiteDatabase db) {
        String query = "create table " + TABLE_NAME + "( " +
                COLUMN_NAME_IDXXXX + " integer primary key AUTOINCREMENT, " +
                COLUMN_NAME_DOCUME + " integer null, " +
                COLUMN_NAME_SECUEN + " integer null, " +
                COLUMN_NAME_ARTICU + " integer null, " +
                COLUMN_NAME_CANTID + " integer null, " +
                COLUMN_NAME_UNIDAD + " integer null, " +
                COLUMN_NAME_PRECIO + " integer null, " +
                COLUMN_NAME_IMPORT + " integer null, " +
                "CONSTRAINT fk_detalledocumento_documento FOREIGN KEY (" + COLUMN_NAME_DOCUME + ") REFERENCES " + DocumentoDbHelper.TABLE_NAME + " (" + DocumentoDbHelper.COLUMN_NAME_IDXXXX + ") ON DELETE CASCADE ON UPDATE CASCADE," +
                "CONSTRAINT fk_detalledocumento_articulo FOREIGN KEY (" + COLUMN_NAME_ARTICU + ") REFERENCES " + ArticuloDbHelper.TABLE_NAME + " (" + ArticuloDbHelper.COLUMN_NAME_IDXXXX + ") ON DELETE RESTRICT ON UPDATE CASCADE" +
                ")";
        db.execSQL(query);
    }

    static void dropTable(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    }

    public long insert(DetalleDocumento detalleDocumento) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME_DOCUME, detalleDocumento.getDocumento().getId());
        values.put(COLUMN_NAME_SECUEN, detalleDocumento.getSecuencia());
        values.put(COLUMN_NAME_ARTICU, detalleDocumento.getArticulo().getId());
        values.put(COLUMN_NAME_CANTID, Utils.fromBigDecimalToInteger(detalleDocumento.getCantidad()));
        values.put(COLUMN_NAME_UNIDAD, Utils.fromBigDecimalToInteger(detalleDocumento.getUnidades()));
        values.put(COLUMN_NAME_PRECIO, Utils.fromBigDecimalToInteger(detalleDocumento.getPrecio()));
        values.put(COLUMN_NAME_IMPORT, Utils.fromBigDecimalToInteger(detalleDocumento.getImporte()));

        return db.insert(TABLE_NAME, null, values);
    }

    public int deleteByDocumento(Documento documento) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME, COLUMN_NAME_DOCUME + "=?", new String[]{String.valueOf(documento.getId())});
    }

    public int deleteAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME, null, null);
    }

}
