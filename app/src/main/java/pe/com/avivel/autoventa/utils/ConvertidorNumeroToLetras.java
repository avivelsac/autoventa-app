package pe.com.avivel.autoventa.utils;

/**
 * @author André Bedregal <abedregal at avivel.com.pe>
 */
public class ConvertidorNumeroToLetras {

    private static final String[] nombresIrregulares = {
            "", "UNO", "DOS", "TRES", "CUATRO",
            "CINCO", "SEIS", "SIETE", "OCHO", "NUEVE",
            "DIEZ", "ONCE", "DOCE", "TRECE", "CATORCE",
            "QUINCE", "DIECISÉIS", "DIECISIETE", "DIECIOCHO", "DIECINUEVE",
            "VEINTE", "VEINTIUNO", "VEINTIDÓS", "VEINTITRÉS", "VEINTICUATRO",
            "VEINTICINCO", "VEINTISÉIS", "VEINTISIETE", "VEINTIOCHO", "VEINTINUEVE"};

    ;
    private static final String[] decenas = {"", "", "", "TREINTA", "CUARENTA", "CINCUENTA",
            "SESENTA", "SETENTA", "OCHENTA", "NOVENTA"};
    private static final String[] centenas = {"", "", "DOSCIENT", "TRESCIENT",
            "CUATROCIENT", "QUINIENT", "SEISCIENT", "SETECIENT", "OCHOCIENT", "NOVECIENT"};
    private static final String[] nombreMillonesSingular = {"", "MILLÓN",
            "BILLÓN", "TRILLÓN", "CUATRILLÓN"};
    private static final String[] nombreMillonesPlural = {"", "MILLONES",
            "BILLONES", "TRILLONES", "CUATRILLONES"};

    private static String Convierte1(int i, Tipo t) {
        String r;
        if ((i == 1) && (t == Tipo.DetMasculino)) {
            r = "UN";
        } else if ((i == 1) && (t == Tipo.DetFemenino)) {
            r = "UNA";
        } else {
            r = nombresIrregulares[i];
        }
        return r;
    }

    private static String Convierte2(int i, Tipo t) {
        StringBuilder r = new StringBuilder();
        if (i <= 9) {
            r.append(Convierte1(i, t));
        } else if (i == 21 && t != Tipo.Pronombre) {
            if (t == Tipo.DetMasculino) {
                r.append("VEINTIÚN");
            } else {
                r.append("VEINTUNA");
            }
        } else if (i >= 10 && i <= 29) {
            r.append(nombresIrregulares[i]);
        } else {
            r.append(decenas[i / 10]);
            if (i % 10 != 0) {
                r.append(" Y ");
                r.append(Convierte1(i % 10, t));
            }
        }
        return r.toString();
    }

    private static String Convierte3(int i, Tipo t) {
        StringBuilder r = new StringBuilder();
        if (i <= 99) {
            r.append(Convierte2(i, t));
        } else if (i == 100) {
            r.append("CIEN");
        } else {
            if (i >= 101 && i <= 199) {
                r.append("CIENTO");
            } else {
                r.append(centenas[i / 100]);
                r.append(t == Tipo.DetFemenino ? "AS" : "OS");
            }
            if (i % 100 > 0) {
                r.append(" ");
                r.append(Convierte2(i % 100, t));
            }
        }
        return r.toString();
    }

    private static String Convierte6(int i, Tipo t) {
        StringBuilder r = new StringBuilder();
        int tresPrimeros = i / 1000;
        int tresUltimos = i % 1000;
        if (tresPrimeros == 1) {
            r.append("MIL");
        } else if (tresPrimeros >= 2) {
            if (t == Tipo.Pronombre) {
                r.append(Convierte3(tresPrimeros, Tipo.DetMasculino));
            } else {
                r.append(Convierte3(tresPrimeros, t));
            }
            r.append(" MIL");
        }

        if (tresUltimos > 0) {
            if (tresPrimeros > 0) {
                r.append(" ");
            }
            r.append(Convierte3(tresUltimos, t));
        }
        return r.toString();
    }

    private static boolean EsNumero(String s) {
        boolean resultado = true;
        int contador = 0;
        int longitud = s.length();
        while (resultado && contador < longitud) {
            if (s.charAt(contador) < '0' || s.charAt(contador) > '9') {
                resultado = false;
            }
            contador++;
        }
        return resultado;
    }

    private static String ConvierteTodo(String s, Tipo t) {
        StringBuilder resultado = new StringBuilder();
        int cuenta = s.length();
        int cuentamillones = 0;
        while (cuenta > 0) {
            int inicio = (cuenta - 6 >= 0) ? (cuenta - 6) : 0;
            int longitud = (6 > cuenta) ? cuenta : 6;
            String stemp = s.substring(inicio, inicio + longitud);
            int i6 = Integer.parseInt(stemp);
            if (cuentamillones > 0 && i6 > 0) {
                if (resultado.length() > 0) {
                    resultado.insert(0, " ");
                }
                if (i6 > 1) {
                    resultado.insert(0, nombreMillonesPlural[cuentamillones]);
                } else {
                    resultado.insert(0, nombreMillonesSingular[cuentamillones]);
                }
                resultado.insert(0, " ");
            }
            resultado.insert(0, Convierte6(i6, t));
            if (cuentamillones == 0) {
                t = Tipo.DetMasculino;
            }
            cuentamillones++;
            cuenta -= 6;
        }
        return resultado.toString();
    }

    /**
     * Convierte el número dado a su representación literal.
     *
     * @param numero El número decimal positivo (No mayor que
     *               999,999,999,999,999,999,999,999,999,999.99).
     * @param tipo   El tipo de conversión a utilizar, ej: Tipo.Pronombre:
     *               VEINTIUNO, Tipo.DetMasculino: VEINTIÚN, Tipo.DetFemenino: VEINTUNA.
     * @param moneda La moneda a utilizar, valores admitidos: 'MN' y 'US'
     */
    public static String convierte(String numero, Tipo tipo, String moneda) {

        String parteEntera = (numero.substring(0, numero.indexOf('.'))).replaceAll(",", "");
        String parteDecimal = numero.substring(numero.indexOf('.') + 1, numero.length());

        String resultado;

        if (parteEntera.length() > 30 || !EsNumero(parteEntera)) {
            resultado = numero;
        } else {
            if (parteEntera.equals("0")) {
                resultado = "CERO";
            } else {
                resultado = ConvierteTodo(parteEntera, tipo);
            }
            if ("MN".equals(moneda)) {
                resultado = resultado + " Y " + parteDecimal + "/100 SOLES";
            } else if ("US".equals(moneda)) {
                resultado = resultado + " Y " + parteDecimal + "/100 DÓLARES AMERICANOS";
            } else {
                resultado = resultado + " Y " + parteDecimal + "/100 " + moneda;
            }
        }
        return resultado;
    }

    public enum Tipo {

        Pronombre, DetMasculino, DetFemenino
    }

}

