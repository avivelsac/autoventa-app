package pe.com.avivel.autoventa.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

/**
 * Created by jemarinero on 25/11/2017.
 */
public class Cliente {

    @JsonIgnore
    private Integer id;

    private TipoPersona tipoPersona;

    private TipoDocumentoIdentidad tipoDocumentoIdentidad;

    @JsonProperty("numeroDocumentoIdentidad")
    private String numeroDocumento;

    private String razonSocial;

    private String direccion;

    private String distrito;

    private String provincia;

    private String departamento;

    private CanalVenta canalVenta = CanalVenta.AUTOVENTA;

    @JsonProperty("email")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String correo;

    @JsonIgnore
    private Double latitud;

    @JsonIgnore
    private Double longitud;

    @JsonIgnore
    private Cliente clienteReal;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoPersona getTipoPersona() {
        if (tipoDocumentoIdentidad == TipoDocumentoIdentidad.DNI) {
            return TipoPersona.N;
        } else if (tipoDocumentoIdentidad == TipoDocumentoIdentidad.OTR) {
            return TipoPersona.J;
        } else {
            if(numeroDocumento.startsWith("2")) {
                return TipoPersona.J;
            } else {
                return TipoPersona.N;
            }
        }
    }

    public void setTipoPersona(TipoPersona tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public TipoDocumentoIdentidad getTipoDocumentoIdentidad() {
        return tipoDocumentoIdentidad;
    }

    public void setTipoDocumentoIdentidad(String tipoDocumentoIdentidad) {
        this.tipoDocumentoIdentidad = TipoDocumentoIdentidad.valueOf(tipoDocumentoIdentidad);
    }

    public void setTipoDocumentoIdentidad(TipoDocumentoIdentidad tipoDocumentoIdentidad) {
        this.tipoDocumentoIdentidad = tipoDocumentoIdentidad;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    @JsonIgnore
    public String getNumeroDocumentoIdentidad() {
        return numeroDocumento;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public CanalVenta getCanalVenta() {
        return canalVenta;
    }

    public void setCanalVenta(CanalVenta canalVenta) {
        this.canalVenta = canalVenta;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    @JsonIgnore
    public String getDireccionCompleta() {
        return direccion + " - " + distrito;
    }

    public Cliente getClienteReal() {
        return clienteReal;
    }

    public void setClienteReal(Cliente clienteReal) {
        this.clienteReal = clienteReal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cliente cliente = (Cliente) o;
        return Objects.equals(id, cliente.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public String toString() {
        if (razonSocial.length() > 35) {
            return razonSocial.substring(0, 35).concat(" (").concat(tipoDocumentoIdentidad.name()).concat(")");
        } else {
            return razonSocial.concat(" (").concat(tipoDocumentoIdentidad.name()).concat(")");
        }
    }

}
