package pe.com.avivel.autoventa.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pe.com.avivel.autoventa.exception.DbHelperException;
import pe.com.avivel.autoventa.model.Articulo;
import pe.com.avivel.autoventa.model.CanalVenta;
import pe.com.avivel.autoventa.model.Cliente;
import pe.com.avivel.autoventa.model.DetalleDocumento;
import pe.com.avivel.autoventa.model.Documento;
import pe.com.avivel.autoventa.model.MedioPago;
import pe.com.avivel.autoventa.model.TipoDocumento;
import pe.com.avivel.autoventa.model.TipoDocumentoEntity;
import pe.com.avivel.autoventa.utils.Fecha;
import pe.com.avivel.autoventa.utils.PlainText;
import pe.com.avivel.autoventa.utils.Utils;

public class DocumentoDbHelper extends FacturacionHelper {

    final static String TABLE_NAME = "documento";
    final static String COLUMN_NAME_IDXXXX = "documento_id";
    private final static String COLUMN_NAME_FECEMI = "documento_fechaemision";
    private final static String COLUMN_NAME_MONEDA = "documento_moneda";
    private final static String COLUMN_NAME_CLIEN1 = "cliente_id_1";
    private final static String COLUMN_NAME_CLIEN2 = "cliente_id_2";
    private final static String COLUMN_NAME_TIPDOC = "documento_tipodocumento";
    private final static String COLUMN_NAME_SERIEX = "documento_serie";
    private final static String COLUMN_NAME_NUMERO = "documento_numero";
    private final static String COLUMN_NAME_PCTIGV = "documento_porcentajeigv";
    private final static String COLUMN_NAME_BASEIM = "documento_baseimponible";
    private final static String COLUMN_NAME_OPEINA = "documento_operacionesinafectas";
    private final static String COLUMN_NAME_OPEEXO = "documento_operacionesexoneradas";
    private final static String COLUMN_NAME_OPEGRA = "documento_operacionesgratuitas";
    private final static String COLUMN_NAME_IGVXXX = "documento_igv";
    private final static String COLUMN_NAME_IMPORT = "documento_importe";
    private final static String COLUMN_NAME_TIPOPE = "documento_tipooperacion";
    private final static String COLUMN_NAME_DIGEST = "documento_digestvalue";
    private final static String COLUMN_NAME_FECGEN = "documento_fechageneracionxml";
    private final static String COLUMN_NAME_FECENV = "documento_fechaenvioxml";
    private final static String COLUMN_NAME_SITUAC = "documento_situacion";
    private final static String COLUMN_NAME_OBSERV = "documento_observaciones";
    private final static String COLUMN_NAME_TICKET = "documento_ticket";
    private final static String COLUMN_NAME_FECAVI = "documento_fechaenvioavivel";
    private final static String COLUMN_NAME_SITAVI = "documento_situacionavivel";
    private final String WHERE_CLAUSE = "WHERE";
    private static DocumentoDbHelper instance;

    private DocumentoDbHelper(Context context) {
        super(context);
    }

    public static DocumentoDbHelper getInstance(Context context) {
        if (instance == null) {
            instance = new DocumentoDbHelper(context);
        }
        return instance;
    }

    /**
     * Crea tabla articulo
     *
     * @param db base de datos
     */
    static void createTable(SQLiteDatabase db) {
        String query = "create table " + TABLE_NAME + "( " +
                COLUMN_NAME_IDXXXX + " integer primary key AUTOINCREMENT, " +
                COLUMN_NAME_FECEMI + " text null, " +
                COLUMN_NAME_MONEDA + " text null, " +
                COLUMN_NAME_CLIEN1 + " integer null, " +
                COLUMN_NAME_CLIEN2 + " integer null, " +
                COLUMN_NAME_TIPDOC + " text null, " +
                COLUMN_NAME_SERIEX + " text null, " +
                COLUMN_NAME_NUMERO + " text null, " +
                COLUMN_NAME_BASEIM + " integer null, " +
                COLUMN_NAME_OPEINA + " integer null, " +
                COLUMN_NAME_OPEEXO + " integer null, " +
                COLUMN_NAME_OPEGRA + " integer null, " +
                COLUMN_NAME_PCTIGV + " integer null, " +
                COLUMN_NAME_IGVXXX + " integer null, " +
                COLUMN_NAME_IMPORT + " integer null, " +
                COLUMN_NAME_TIPOPE + " text null, " +
                COLUMN_NAME_DIGEST + " text null, " +
                COLUMN_NAME_FECGEN + " text null, " +
                COLUMN_NAME_FECENV + " text null, " +
                COLUMN_NAME_SITUAC + " text null, " +
                COLUMN_NAME_OBSERV + " text null, " +
                COLUMN_NAME_TICKET + " text null, " +
                COLUMN_NAME_FECAVI + " text null, " +
                COLUMN_NAME_SITAVI + " text null, " +
                "CONSTRAINT fk_documento_cliente1 FOREIGN KEY (" + COLUMN_NAME_CLIEN1 + ") REFERENCES " + ClienteDbHelper.TABLE_NAME + " (" + ClienteDbHelper.COLUMN_NAME_ID + ") ON DELETE RESTRICT ON UPDATE CASCADE," +
                "CONSTRAINT fk_documento_cliente2 FOREIGN KEY (" + COLUMN_NAME_CLIEN2 + ") REFERENCES " + ClienteDbHelper.TABLE_NAME + " (" + ClienteDbHelper.COLUMN_NAME_ID + ") ON DELETE RESTRICT ON UPDATE CASCADE" +
                ")";
        db.execSQL(query);
    }

    static void dropTable(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    }

    public long insert(Documento documento) throws DbHelperException {
        long insert;
        SQLiteDatabase db = null;
        ContentValues values;
        try {
            db = this.getWritableDatabase();
            values = new ContentValues();

            values.put(COLUMN_NAME_FECEMI, Utils.fromDateToString(documento.getFechaEmision()));
            values.put(COLUMN_NAME_MONEDA, String.valueOf(documento.getMoneda()));
            values.put(COLUMN_NAME_CLIEN1, documento.getCliente().getId());
            values.put(COLUMN_NAME_CLIEN2, documento.getClienteDestino().getId());
            values.put(COLUMN_NAME_TIPDOC, String.valueOf(documento.getTipoDocumento()));
            values.put(COLUMN_NAME_SERIEX, documento.getSerie());
            values.put(COLUMN_NAME_NUMERO, documento.getNumero());
            values.put(COLUMN_NAME_BASEIM, Utils.fromBigDecimalToInteger(documento.getBaseImponible()));
            values.put(COLUMN_NAME_OPEINA, Utils.fromBigDecimalToInteger(documento.getOperacionesInafectas()));
            values.put(COLUMN_NAME_OPEEXO, Utils.fromBigDecimalToInteger(documento.getOperacionesExoneradas()));
            values.put(COLUMN_NAME_OPEGRA, Utils.fromBigDecimalToInteger(documento.getOperacionesGratuitas()));
            values.put(COLUMN_NAME_PCTIGV, Utils.fromBigDecimalToInteger(documento.getPorcentajeIgv()));
            values.put(COLUMN_NAME_IGVXXX, Utils.fromBigDecimalToInteger(documento.getIgv()));
            values.put(COLUMN_NAME_IMPORT, Utils.fromBigDecimalToInteger(documento.getImporte()));
            values.put(COLUMN_NAME_TIPOPE, String.valueOf(documento.getTipoOperacion()));
            values.put(COLUMN_NAME_DIGEST, documento.getDigestValue());
            values.put(COLUMN_NAME_FECGEN, Utils.fromDateTimeToString(documento.getFechaGeneracionXml()));
            values.put(COLUMN_NAME_FECENV, Utils.fromDateTimeToString(documento.getFechaEnvioXml()));
            values.put(COLUMN_NAME_SITUAC, documento.getSituacion());
            values.put(COLUMN_NAME_OBSERV, documento.getObservaciones());
            values.put(COLUMN_NAME_TICKET, documento.getTicket());
            values.put(COLUMN_NAME_FECAVI, Utils.fromDateTimeToString(documento.getFechaEnvioAvivel()));
            values.put(COLUMN_NAME_SITAVI, documento.getSituacionAvivel());

            insert = db.insert(TABLE_NAME, null, values);
            documento.setId((int) insert);

            for (DetalleDocumento detalleDocumento : documento.getDetalleDocumentoList()) {
                values = new ContentValues();
                values.put(DetalleDocumentoDbHelper.COLUMN_NAME_DOCUME, documento.getId());
                values.put(DetalleDocumentoDbHelper.COLUMN_NAME_SECUEN, detalleDocumento.getSecuencia());
                values.put(DetalleDocumentoDbHelper.COLUMN_NAME_ARTICU, detalleDocumento.getArticulo().getId());
                values.put(DetalleDocumentoDbHelper.COLUMN_NAME_UNIDAD, Utils.fromBigDecimalToInteger(detalleDocumento.getUnidades()));
                values.put(DetalleDocumentoDbHelper.COLUMN_NAME_CANTID, Utils.fromBigDecimalToInteger(detalleDocumento.getCantidad()));
                values.put(DetalleDocumentoDbHelper.COLUMN_NAME_PRECIO, Utils.fromBigDecimalToInteger(detalleDocumento.getPrecio()));
                values.put(DetalleDocumentoDbHelper.COLUMN_NAME_IMPORT, Utils.fromBigDecimalToInteger(detalleDocumento.getImporte()));

                db.insert(DetalleDocumentoDbHelper.TABLE_NAME, null, values);
            }
            Log.e("DATABASE", "El documento se insertó satisfactotiamente.");
        } catch (Exception ex) {
            Log.e("DATABASE", ex.getLocalizedMessage(), ex);
            throw new DbHelperException(ex.getLocalizedMessage(), ex);
        } finally {
            if (db != null && db.isOpen()) {
                db.close();
            }
        }
        return insert;
    }

    public long update(Documento documento) throws DbHelperException {
        long update;
        SQLiteDatabase db = null;
        ContentValues values;
        try {
            db = this.getWritableDatabase();
            values = new ContentValues();

            values.put(COLUMN_NAME_FECEMI, Utils.fromDateToString(documento.getFechaEmision()));
            values.put(COLUMN_NAME_MONEDA, String.valueOf(documento.getMoneda()));
            values.put(COLUMN_NAME_CLIEN1, documento.getCliente().getId());
            values.put(COLUMN_NAME_CLIEN2, documento.getClienteDestino().getId());
            values.put(COLUMN_NAME_TIPDOC, String.valueOf(documento.getTipoDocumento()));
            values.put(COLUMN_NAME_SERIEX, documento.getSerie());
            values.put(COLUMN_NAME_NUMERO, documento.getNumero());
            values.put(COLUMN_NAME_BASEIM, Utils.fromBigDecimalToInteger(documento.getBaseImponible()));
            values.put(COLUMN_NAME_OPEINA, Utils.fromBigDecimalToInteger(documento.getOperacionesInafectas()));
            values.put(COLUMN_NAME_OPEEXO, Utils.fromBigDecimalToInteger(documento.getOperacionesExoneradas()));
            values.put(COLUMN_NAME_OPEGRA, Utils.fromBigDecimalToInteger(documento.getOperacionesGratuitas()));
            values.put(COLUMN_NAME_PCTIGV, Utils.fromBigDecimalToInteger(documento.getPorcentajeIgv()));
            values.put(COLUMN_NAME_IGVXXX, Utils.fromBigDecimalToInteger(documento.getIgv()));
            values.put(COLUMN_NAME_IMPORT, Utils.fromBigDecimalToInteger(documento.getImporte()));
            values.put(COLUMN_NAME_TIPOPE, String.valueOf(documento.getTipoOperacion()));
            values.put(COLUMN_NAME_DIGEST, documento.getDigestValue());
            values.put(COLUMN_NAME_FECGEN, Utils.fromDateTimeToString(documento.getFechaGeneracionXml()));
            values.put(COLUMN_NAME_FECENV, Utils.fromDateTimeToString(documento.getFechaEnvioXml()));
            values.put(COLUMN_NAME_SITUAC, documento.getSituacion());
            values.put(COLUMN_NAME_OBSERV, documento.getObservaciones());
            values.put(COLUMN_NAME_TICKET, documento.getTicket());
            values.put(COLUMN_NAME_FECAVI, Utils.fromDateTimeToString(documento.getFechaEnvioAvivel()));
            values.put(COLUMN_NAME_SITAVI, documento.getSituacionAvivel());

            update = db.update(TABLE_NAME, values, "documento_id=?", new String[]{String.valueOf(documento.getId())});

            // eliminar detalles
            db.delete(DetalleDocumentoDbHelper.TABLE_NAME, "documento_id=?", new String[]{String.valueOf(documento.getId())});

            // insertar detalles
            for (DetalleDocumento detalleDocumento : documento.getDetalleDocumentoList()) {
                values = new ContentValues();
                values.put(DetalleDocumentoDbHelper.COLUMN_NAME_DOCUME, documento.getId());
                values.put(DetalleDocumentoDbHelper.COLUMN_NAME_SECUEN, detalleDocumento.getSecuencia());
                values.put(DetalleDocumentoDbHelper.COLUMN_NAME_ARTICU, detalleDocumento.getArticulo().getId());
                values.put(DetalleDocumentoDbHelper.COLUMN_NAME_UNIDAD, Utils.fromBigDecimalToInteger(detalleDocumento.getUnidades()));
                values.put(DetalleDocumentoDbHelper.COLUMN_NAME_CANTID, Utils.fromBigDecimalToInteger(detalleDocumento.getCantidad()));
                values.put(DetalleDocumentoDbHelper.COLUMN_NAME_PRECIO, Utils.fromBigDecimalToInteger(detalleDocumento.getPrecio()));
                values.put(DetalleDocumentoDbHelper.COLUMN_NAME_IMPORT, Utils.fromBigDecimalToInteger(detalleDocumento.getImporte()));

                db.insert(DetalleDocumentoDbHelper.TABLE_NAME, null, values);
            }

            Log.e("DATABASE", "El documento se modificó satisfactotiamente.");
        } catch (Exception ex) {
            Log.e("DATABASE", ex.getLocalizedMessage(), ex);
            throw new DbHelperException(ex.getLocalizedMessage(), ex);
        } finally {
            if (db != null && db.isOpen()) {
                db.close();
            }
        }
        return update;
    }

    public int deleteById(Integer id) throws DbHelperException {
        int delete;
        SQLiteDatabase db = null;
        try {
            db = this.getWritableDatabase();

            delete = db.delete(TABLE_NAME, "documento_id=?", new String[]{String.valueOf(id)});
            Log.e("DATABASE", "El documento se eliminó satisfactotiamente.");
        } catch (Exception ex) {
            Log.e("DATABASE", ex.getLocalizedMessage(), ex);
            throw new DbHelperException(ex.getLocalizedMessage(), ex);
        } finally {
            if (db != null && db.isOpen()) {
                db.close();
            }
        }
        return delete;
    }

    public int deleteAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME, null, null);
    }

    private Documento getDocumento(String where, String[] params) throws DbHelperException {
        Documento documento = null;
        Cliente cliente1, cliente2;
        DetalleDocumento detalleDocumento;
        Articulo articulo;
        List<DetalleDocumento> detalleDocumentoList;

        SQLiteDatabase db;
        Cursor mCursor = null;
        Cursor mCursorDetalles = null;

        try {
            db = this.getWritableDatabase();
            String queryCabecera = "SELECT " +
                    "doc.documento_id," +
                    "doc.documento_fechaemision," +
                    "doc.documento_moneda," +
                    "doc.documento_tipodocumento," +
                    "doc.documento_serie," +
                    "doc.documento_numero," +
                    "doc.documento_porcentajeigv," +
                    "doc.documento_baseimponible," +
                    "doc.documento_operacionesinafectas," +
                    "doc.documento_operacionesexoneradas," +
                    "doc.documento_operacionesgratuitas," +
                    "doc.documento_igv," +
                    "doc.documento_importe," +
                    "doc.documento_tipooperacion," +
                    "doc.documento_digestvalue," +
                    "doc.documento_fechageneracionxml," +
                    "doc.documento_fechaenvioxml," +
                    "doc.documento_situacion," +
                    "doc.documento_observaciones," +
                    "doc.documento_ticket," +
                    "doc.documento_fechaenvioavivel," +
                    "doc.documento_situacionavivel," +
                    "cli1.cliente_id," +
                    "cli1.cliente_tipodocumento," +
                    "cli1.cliente_numerodocumento," +
                    "cli1.cliente_razonsocial," +
                    "cli1.cliente_direccion," +
                    "cli1.cliente_distrito," +
                    "cli1.cliente_provincia," +
                    "cli1.cliente_departamento," +
                    "cli1.cliente_correo," +
                    "cli1.cliente_latitud," +
                    "cli1.cliente_longitud," +
                    "cli2.cliente_id AS cliente_id_2," +
                    "cli2.cliente_tipodocumento AS cliente_tipodocumento_2," +
                    "cli2.cliente_numerodocumento AS cliente_numerodocumento_2," +
                    "cli2.cliente_razonsocial AS cliente_razonsocial_2," +
                    "cli2.cliente_direccion AS cliente_direccion_2," +
                    "cli2.cliente_distrito AS cliente_distrito_2," +
                    "cli2.cliente_provincia AS cliente_provincia_2," +
                    "cli2.cliente_departamento AS cliente_departamento_2," +
                    "cli2.cliente_correo AS cliente_correo_2," +
                    "cli2.cliente_latitud AS cliente_latitud_2," +
                    "cli2.cliente_longitud AS cliente_longitud_2 " +
                    "FROM " +
                    "documento AS doc " +
                    "INNER JOIN cliente AS cli1 ON doc.cliente_id_1 = cli1.cliente_id " +
                    "INNER JOIN cliente AS cli2 ON doc.cliente_id_2 = cli2.cliente_id ";

            mCursor = db.rawQuery(queryCabecera + WHERE_CLAUSE + " " + where, params);
            if (mCursor != null && mCursor.getCount() > 0) {

                mCursor.moveToFirst();
                documento = new Documento();
                documento.setId(mCursor.getInt(mCursor.getColumnIndex(COLUMN_NAME_IDXXXX)));
                documento.setFechaEmision(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_FECEMI)));
                documento.setFechaVencimiento(documento.getFechaEmision());
                documento.setMoneda(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_MONEDA)));
                documento.setTipoDocumento(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_TIPDOC)));
                documento.setSerie(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_SERIEX)));
                documento.setNumero(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_NUMERO)));
                documento.setBaseImponible(mCursor.getInt(mCursor.getColumnIndex(COLUMN_NAME_BASEIM)));
                documento.setOperacionesInafectas(mCursor.getInt(mCursor.getColumnIndex(COLUMN_NAME_OPEINA)));
                documento.setOperacionesExoneradas(mCursor.getInt(mCursor.getColumnIndex(COLUMN_NAME_OPEEXO)));
                documento.setOperacionesGratuitas(mCursor.getInt(mCursor.getColumnIndex(COLUMN_NAME_OPEGRA)));
                documento.setPorcentajeIgv(mCursor.getInt(mCursor.getColumnIndex(COLUMN_NAME_PCTIGV)));
                documento.setIgv(mCursor.getInt(mCursor.getColumnIndex(COLUMN_NAME_IGVXXX)));
                documento.setImporte(mCursor.getInt(mCursor.getColumnIndex(COLUMN_NAME_IMPORT)));
                documento.setTipoOperacion(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_TIPOPE)));
                documento.setDigestValue(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_DIGEST)));
                documento.setFechaGeneracionXml(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_FECGEN)));
                documento.setFechaEnvioXml(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_FECENV)));
                documento.setSituacion(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_SITUAC)));
                documento.setObservaciones(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_OBSERV)));
                documento.setTicket(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_TICKET)));
                documento.setGlosa2(documento.getTipoDocumento().name() + " " + documento.getNumeroConSerie());
                documento.setSaldo(documento.getImporte());
                documento.setSemana(Fecha.getNumeroSemana(documento.getFechaEmision()));
                documento.setSemana2(documento.getSemana());
                documento.setFechaEnvioAvivel(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_FECAVI)));
                documento.setSituacionAvivel(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_SITAVI)));


                cliente1 = new Cliente();
                cliente1.setId(mCursor.getInt(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_ID)));
                cliente1.setTipoDocumentoIdentidad(mCursor.getString(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_TIPODOC)));
                cliente1.setNumeroDocumento(mCursor.getString(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_NUMDOC)));
                cliente1.setRazonSocial(mCursor.getString(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_RAZSOC)));
                cliente1.setDireccion(mCursor.getString(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_DIRECC)));
                cliente1.setDistrito(mCursor.getString(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_DISTRI)));
                cliente1.setProvincia(mCursor.getString(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_PROVIN)));
                cliente1.setDepartamento(mCursor.getString(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_DEPART)));
                cliente1.setCorreo(mCursor.getString(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_CORREO)));
                cliente1.setLatitud(mCursor.getDouble(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_LATITU)));
                cliente1.setLongitud(mCursor.getDouble(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_LONGIT)));
                documento.setCliente(cliente1);

                cliente2 = new Cliente();

                cliente2.setId(mCursor.getInt(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_ID + "_2")));
                cliente2.setTipoDocumentoIdentidad(mCursor.getString(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_TIPODOC + "_2")));
                cliente2.setNumeroDocumento(mCursor.getString(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_NUMDOC + "_2")));
                cliente2.setRazonSocial(mCursor.getString(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_RAZSOC + "_2")));
                cliente2.setDireccion(mCursor.getString(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_DIRECC + "_2")));
                cliente2.setDistrito(mCursor.getString(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_DISTRI + "_2")));
                cliente2.setProvincia(mCursor.getString(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_PROVIN + "_2")));
                cliente2.setDepartamento(mCursor.getString(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_DEPART + "_2")));
                cliente2.setCorreo(mCursor.getString(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_CORREO + "_2")));
                cliente2.setLatitud(mCursor.getDouble(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_LATITU + "_2")));
                cliente2.setLongitud(mCursor.getDouble(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_LONGIT + "_2")));

                documento.setClienteDestino(cliente2);

                String queryDetalles = "SELECT " +
                        "det.detalledocumento_id," +
                        "det.detalledocumento_secuencia," +
                        "det.detalledocumento_cantidad," +
                        "det.detalledocumento_unidades," +
                        "det.detalledocumento_precio," +
                        "det.detalledocumento_importe," +
                        "art.articulo_id," +
                        "art.articulo_codigo," +
                        "art.articulo_descripcion," +
                        "art.articulo_unidadmedida " +
                        "FROM " +
                        "detalle_documento AS det " +
                        "INNER JOIN articulo AS art ON det.articulo_id = art.articulo_id " +
                        "WHERE " +
                        "det.documento_id = ?";

                mCursorDetalles = db.rawQuery(queryDetalles, new String[]{String.valueOf(documento.getId())});
                detalleDocumentoList = new ArrayList<>();

                while (mCursorDetalles.moveToNext()) {

                    detalleDocumento = new DetalleDocumento();
                    detalleDocumento.setId(mCursorDetalles.getInt(mCursorDetalles.getColumnIndex(DetalleDocumentoDbHelper.COLUMN_NAME_IDXXXX)));
                    detalleDocumento.setDocumento(documento);
                    detalleDocumento.setSecuencia(mCursorDetalles.getInt(mCursorDetalles.getColumnIndex(DetalleDocumentoDbHelper.COLUMN_NAME_SECUEN)));
                    detalleDocumento.setCantidad(mCursorDetalles.getInt(mCursorDetalles.getColumnIndex(DetalleDocumentoDbHelper.COLUMN_NAME_CANTID)));
                    detalleDocumento.setUnidades(mCursorDetalles.getInt(mCursorDetalles.getColumnIndex(DetalleDocumentoDbHelper.COLUMN_NAME_UNIDAD)));
                    detalleDocumento.setPrecio(mCursorDetalles.getInt(mCursorDetalles.getColumnIndex(DetalleDocumentoDbHelper.COLUMN_NAME_PRECIO)));
                    detalleDocumento.setImporte(mCursorDetalles.getInt(mCursorDetalles.getColumnIndex(DetalleDocumentoDbHelper.COLUMN_NAME_IMPORT)));

                    articulo = new Articulo();
                    articulo.setId(mCursorDetalles.getInt(mCursorDetalles.getColumnIndex(ArticuloDbHelper.COLUMN_NAME_IDXXXX)));
                    articulo.setCodigo(mCursorDetalles.getString(mCursorDetalles.getColumnIndex(ArticuloDbHelper.COLUMN_NAME_CODIGO)));
                    articulo.setDescripcion(mCursorDetalles.getString(mCursorDetalles.getColumnIndex(ArticuloDbHelper.COLUMN_NAME_DESCRI)));
                    articulo.setUnidadMedida(mCursorDetalles.getString(mCursorDetalles.getColumnIndex(ArticuloDbHelper.COLUMN_NAME_UNIMED)));

                    detalleDocumento.setArticulo(articulo);
                    detalleDocumentoList.add(detalleDocumento);
                }
                documento.setDetalleDocumentoList(detalleDocumentoList);
            }

        } catch (Exception ex) {
            Log.e("DATABASE", ex.getLocalizedMessage(), ex);
            throw new DbHelperException(ex.getLocalizedMessage(), ex);
        } finally {
            if (mCursor != null) {
                mCursor.close();
            }
            if (mCursorDetalles != null) {
                mCursorDetalles.close();
            }
        }
        return documento;
    }

    public Documento getDocumentoById(Integer id) throws DbHelperException {
        return getDocumento("documento_id = ?", new String[]{String.valueOf(id)});
    }

    public Documento getDocumentoByTipoAndSerieAndNumero(TipoDocumento tipoDocumento, String serie, String numero) throws DbHelperException {
        return getDocumento("documento_tipodocumento = ? AND documento_serie = ? AND documento_numero = ?", new String[]{tipoDocumento.name(), serie, numero});
    }

    public List<Documento> getDocumentoListByFechaEmision(Date fecha) throws DbHelperException {
        Documento documento;
        List<Documento> documentoList;
        Cliente cliente1, cliente2;
        DetalleDocumento detalleDocumento;
        Articulo articulo;
        List<DetalleDocumento> detalleDocumentoList;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor mCursor = null;
        Cursor mCursorDetalles = null;

        try {
            String queryCabecera = "SELECT " +
                    "doc.documento_id," +
                    "doc.documento_fechaemision," +
                    "doc.documento_moneda," +
                    "doc.documento_tipodocumento," +
                    "doc.documento_serie," +
                    "doc.documento_numero," +
                    "doc.documento_porcentajeigv," +
                    "doc.documento_baseimponible," +
                    "doc.documento_operacionesinafectas," +
                    "doc.documento_operacionesexoneradas," +
                    "doc.documento_operacionesgratuitas," +
                    "doc.documento_igv," +
                    "doc.documento_importe," +
                    "doc.documento_tipooperacion," +
                    "doc.documento_digestvalue," +
                    "doc.documento_fechageneracionxml," +
                    "doc.documento_fechaenvioxml," +
                    "doc.documento_situacion," +
                    "doc.documento_observaciones," +
                    "doc.documento_ticket," +
                    "doc.documento_fechaenvioavivel," +
                    "doc.documento_situacionavivel," +
                    "cli1.cliente_id," +
                    "cli1.cliente_tipodocumento," +
                    "cli1.cliente_numerodocumento," +
                    "cli1.cliente_razonsocial," +
                    "cli1.cliente_direccion," +
                    "cli1.cliente_distrito," +
                    "cli1.cliente_provincia," +
                    "cli1.cliente_departamento," +
                    "cli1.cliente_correo," +
                    "cli1.cliente_latitud," +
                    "cli1.cliente_longitud," +
                    "cli2.cliente_id AS cliente_id_2," +
                    "cli2.cliente_tipodocumento AS cliente_tipodocumento_2," +
                    "cli2.cliente_numerodocumento AS cliente_numerodocumento_2," +
                    "cli2.cliente_razonsocial AS cliente_razonsocial_2," +
                    "cli2.cliente_direccion AS cliente_direccion_2," +
                    "cli2.cliente_distrito AS cliente_distrito_2," +
                    "cli2.cliente_provincia AS cliente_provincia_2," +
                    "cli2.cliente_departamento AS cliente_departamento_2," +
                    "cli2.cliente_correo AS cliente_correo_2," +
                    "cli2.cliente_latitud AS cliente_latitud_2," +
                    "cli2.cliente_longitud AS cliente_longitud_2 " +
                    "FROM " +
                    "documento AS doc " +
                    "INNER JOIN cliente AS cli1 ON doc.cliente_id_1 = cli1.cliente_id " +
                    "INNER JOIN cliente AS cli2 ON doc.cliente_id_2 = cli2.cliente_id " +
                    "WHERE " +
                    "doc.documento_fechaemision = ? " +
                    "ORDER BY documento_id";

            mCursor = db.rawQuery(queryCabecera, new String[]{Fecha.toStringMySQL(fecha)});

            documentoList = new ArrayList<>();
            while (mCursor.moveToNext()) {
                Log.i("DATABASE", "ENTRA");

                documento = new Documento();
                documento.setId(mCursor.getInt(mCursor.getColumnIndex(COLUMN_NAME_IDXXXX)));
                documento.setFechaEmision(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_FECEMI)));
                documento.setFechaVencimiento(documento.getFechaEmision());
                documento.setMoneda(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_MONEDA)));
                documento.setTipoDocumento(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_TIPDOC)));
                documento.setSerie(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_SERIEX)));
                documento.setNumero(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_NUMERO)));
                documento.setBaseImponible(mCursor.getInt(mCursor.getColumnIndex(COLUMN_NAME_BASEIM)));
                documento.setOperacionesInafectas(mCursor.getInt(mCursor.getColumnIndex(COLUMN_NAME_OPEINA)));
                documento.setOperacionesExoneradas(mCursor.getInt(mCursor.getColumnIndex(COLUMN_NAME_OPEEXO)));
                documento.setOperacionesGratuitas(mCursor.getInt(mCursor.getColumnIndex(COLUMN_NAME_OPEGRA)));
                documento.setPorcentajeIgv(mCursor.getInt(mCursor.getColumnIndex(COLUMN_NAME_PCTIGV)));
                documento.setIgv(mCursor.getInt(mCursor.getColumnIndex(COLUMN_NAME_IGVXXX)));
                documento.setImporte(mCursor.getInt(mCursor.getColumnIndex(COLUMN_NAME_IMPORT)));
                documento.setTipoOperacion(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_TIPOPE)));
                documento.setDigestValue(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_DIGEST)));
                documento.setFechaGeneracionXml(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_FECGEN)));
                documento.setFechaEnvioXml(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_FECENV)));
                documento.setSituacion(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_SITUAC)));
                documento.setObservaciones(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_OBSERV)));
                documento.setTicket(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_TICKET)));
                documento.setGlosa2(documento.getTipoDocumento().name() + " " + documento.getNumeroConSerie());
                documento.setSaldo(documento.getImporte());
                documento.setSemana(Fecha.getNumeroSemana(documento.getFechaEmision()));
                documento.setSemana2(documento.getSemana());
                documento.setFechaEnvioAvivel(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_FECAVI)));
                documento.setSituacionAvivel(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_SITAVI)));

                cliente1 = new Cliente();
                cliente1.setId(mCursor.getInt(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_ID)));
                cliente1.setTipoDocumentoIdentidad(mCursor.getString(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_TIPODOC)));
                cliente1.setNumeroDocumento(mCursor.getString(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_NUMDOC)));
                cliente1.setRazonSocial(mCursor.getString(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_RAZSOC)));
                cliente1.setDireccion(mCursor.getString(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_DIRECC)));
                cliente1.setDistrito(mCursor.getString(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_DISTRI)));
                cliente1.setProvincia(mCursor.getString(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_PROVIN)));
                cliente1.setDepartamento(mCursor.getString(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_DEPART)));
                cliente1.setCorreo(mCursor.getString(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_CORREO)));
                cliente1.setLatitud(mCursor.getDouble(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_LATITU)));
                cliente1.setLongitud(mCursor.getDouble(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_LONGIT)));
                documento.setCliente(cliente1);

                cliente2 = new Cliente();
                cliente2.setId(mCursor.getInt(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_ID + "_2")));
                cliente2.setTipoDocumentoIdentidad(mCursor.getString(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_TIPODOC + "_2")));
                cliente2.setNumeroDocumento(mCursor.getString(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_NUMDOC + "_2")));
                cliente2.setRazonSocial(mCursor.getString(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_RAZSOC + "_2")));
                cliente2.setDireccion(mCursor.getString(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_DIRECC + "_2")));
                cliente2.setDistrito(mCursor.getString(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_DISTRI + "_2")));
                cliente2.setProvincia(mCursor.getString(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_PROVIN + "_2")));
                cliente2.setDepartamento(mCursor.getString(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_DEPART + "_2")));
                cliente2.setCorreo(mCursor.getString(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_CORREO + "_2")));
                cliente2.setLatitud(mCursor.getDouble(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_LATITU + "_2")));
                cliente2.setLongitud(mCursor.getDouble(mCursor.getColumnIndex(ClienteDbHelper.COLUMN_NAME_LONGIT + "_2")));

                documento.setClienteDestino(cliente2);

                String queryDetalles = "SELECT " +
                        "det.detalledocumento_id," +
                        "det.detalledocumento_secuencia," +
                        "det.detalledocumento_cantidad," +
                        "det.detalledocumento_unidades," +
                        "det.detalledocumento_precio," +
                        "det.detalledocumento_importe," +
                        "art.articulo_id," +
                        "art.articulo_codigo," +
                        "art.articulo_descripcion," +
                        "art.articulo_unidadmedida " +
                        "FROM " +
                        "detalle_documento AS det " +
                        "INNER JOIN articulo AS art ON det.articulo_id = art.articulo_id " +
                        "WHERE " +
                        "det.documento_id = ?";

                mCursorDetalles = db.rawQuery(queryDetalles, new String[]{String.valueOf(documento.getId())});
                detalleDocumentoList = new ArrayList<>();

                while (mCursorDetalles.moveToNext()) {

                    detalleDocumento = new DetalleDocumento();
                    detalleDocumento.setId(mCursorDetalles.getInt(mCursorDetalles.getColumnIndex(DetalleDocumentoDbHelper.COLUMN_NAME_IDXXXX)));
                    detalleDocumento.setDocumento(documento);
                    detalleDocumento.setSecuencia(mCursorDetalles.getInt(mCursorDetalles.getColumnIndex(DetalleDocumentoDbHelper.COLUMN_NAME_SECUEN)));
                    detalleDocumento.setCantidad(mCursorDetalles.getInt(mCursorDetalles.getColumnIndex(DetalleDocumentoDbHelper.COLUMN_NAME_CANTID)));
                    detalleDocumento.setUnidades(mCursorDetalles.getInt(mCursorDetalles.getColumnIndex(DetalleDocumentoDbHelper.COLUMN_NAME_UNIDAD)));
                    detalleDocumento.setPrecio(mCursorDetalles.getInt(mCursorDetalles.getColumnIndex(DetalleDocumentoDbHelper.COLUMN_NAME_PRECIO)));
                    detalleDocumento.setImporte(mCursorDetalles.getInt(mCursorDetalles.getColumnIndex(DetalleDocumentoDbHelper.COLUMN_NAME_IMPORT)));

                    articulo = new Articulo();
                    articulo.setId(mCursorDetalles.getInt(mCursorDetalles.getColumnIndex(ArticuloDbHelper.COLUMN_NAME_IDXXXX)));
                    articulo.setCodigo(mCursorDetalles.getString(mCursorDetalles.getColumnIndex(ArticuloDbHelper.COLUMN_NAME_CODIGO)));
                    articulo.setDescripcion(mCursorDetalles.getString(mCursorDetalles.getColumnIndex(ArticuloDbHelper.COLUMN_NAME_DESCRI)));
                    articulo.setUnidadMedida(mCursorDetalles.getString(mCursorDetalles.getColumnIndex(ArticuloDbHelper.COLUMN_NAME_UNIMED)));

                    detalleDocumento.setArticulo(articulo);
                    detalleDocumentoList.add(detalleDocumento);
                }
                documento.setDetalleDocumentoList(detalleDocumentoList);

                documentoList.add(documento);
            }
            Log.i("DATABASE", "Lista de documentos obtenida satisfactoriamente. Longitud: " + documentoList.size());
        } catch (Exception ex) {
            Log.e("DATABASE", ex.getLocalizedMessage(), ex);
            throw new DbHelperException(ex.getLocalizedMessage(), ex);
        } finally {
            if (mCursor != null) {
                mCursor.close();
            }
            if (mCursorDetalles != null) {
                mCursorDetalles.close();
            }
        }
        return documentoList;
    }

    public BigDecimal getVentasPorDia(Date fecha) throws DbHelperException {
        BigDecimal totalVentas = BigDecimal.ZERO;

        SQLiteDatabase db;
        Cursor mCursor = null;

        try {
            db = this.getWritableDatabase();
            String query = "SELECT " +
                    "SUM(documento.documento_importe) AS total_ventas " +
                    "FROM " +
                    "documento " +
                    "WHERE documento_fechaemision = ?";

            mCursor = db.rawQuery(query, new String[]{Fecha.toStringMySQL(fecha)});

            if (mCursor != null && mCursor.getCount() > 0) {
                mCursor.moveToFirst();
                int total_ventas = mCursor.getInt(mCursor.getColumnIndex("total_ventas"));
                totalVentas = new BigDecimal(total_ventas).divide(new BigDecimal("100.00"));
            }
        } catch (Exception ex) {
            Log.e("DATABASE", ex.getLocalizedMessage(), ex);
            throw new DbHelperException(ex.getLocalizedMessage(), ex);
        } finally {
            if (mCursor != null) {
                mCursor.close();
            }
        }
        return totalVentas;
    }

    public String getNumeroDisponibleByTipoAndSerie(TipoDocumento tipoDocumento, String serie) throws DbHelperException {
        String numeroDisponible;

        SQLiteDatabase db;
        Cursor mCursor = null;

        try {
            db = this.getWritableDatabase();
            mCursor = db.query(TABLE_NAME, new String[]{COLUMN_NAME_NUMERO}, "documento_tipodocumento = ? AND documento_serie = ?", new String[]{tipoDocumento.name(), serie}, null, null, COLUMN_NAME_NUMERO + " DESC", "1");

            if (mCursor != null && mCursor.getCount() > 0) {
                mCursor.moveToFirst();
                int numeroActual = Integer.parseInt(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_NUMERO)));
                numeroDisponible = PlainText.alinearDerecha(String.valueOf(numeroActual + 1), 7, "0");
            } else {
                numeroDisponible = "0000001";
            }
            Log.i("EFACT", "Número disponible para " + tipoDocumento.name() + " serie " + serie + ": " + numeroDisponible);
        } catch (Exception ex) {
            Log.e("DATABASE", ex.getLocalizedMessage(), ex);
            throw new DbHelperException(ex.getLocalizedMessage(), ex);
        } finally {
            if (mCursor != null) {
                mCursor.close();
            }
        }
        return numeroDisponible;
    }

    public List<DetalleDocumento> getDetalleListByFecha(Date fecha) throws DbHelperException {
        List<DetalleDocumento> detalleDocumentoList;
        Articulo articulo;
        DetalleDocumento detalleDocumento;
        SQLiteDatabase db;
        Cursor mCursor = null;

        try {

            db = this.getWritableDatabase();

            String query = "SELECT " +
                    "art.articulo_codigo, " +
                    "art.articulo_descripcion, " +
                    "art.articulo_unidadmedida, " +
                    "SUM(det.detalledocumento_cantidad) AS detalledocumento_cantidad, " +
                    "SUM(det.detalledocumento_unidades) AS detalledocumento_unidades, " +
                    "SUM(det.detalledocumento_importe) AS detalledocumento_importe " +
                    "FROM " +
                    "detalle_documento AS det " +
                    "INNER JOIN documento AS doc ON det.documento_id = doc.documento_id " +
                    "INNER JOIN articulo AS art ON det.articulo_id = art.articulo_id " +
                    "WHERE doc.documento_fechaemision =  ? " +
                    "GROUP BY " +
                    "art.articulo_codigo";

            mCursor = db.rawQuery(query, new String[]{Fecha.toStringMySQL(fecha)});

            detalleDocumentoList = new ArrayList<>();
            while (mCursor.moveToNext()) {
                articulo = new Articulo();

                articulo.setCodigo(mCursor.getString(mCursor.getColumnIndex(ArticuloDbHelper.COLUMN_NAME_CODIGO)));
                articulo.setDescripcion(mCursor.getString(mCursor.getColumnIndex(ArticuloDbHelper.COLUMN_NAME_DESCRI)));
                articulo.setUnidadMedida(mCursor.getString(mCursor.getColumnIndex(ArticuloDbHelper.COLUMN_NAME_UNIMED)));

                detalleDocumento = new DetalleDocumento();

                detalleDocumento.setArticulo(articulo);
                detalleDocumento.setCantidad(mCursor.getInt(mCursor.getColumnIndex(DetalleDocumentoDbHelper.COLUMN_NAME_CANTID)));
                detalleDocumento.setUnidades(mCursor.getInt(mCursor.getColumnIndex(DetalleDocumentoDbHelper.COLUMN_NAME_UNIDAD)));
                detalleDocumento.setImporte(mCursor.getInt(mCursor.getColumnIndex(DetalleDocumentoDbHelper.COLUMN_NAME_IMPORT)));
                if (detalleDocumento.getImporte().compareTo(BigDecimal.ZERO) != 0) {
                    detalleDocumento.setPrecio(detalleDocumento.getImporte().divide(detalleDocumento.getCantidad(), 2, RoundingMode.HALF_UP));
                } else {
                    detalleDocumento.setPrecio(BigDecimal.ZERO);
                }
                detalleDocumentoList.add(detalleDocumento);
            }
            Log.i("DATABASE", "Resumen de ventas del día: " + detalleDocumentoList.size());
        } catch (Exception ex) {
            Log.e("DATABASE", ex.getLocalizedMessage(), ex);
            throw new DbHelperException(ex.getLocalizedMessage(), ex);
        } finally {
            if (mCursor != null) {
                mCursor.close();
            }
        }
        return detalleDocumentoList;
    }

    public Integer getComprobantes(Date fecha, TipoDocumento tipoDocumento) throws DbHelperException {
        Integer comprobantes = 0;

        SQLiteDatabase db;
        Cursor mCursor = null;

        try {
            db = this.getWritableDatabase();

            String query = "SELECT " +
                    "COUNT(1) AS documento_numero " +
                    "FROM " +
                    "documento AS doc " +
                    "WHERE doc.documento_fechaemision =  ? AND doc.documento_tipodocumento = ?";

            mCursor = db.rawQuery(query, new String[]{Fecha.toStringMySQL(fecha), tipoDocumento.name()});

            if (mCursor != null && mCursor.getCount() > 0) {
                mCursor.moveToFirst();
                comprobantes = mCursor.getInt(mCursor.getColumnIndex(COLUMN_NAME_NUMERO));
            }
            Log.i("DATABASE", "Cantidad de " + tipoDocumento.name() + ": " + comprobantes);
        } catch (Exception ex) {
            Log.e("DATABASE", ex.getLocalizedMessage(), ex);
            throw new DbHelperException(ex.getLocalizedMessage(), ex);
        } finally {
            if (mCursor != null) {
                mCursor.close();
            }
        }
        return comprobantes;
    }


}
