package pe.com.avivel.autoventa.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import pe.com.avivel.autoventa.model.Empresa;

public class EmpresaDbHelper extends FacturacionHelper {

    private static EmpresaDbHelper instance;

    private static String TABLE_NAME = "empresa";
    private final static String COLUMN_NAME_ID = "empresa_id";
    private final static String COLUMN_NAME_RUC = "empresa_ruc";
    private final static String COLUMN_NAME_RAZON_SOCIAL = "empresa_razonsocial";
    private final static String COLUMN_NAME_DIRECCION = "empresa_direccion";
    private final static String COLUMN_NAME_TELEFONO = "empresa_telefono";
    private final static String COLUMN_NAME_CORREO = "empresa_correo";

    private EmpresaDbHelper(Context context) {
        super(context);
    }

    public static EmpresaDbHelper getInstance(Context context) {
        if (instance == null) {
            instance = new EmpresaDbHelper(context);
        }
        return instance;
    }

    /**
     * Crea tabla empresa
     *
     * @param db base de datos
     */
    static void createTable(SQLiteDatabase db) {
        String query = "create table " + TABLE_NAME + "( " +
                COLUMN_NAME_ID + " integer primary key AUTOINCREMENT, " +
                COLUMN_NAME_RUC + " text null, " +
                COLUMN_NAME_RAZON_SOCIAL + " text null, " +
                COLUMN_NAME_DIRECCION + " text null, " +
                COLUMN_NAME_TELEFONO + " text null, " +
                COLUMN_NAME_CORREO + " text null " +
                ")";
        db.execSQL(query);
    }

    static void dropTable(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    }

    public long insert(Empresa empresa) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME_RUC, empresa.getRuc());
        values.put(COLUMN_NAME_RAZON_SOCIAL, empresa.getRazonSocial());
        values.put(COLUMN_NAME_DIRECCION, empresa.getDireccion());
        values.put(COLUMN_NAME_TELEFONO, empresa.getTelefono());
        values.put(COLUMN_NAME_CORREO, empresa.getCorreo());
        return db.insert(TABLE_NAME, null, values);
    }

    public long update(Empresa empresa) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(COLUMN_NAME_RUC, empresa.getRuc());
        values.put(COLUMN_NAME_RAZON_SOCIAL, empresa.getRazonSocial());
        values.put(COLUMN_NAME_DIRECCION, empresa.getDireccion());
        values.put(COLUMN_NAME_TELEFONO, empresa.getTelefono());
        values.put(COLUMN_NAME_CORREO, empresa.getCorreo());

        return db.update(TABLE_NAME, values, "empresa_id=?", new String[]{String.valueOf(empresa.getId())});
    }

    public int deleteById(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME, "empresa_id=?", new String[]{String.valueOf(id)});
    }

    public int deleteAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME, null, null);
    }

    private Empresa getEmpresa(String where, String[] params) {
        Empresa empresa = null;
        SQLiteDatabase db = this.getWritableDatabase();
        String[] cols = new String[]{COLUMN_NAME_ID, COLUMN_NAME_RUC, COLUMN_NAME_RAZON_SOCIAL, COLUMN_NAME_DIRECCION, COLUMN_NAME_TELEFONO, COLUMN_NAME_CORREO};
        Cursor mCursor = db.query(TABLE_NAME, cols, where, params, null, null, null, null);
        if (mCursor != null && mCursor.getCount() > 0) {
            mCursor.moveToFirst();
            empresa = new Empresa();
            empresa.setId(mCursor.getInt(mCursor.getColumnIndex(COLUMN_NAME_ID)));
            empresa.setRuc(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_RUC)));
            empresa.setRazonSocial(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_RAZON_SOCIAL)));
            empresa.setDireccion(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_DIRECCION)));
            empresa.setTelefono(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_TELEFONO)));
            empresa.setCorreo(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_CORREO)));
        }
        if (mCursor != null) {
            mCursor.close();
        }
        return empresa;
    }

    public Empresa getEmpresaById(Integer id) {
        return getEmpresa("empresa_id=?", new String[]{String.valueOf(id)});
    }

    public List<Empresa> getAll() {
        Empresa empresa;
        List<Empresa> list = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        String[] cols = new String[]{COLUMN_NAME_ID, COLUMN_NAME_RUC, COLUMN_NAME_RAZON_SOCIAL, COLUMN_NAME_DIRECCION, COLUMN_NAME_TELEFONO, COLUMN_NAME_CORREO};
        Cursor mCursor = db.query(TABLE_NAME, cols, null, null, null, null, null, null);
        mCursor.moveToFirst();

        while (!mCursor.isAfterLast()) {
            empresa = new Empresa();
            empresa.setId(mCursor.getInt(mCursor.getColumnIndex(COLUMN_NAME_ID)));
            empresa.setRuc(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_RUC)));
            empresa.setRazonSocial(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_RAZON_SOCIAL)));
            empresa.setDireccion(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_DIRECCION)));
            empresa.setTelefono(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_TELEFONO)));
            empresa.setCorreo(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_CORREO)));
            list.add(empresa);
            mCursor.moveToNext();
        }
        mCursor.close();

        return list;
    }

}
