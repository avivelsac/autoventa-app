package pe.com.avivel.autoventa.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


/**
 * Created by jemarinero on 20/11/2017.
 */
public class FacturacionHelper extends SQLiteOpenHelper {

    private SQLiteDatabase database;

    //variables locales
    private static final String DATABASE_NAME = "Facturacion";
    private static final int DATABASE_VERSION = 17;
    private Empresa empr = new Empresa();
    private Numeradores num = new Numeradores();
    private Servicios ser = new Servicios();
    private RecibosEnc recEnc = new RecibosEnc();
    private RecibosDet recDet = new RecibosDet();

    //constructor
    public FacturacionHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        ConfiguracionDbHelper.createTable(db);
        ClienteDbHelper.createTableCliente(db);
        UbigeoDbHelper.createTable(db);
        ArticuloDbHelper.createTable(db);
        DetalleDocumentoDbHelper.createTable(db);
        DocumentoDbHelper.createTable(db);
        EmpresaDbHelper.createTable(db);
        CreateTableNumerador(db);
        CreateTableServicios(db);
        CreateTableRecibosEnc(db);
        CreateTableRecibosDet(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        ConfiguracionDbHelper.dropTable(db);
        DetalleDocumentoDbHelper.dropTable(db);
        DocumentoDbHelper.dropTable(db);
        ArticuloDbHelper.dropTable(db);
        ClienteDbHelper.dropTableCliente(db);
        UbigeoDbHelper.dropTable(db);
        EmpresaDbHelper.dropTable(db);
        DropTableNumerador(db);
        DropTableServicios(db);
        DropTableRecibosEnc(db);
        DropTableRecibosDet(db);
        this.onCreate(db);
    }


    /**
     * Open the databases connection.
     */
    public void open() {
        this.database = this.getWritableDatabase();
    }

    /**
     * Close the databases connection.
     */
    public void close() {
        if (database != null) {
            this.database.close();
            Log.i("database", "successfully closed database autoventadb.db");
        }
    }

    public SQLiteDatabase getDatabase() {
        return database;
    }


    //=================================================
    //EMPRESA
    //=================================================

    public void CreateTableEmpresa(SQLiteDatabase db) {
        String query = "create table " + empr.TABLE + "( " +
                "id integer primary key AUTOINCREMENT, " +
                "a text null, " +
                "b text null, " +
                "c text null, " +
                "d text null, " +
                "e text null " +
                ")";
        db.execSQL(query);
    }

    public void DropTableEmpresa(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + empr.TABLE);
    }

    public long insertEmpresa(String nombre, String direccion, String telefono, String correo, String rtn) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(empr.NOMBRE, nombre);
        values.put(empr.DIRECCION, direccion);
        values.put(empr.RTN, rtn);
        values.put(empr.TELEFONO, telefono);
        values.put(empr.CORREO, correo);
        return db.insert(empr.TABLE, null, values);
    }

    public int deleteAllEmpresa() {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(empr.TABLE, null, null);
    }

    public Cursor selectAllEmpresa() {
        SQLiteDatabase db = this.getWritableDatabase();
        String[] cols = new String[]{empr.ID, empr.NOMBRE, empr.DIRECCION, empr.RTN, empr.TELEFONO, empr.CORREO};
        Cursor mCursor = db.query(true, empr.TABLE, cols, null
                , null, null, null, null, null);
        return mCursor; // iterate to get each value.
    }

    public long updateEmpresa(String nombre, String direccion, String telefono, String correo, String rtn, String where, String[] params) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(empr.NOMBRE, nombre);
        values.put(empr.DIRECCION, direccion);
        values.put(empr.RTN, rtn);
        values.put(empr.TELEFONO, telefono);
        values.put(empr.CORREO, correo);
        return db.update(empr.TABLE, values, where, params);

    }


    //=================================================
    //Numeradores
    //=================================================

    public void CreateTableNumerador(SQLiteDatabase db) {
        String query = "create table " + num.TABLE + "( " +
                num.ID + " integer primary key AUTOINCREMENT, " +
                num.NUMERADOR + " text null, " +
                num.SERIE + " text null, " +
                num.NUMERO_INICIO + " integer null, " +
                num.NUMERO_FIN + " integer null, " +
                num.FECHA_INICIO + " text null, " +
                num.FECHA_FIN + " text null, " +
                num.CAI + " text null, " +
                num.ULTIMO_USADO + " integer null, " +
                num.ESTADO + " integer null " +
                ")";
        db.execSQL(query);
    }

    public void DropTableNumerador(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + num.TABLE);
    }

    public long insertNumerador(String numerador, String serie, String numeroInicio, String numeroFin, String fechaInicio, String fechaFin, String cai, String ultimo, String estado) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(num.NUMERADOR, numerador);
        values.put(num.SERIE, serie);
        values.put(num.NUMERO_INICIO, numeroInicio);
        values.put(num.NUMERO_FIN, numeroFin);
        values.put(num.FECHA_INICIO, fechaInicio);
        values.put(num.FECHA_FIN, fechaFin);
        values.put(num.CAI, cai);
        values.put(num.ULTIMO_USADO, ultimo);
        values.put(num.ESTADO, estado);
        return db.insert(num.TABLE, null, values);
    }

    public int deleteAllNumeradores() {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(num.TABLE, null, null);
    }

    public int deleteNumerador(String where, String[] params) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(num.TABLE, where, params);
    }

    public Cursor selectAllNumeradores() {
        SQLiteDatabase db = this.getWritableDatabase();
        String[] cols = new String[]{num.ID, num.NUMERADOR, num.SERIE, num.NUMERO_INICIO, num.NUMERO_FIN, num.FECHA_INICIO, num.FECHA_FIN, num.CAI, num.ULTIMO_USADO, num.ESTADO};
        Cursor mCursor = db.query(true, num.TABLE, cols, null
                , null, null, null, null, null);
        return mCursor; // iterate to get each value.
    }

    public Cursor selectNumerador(String where, String[] params) {
        SQLiteDatabase db = this.getWritableDatabase();
        String[] cols = new String[]{num.ID, num.NUMERADOR, num.SERIE, num.NUMERO_INICIO, num.NUMERO_FIN, num.FECHA_INICIO, num.FECHA_FIN, num.CAI, num.ULTIMO_USADO, num.ESTADO};
        Cursor mCursor = db.query(true, num.TABLE, cols, where
                , params, null, null, null, null);
        return mCursor; // iterate to get each value.
    }

    public long updateNumerador(String numerador, String serie, String numeroInicio, String numeroFin, String fechaInicio, String fechaFin, String cai, String ultimo, String estado, String where, String[] params) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(num.NUMERADOR, numerador);
        values.put(num.SERIE, serie);
        values.put(num.NUMERO_INICIO, numeroInicio);
        values.put(num.NUMERO_FIN, numeroFin);
        values.put(num.FECHA_INICIO, fechaInicio);
        values.put(num.FECHA_FIN, fechaFin);
        values.put(num.CAI, cai);
        values.put(num.ULTIMO_USADO, ultimo);
        values.put(num.ESTADO, estado);

        return db.update(num.TABLE, values, where, params);

    }

    public long updateNumeradorUltimo(String ultimo, String where, String[] params) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(num.ULTIMO_USADO, ultimo);

        return db.update(num.TABLE, values, where, params);

    }
    //=================================================
    //Encabezado Recibos
    //=================================================

    public void CreateTableRecibosEnc(SQLiteDatabase db) {
        String query = "create table " + recEnc.TABLE + "( " +
                recEnc.ID + " integer primary key AUTOINCREMENT, " +
                recEnc.NO_RECIBO + " integer null, " +
                recEnc.FECHA + " text null, " +
                recEnc.CAI + " text null, " +
                recEnc.CLIENTE + " integer null, " +
                recEnc.ESTADO + " integer null, " +
                recEnc.LATITUD + " text null, " +
                recEnc.LONGITUD + " text null " +
                ")";
        db.execSQL(query);
    }

    public void DropTableRecibosEnc(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + recEnc.TABLE);
    }

    public long insertReciboEnc(String noRecibo, String fecha, String cai, String cliente, String estado, String latitud, String longitud) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(recEnc.NO_RECIBO, noRecibo);
        values.put(recEnc.FECHA, fecha);
        values.put(recEnc.CAI, cai);
        values.put(recEnc.CLIENTE, cliente);
        values.put(recEnc.ESTADO, estado);
        values.put(recEnc.LATITUD, latitud);
        values.put(recEnc.LONGITUD, longitud);
        return db.insert(recEnc.TABLE, null, values);
    }


    public Cursor selectAllRecibosEnc() {
        SQLiteDatabase db = this.getWritableDatabase();
        String MY_QUERY = "SELECT a." + recEnc.ID + ",a."
                + recEnc.NO_RECIBO + ", a."
                + recEnc.CLIENTE + ",b."
                + ClienteDbHelper.COLUMN_NAME_RAZSOC + ",a."
                + recEnc.FECHA + ",a."
                + recEnc.CAI + ",a."
                + recEnc.ESTADO + ", ifnull(sum(c."
                + recDet.TOTAL + "),0) MONTO_TOTAL ,a."
                + recEnc.LATITUD + ",a."
                + recEnc.LONGITUD + ",a."
                + recEnc.ESTADO
                + " FROM " + recEnc.TABLE + " a"
                + " INNER JOIN " + ClienteDbHelper.TABLE_NAME + " b ON a."
                + recEnc.CLIENTE + "=b." + ClienteDbHelper.COLUMN_NAME_ID
                + " left JOIN " + recDet.TABLE + " c ON a."
                + recEnc.ID + "=c." + recDet.ID_RECIBO
                + " group by a." + recEnc.ID + ",a."
                + recEnc.NO_RECIBO + ", a."
                + recEnc.CLIENTE + ",b."
                + ClienteDbHelper.COLUMN_NAME_RAZSOC + ",a."
                + recEnc.FECHA + ",a."
                + recEnc.CAI + ",a."
                + recEnc.ESTADO + ",a."
                + recEnc.LATITUD + ",a."
                + recEnc.LONGITUD;

        Cursor mCursor = db.rawQuery(MY_QUERY, null);
        return mCursor; // iterate to get each value.
    }

    public Cursor selectAllRecibosEncByDate(String fecha) {
        SQLiteDatabase db = this.getWritableDatabase();
        String MY_QUERY = "SELECT a." + recEnc.ID + ",a."
                + recEnc.NO_RECIBO + ", a."
                + recEnc.CLIENTE + ",b."
                + ClienteDbHelper.COLUMN_NAME_RAZSOC + ",a."
                + recEnc.FECHA + ",a."
                + recEnc.CAI + ",a."
                + recEnc.ESTADO + ", ifnull(sum(c."
                + recDet.TOTAL + "),0) MONTO_TOTAL ,a."
                + recEnc.LATITUD + ",a."
                + recEnc.LONGITUD + ",a."
                + recEnc.ESTADO
                + " FROM " + recEnc.TABLE + " a"
                + " INNER JOIN " + ClienteDbHelper.TABLE_NAME + " b ON a."
                + recEnc.CLIENTE + "=b." + ClienteDbHelper.COLUMN_NAME_ID
                + " and a." + recEnc.FECHA + " = '" + fecha + "'"
                + " left JOIN " + recDet.TABLE + " c ON a."
                + recEnc.ID + "=c." + recDet.ID_RECIBO
                + " group by a." + recEnc.ID + ",a."
                + recEnc.NO_RECIBO + ", a."
                + recEnc.CLIENTE + ",b."
                + ClienteDbHelper.COLUMN_NAME_RAZSOC + ",a."
                + recEnc.FECHA + ",a."
                + recEnc.CAI + ",a."
                + recEnc.ESTADO + ",a."
                + recEnc.LATITUD + ",a."
                + recEnc.LONGITUD;

        Cursor mCursor = db.rawQuery(MY_QUERY, null);
        return mCursor; // iterate to get each value.
    }

    public Cursor selectSumRecibosEncByDate(String fecha) {
        SQLiteDatabase db = this.getWritableDatabase();
        String MY_QUERY = "SELECT ifnull(sum(c."
                + recDet.TOTAL + "),0) MONTO_TOTAL "
                + " FROM " + recEnc.TABLE + " a"
                + " INNER JOIN " + ClienteDbHelper.TABLE_NAME + " b ON a."
                + recEnc.CLIENTE + "=b." + ClienteDbHelper.COLUMN_NAME_ID
                + " and a." + recEnc.ESTADO + " = 2"
                + " and a." + recEnc.FECHA + " = '" + fecha + "'"
                + " inner JOIN " + recDet.TABLE + " c ON a."
                + recEnc.ID + "=c." + recDet.ID_RECIBO;

        Cursor mCursor = db.rawQuery(MY_QUERY, null);
        return mCursor; // iterate to get each value.
    }

    public Cursor selectReciboEnc(String where, String[] params) {
        SQLiteDatabase db = this.getWritableDatabase();
        String[] cols = new String[]{recEnc.ID, recEnc.NO_RECIBO, recEnc.FECHA, recEnc.CAI, recEnc.CLIENTE, recEnc.ESTADO, recEnc.LATITUD, recEnc.LONGITUD};
        Cursor mCursor = db.query(true, recEnc.TABLE, cols, where
                , params, null, null, null, null);
        return mCursor; // iterate to get each value.
    }

    public long updateReciboEncEstado(String estado, String where, String[] params) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(recEnc.ESTADO, estado);

        return db.update(recEnc.TABLE, values, where, params);

    }
    //=================================================
    //Detalle Recibos
    //=================================================

    public void CreateTableRecibosDet(SQLiteDatabase db) {
        String query = "create table " + recDet.TABLE + "( " +
                recDet.ID + " integer primary key AUTOINCREMENT, " +
                recDet.ID_RECIBO + " integer null, " +
                recDet.ID_SERVICIO + " integer null, " +
                recDet.CANTIDAD + " real null, " +
                recDet.PRECIO + " real null, " +
                recDet.IMPUESTO + " real null, " +
                recDet.TOTAL + " real null " +
                ")";
        db.execSQL(query);
    }

    public void DropTableRecibosDet(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + recDet.TABLE);
    }

    public long insertReciboDet(String idRecibo, String idArticulo, String cantidad, String precio, String impuesto, String total) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(recDet.ID_RECIBO, idRecibo);
        values.put(recDet.ID_SERVICIO, idArticulo);
        values.put(recDet.CANTIDAD, cantidad);
        values.put(recDet.PRECIO, precio);
        values.put(recDet.IMPUESTO, impuesto);
        values.put(recDet.TOTAL, total);
        return db.insert(recDet.TABLE, null, values);
    }

    public int deleteAllRecibosDet() {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(recDet.TABLE, null, null);
    }

    public int deleteReciboDet(String where, String[] params) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(recDet.TABLE, where, params);
    }


    public Cursor selectReciboDet(String idRecibo) {
        SQLiteDatabase db = this.getWritableDatabase();
        String MY_QUERY = "SELECT a." + recDet.ID
                + ", a." + recDet.ID_RECIBO
                + ", a." + recDet.ID_SERVICIO
                + ", b." + ser.NOMBRE
                + ", a." + recDet.CANTIDAD
                + ", a." + recDet.PRECIO
                + ", a." + recDet.IMPUESTO
                + ", a." + recDet.TOTAL
                + " FROM " + recDet.TABLE
                + " a INNER JOIN " + ser.TABLE + " b ON b."
                + ser.ID + "=a." + recDet.ID_SERVICIO
                + " WHERE a." + recDet.ID_RECIBO + "=?";

        Cursor mCursor = db.rawQuery(MY_QUERY, new String[]{String.valueOf(idRecibo)});
        return mCursor; // iterate to get each value.
    }

    public Cursor selectAllRecibosDet() {
        SQLiteDatabase db = this.getWritableDatabase();
        String[] cols = new String[]{recDet.ID, recDet.ID_RECIBO, recDet.ID_SERVICIO, recDet.CANTIDAD, recDet.PRECIO, recDet.IMPUESTO, recDet.TOTAL};
        Cursor mCursor = db.query(true, recDet.TABLE, cols, null
                , null, null, null, null, null);
        return mCursor; // iterate to get each value.
    }

    //=================================================
    //Servicios
    //=================================================

    public void CreateTableServicios(SQLiteDatabase db) {
        String query = "create table " + ser.TABLE + "( " +
                ser.ID + " integer primary key AUTOINCREMENT, " +
                ser.NOMBRE + " text null, " +
                ser.PRECIO + " real null, " +
                ser.IMPUESTO + " real null " +
                ")";
        db.execSQL(query);
    }

    public void DropTableServicios(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + ser.TABLE);
    }

    public long insertServicios(String nombre, String precio, String impuesto) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ser.NOMBRE, nombre);
        values.put(ser.PRECIO, precio);
        values.put(ser.IMPUESTO, impuesto);
        return db.insert(ser.TABLE, null, values);
    }

    public int deleteAllServicios() {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(ser.TABLE, null, null);
    }

    public int deleteServicio(String where, String[] params) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(ser.TABLE, where, params);
    }

    public Cursor selectAllServicios() {
        SQLiteDatabase db = this.getWritableDatabase();
        String[] cols = new String[]{ser.ID, ser.NOMBRE, ser.PRECIO, ser.IMPUESTO};
        Cursor mCursor = db.query(true, ser.TABLE, cols, null
                , null, null, null, null, null);
        return mCursor; // iterate to get each value.
    }

    public Cursor selectServicio(String where, String[] params) {
        SQLiteDatabase db = this.getWritableDatabase();
        String[] cols = new String[]{ser.ID, ser.NOMBRE, ser.PRECIO, ser.IMPUESTO};
        Cursor mCursor = db.query(true, ser.TABLE, cols, where
                , params, null, null, null, null);
        return mCursor; // iterate to get each value.
    }

    public long updateServicios(String nombre, String precio, String impuesto, String where, String[] params) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ser.NOMBRE, nombre);
        values.put(ser.PRECIO, precio);
        values.put(ser.IMPUESTO, impuesto);

        return db.update(ser.TABLE, values, where, params);

    }

}