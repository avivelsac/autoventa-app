package pe.com.avivel.autoventa.model;

public enum UnidadMedida {

    UND("UNIDADES", "NIU"),
    KGR("KILOGRAMOS", "KGM");

    private final String descripcion;
    private final String codigoUNECE;

    UnidadMedida(String descripcion, String codigoUNECE) {
        this.codigoUNECE = codigoUNECE;
        this.descripcion = descripcion;
    }

    public String getNombreCorto() {
        return name();
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getCodigoUNECE() {
        return codigoUNECE;
    }
}
