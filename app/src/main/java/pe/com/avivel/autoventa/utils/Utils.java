package pe.com.avivel.autoventa.utils;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Utils {

    private static final BigDecimal _100 = new BigDecimal("100.00");
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
    private static final SimpleDateFormat DATETIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
    private static final SimpleDateFormat DATE_FORMAT_PRINTABLE = new SimpleDateFormat("dd/MM/yyyy", Locale.US);

    public static String fromDateToString(Date date) {
        if (date == null) {
            return null;
        } else {
            return DATE_FORMAT.format(date);
        }
    }

    public static String fromDateToStringPrintable(Date date) {
        if (date == null) {
            return null;
        } else {
            return DATE_FORMAT_PRINTABLE.format(date);
        }
    }

    public static String fromDateTimeToString(Date date) {
        if (date == null) {
            return null;
        } else {
            return DATETIME_FORMAT.format(date);
        }
    }

    public static Date fromStringToDate(String source) {
        Date fecha = null;
        try {
            fecha = DATE_FORMAT.parse(source);
        } finally {
            return fecha;
        }
    }

    public static Date fromStringToDateTime(String source) {
        Date fecha = null;
        try {
            fecha = DATETIME_FORMAT.parse(source);
        } finally {
            return fecha;
        }
    }

    public static Integer fromBigDecimalToInteger(BigDecimal bigDecimal) {
        return (bigDecimal.multiply(_100)).intValue();
    }

    public static BigDecimal fromIntegerToBigDecimal(Integer integer) {
        return (new BigDecimal(integer).divide(_100));
    }

    public static String ifNull(String val, String inCaseOfNull) {
        return val != null ? val : inCaseOfNull;
    }

    public static boolean isBlank(String val) {
        return val == null || val.isEmpty();
    }

}
