package pe.com.avivel.autoventa.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import pe.com.avivel.autoventa.R;
import pe.com.avivel.autoventa.helpers.EmpresaDbHelper;
import pe.com.avivel.autoventa.model.Empresa;

public class EmpresaActivity extends AppCompatActivity {

    Context context;
    EmpresaDbHelper empresaDbHelper;
    long id = 0;
    private EditText txtRuc, txtRazonSocial, txtDireccion, txtTelefono, txtCorreo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empresa);
        context = this;

        //pe.com.avivel.autoventa.helpers
        empresaDbHelper = EmpresaDbHelper.getInstance(this);

        //botones
        Button btnGuardar = findViewById(R.id.btnSave);

        //campos
        txtRuc = findViewById(R.id.txtRucEmpresa);
        txtRazonSocial = findViewById(R.id.txtRazonSocialEmpresa);
        txtDireccion = findViewById(R.id.txtDireccionCliente);
        txtTelefono = findViewById(R.id.txtTelefonoEmpresa);
        txtCorreo = findViewById(R.id.txtCorreoEmpresa);

        // Input en mayúsculas
        txtRuc.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        txtRazonSocial.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        txtDireccion.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        txtTelefono.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        txtCorreo.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

        //listeners
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Empresa empresa = new Empresa();
                empresa.setRuc(txtRuc.getText().toString());
                empresa.setRazonSocial(txtRazonSocial.getText().toString());
                empresa.setDireccion(txtDireccion.getText().toString());
                empresa.setTelefono(txtTelefono.getText().toString());
                empresa.setCorreo(txtCorreo.getText().toString());

                long rs;
                if (id == 0) {
                    rs = empresaDbHelper.insert(empresa);
                } else {
                    empresa.setId((int) id);
                    rs = empresaDbHelper.update(empresa);
                }

                if (rs > 0) {
                    Toast.makeText(context, "Datos guardados existosamente.", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(context, "Se producjo un error al guardar los datos.", Toast.LENGTH_LONG).show();
                }
            }
        });

        getData();
    }


    private void getData() {
        List<Empresa> empresaList = empresaDbHelper.getAll();

        if (empresaList != null && !empresaList.isEmpty()) {
            Empresa empresa = empresaList.get(0);
            id = empresa.getId();
            txtRuc.setText(empresa.getRuc());
            txtRazonSocial.setText(empresa.getRazonSocial());
            txtDireccion.setText(empresa.getDireccion());
            txtTelefono.setText(empresa.getTelefono());
            txtCorreo.setText(empresa.getCorreo());
        }
    }
}
