package pe.com.avivel.autoventa.activity;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import pe.com.avivel.autoventa.R;
import pe.com.avivel.autoventa.exception.DbHelperException;
import pe.com.avivel.autoventa.helpers.ClienteDbHelper;
import pe.com.avivel.autoventa.model.Cliente;
import pe.com.avivel.autoventa.model.LocationTrack;


public class MapClienteActivity extends AppCompatActivity implements OnMapReadyCallback {

    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;

    private GoogleMap mMap;
    private Long idCliente;
    private ClienteDbHelper clienteDbHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_cliente);
        Bundle bundle = getIntent().getExtras();
        assert bundle != null;
        idCliente = bundle.getLong("idCliente");
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Add a marker in Lima and move the camera
        LatLng hn = new LatLng(-12.046328, -77.0442686);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(hn, 10));

        clienteDbHelper = ClienteDbHelper.getInstance(this);

        LatLng ll = getLatLog();

        mMap.addMarker(new MarkerOptions().position(ll));

        Button btnUbicar = findViewById(R.id.btnCoordenadas);

        btnUbicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMap.clear();
                LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                LocationTrack locationListener = new LocationTrack(getBaseContext());
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(MapClienteActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_ASK_PERMISSIONS);
                    } else {
                        assert locationManager != null;
                        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1, 0, locationListener);
                        LatLng lat = new LatLng(locationListener.getLatitude(), locationListener.getLongitude());
                        mMap.addMarker(new MarkerOptions().position(lat));
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(lat));
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(lat, 10));
                        String[] id = {Long.toString(idCliente)};
                        clienteDbHelper.updateClienteGPS(Double.toString(lat.latitude), Double.toString(lat.longitude), "cliente_id=?", id);
                    }

                } else {
                    assert locationManager != null;
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1, 0, locationListener);
                    LatLng lat = new LatLng(locationListener.getLatitude(), locationListener.getLongitude());
                    mMap.addMarker(new MarkerOptions().position(lat));
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(lat));
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(lat, 10));
                    String[] id = {Long.toString(idCliente)};
                    clienteDbHelper.updateClienteGPS(Double.toString(lat.latitude), Double.toString(lat.longitude), "cliente_id=?", id);
                }

            }
        });

    }

    private LatLng getLatLog() {
        if (idCliente > 0) {
            Cliente cliente = null;
            try {
                cliente = clienteDbHelper.getClienteById(idCliente);
                if (cliente != null) {
                    return new LatLng(cliente.getLatitud(), cliente.getLongitud());
                } else {
                    return new LatLng(0, 0);
                }
            } catch (Exception e) {
                Toast.makeText(MapClienteActivity.this, "Error al consultar los datos", Toast.LENGTH_SHORT).show();
            }
        }
        return new LatLng(0, 0);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    Toast.makeText(MapClienteActivity.this, "Se otorgaron los permisos", Toast.LENGTH_SHORT)
                            .show();
                } else {
                    // Permission Denied
                    Toast.makeText(MapClienteActivity.this, "Se negaron los permisos", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
