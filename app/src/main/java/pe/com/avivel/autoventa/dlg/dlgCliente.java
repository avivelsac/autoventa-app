package pe.com.avivel.autoventa.dlg;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import java.util.List;

import pe.com.avivel.autoventa.R;
import pe.com.avivel.autoventa.helpers.ClienteDbHelper;
import pe.com.avivel.autoventa.model.Cliente;

public class dlgCliente extends Dialog {

    private Context mContext;
    private Spinner mSpinner;
    private DialogListener mReadyListener;

    private ClienteDbHelper clienteDbHelper;
    private ArrayAdapter<Cliente> dataAdapterCliente;
    private Button botonOk, botonCancel;

    private String idCliente = "0";

    public dlgCliente(Context context, DialogListener dl) {
        super(context);
        mReadyListener = dl;
        mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dlg_add_recibo);

        clienteDbHelper = ClienteDbHelper.getInstance(mContext);

        mSpinner = findViewById(R.id.cbxCliente);

        dataAdapterCliente = new ArrayAdapter<>(mContext, android.R.layout.simple_spinner_dropdown_item);
        dataAdapterCliente.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(dataAdapterCliente);

        cargarDataSpinnerCliente();

        botonOk = (Button) findViewById(R.id.btn_yes);
        //find cancel button
        botonCancel = (Button) findViewById(R.id.btn_no);
        //set action
        botonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //send cancel action to the interface
                mReadyListener.cancelled();
                //close dialog
                dismiss();
            }
        });

        botonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!idCliente.equals("0")) {
                    mReadyListener.ready(idCliente);
                }
                dismiss();
            }
        });

        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Cliente selectedCliente = (Cliente) mSpinner.getSelectedItem();
                idCliente = String.valueOf(selectedCliente.getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void cargarDataSpinnerCliente() {
        /*List<Cliente> clienteList = clienteDbHelper.getAllClientes();


        dataAdapterCliente.clear();
        dataAdapterCliente.addAll(clienteList);*/
    }

    public interface DialogListener {
        void ready(String idCliente);

        void cancelled();
    }
}
