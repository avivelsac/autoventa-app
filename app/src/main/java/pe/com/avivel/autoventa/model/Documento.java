package pe.com.avivel.autoventa.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import pe.com.avivel.autoventa.fe.FacturadorEfact;
import pe.com.avivel.autoventa.utils.Fecha;
import pe.com.avivel.autoventa.utils.Utils;

/**
 * @author André Bedregal <andre.bedregal at avivel.com.pe>
 */
@JsonRootName("documento")
public class Documento {

    private BigDecimal baseImponible;
    private BigDecimal igv;
    private BigDecimal importe;
    private BigDecimal operacionesExoneradas;
    private BigDecimal operacionesGratuitas;
    private BigDecimal operacionesInafectas;
    private BigDecimal porcentajeIgv;
    private BigDecimal saldo;
    private BigDecimal tipoCambio = BigDecimal.ONE;
    private Boolean impreso = true;
    private Boolean itinerante = true;
    private Boolean sujetoSpot = false;
    private CanalVenta canalVenta = CanalVenta.AUTOVENTA;
    private Cliente cliente;
    private Cliente clienteDestino;
    @JsonFormat(pattern = "dd-MM-yyyy", timezone = "America/Lima")
    private Date fechaEmision;
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss", timezone = "America/Lima")
    private Date fechaEnvioXml;
    @JsonFormat(pattern = "dd-MM-yyyy HH:mm:ss", timezone = "America/Lima")
    private Date fechaGeneracionXml;
    @JsonFormat(pattern = "dd-MM-yyyy", timezone = "America/Lima")
    private Date fechaVencimiento;
    @JsonIgnore
    private Integer id;
    private Integer semana;
    private Integer semana2;
    private MedioPago medioPago = MedioPago.DEPOSITO;
    private Moneda moneda;
    private String cuenta = "121201";
    private String digestValue;
    private String glosa = "VENTA REALIZADA POR EMISOR ITINERANTE";
    private String glosa2;
    private String numero;
    private String observaciones;
    private String serie;
    private String situacion;
    private String ticket;
    private Subdiario subdiario = Subdiario._05;
    private TipoConversion tipoConversion = TipoConversion.C;
    private TipoDocumento tipoDocumento;
    private TipoOperacion tipoOperacion;

    @JsonIgnore
    private Date fechaEnvioAvivel;

    @JsonIgnore
    private String situacionAvivel;

    private List<DetalleDocumento> detalleDocumentoList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getBaseImponible() {
        return baseImponible;
    }

    public void setBaseImponible(Integer baseImponible) {
        this.baseImponible = Utils.fromIntegerToBigDecimal(baseImponible);
    }

    public void setBaseImponible(BigDecimal baseImponible) {
        this.baseImponible = baseImponible;
    }

    public CanalVenta getCanalVenta() {
        return canalVenta;
    }

    public void setCanalVenta(CanalVenta canalVenta) {
        this.canalVenta = canalVenta;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Cliente getClienteDestino() {
        return clienteDestino;
    }

    public void setClienteDestino(Cliente clienteDestino) {
        this.clienteDestino = clienteDestino;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getDigestValue() {
        return digestValue;
    }

    public void setDigestValue(String digestValue) {
        this.digestValue = digestValue;
    }

    public Date getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(String fechaEmision) {
        this.fechaEmision = Utils.fromStringToDate(fechaEmision);
    }

    public void setFechaEmision(Date fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public Date getFechaEnvioXml() {
        return fechaEnvioXml;
    }

    public void setFechaEnvioXml(String fechaEnvioXml) {
        this.fechaEnvioXml = Utils.fromStringToDateTime(fechaEnvioXml);
    }

    public void setFechaEnvioXml(Date fechaEnvioXml) {
        this.fechaEnvioXml = fechaEnvioXml;
    }

    public Date getFechaGeneracionXml() {
        return fechaGeneracionXml;
    }

    public void setFechaGeneracionXml(String fechaGeneracionXml) {
        this.fechaGeneracionXml = Utils.fromStringToDateTime(fechaGeneracionXml);
    }

    public void setFechaGeneracionXml(Date fechaGeneracionXml) {
        this.fechaGeneracionXml = fechaGeneracionXml;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public String getGlosa() {
        return glosa;
    }

    public void setGlosa(String glosa) {
        this.glosa = glosa;
    }

    public String getGlosa2() {
        return glosa2;
    }

    public void setGlosa2(String glosa2) {
        this.glosa2 = glosa2;
    }

    public BigDecimal getIgv() {
        return igv;
    }

    public void setIgv(Integer igv) {
        this.igv = Utils.fromIntegerToBigDecimal(igv);
    }

    public void setIgv(BigDecimal igv) {
        this.igv = igv;
    }

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(Integer importe) {
        this.importe = Utils.fromIntegerToBigDecimal(importe);
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    public Boolean getImpreso() {
        return impreso;
    }

    public void setImpreso(Boolean impreso) {
        this.impreso = impreso;
    }

    public Boolean isItinerante() {
        return itinerante;
    }

    public void setItinerante(Boolean itinerante) {
        this.itinerante = itinerante;
    }

    public MedioPago getMedioPago() {
        return medioPago;
    }

    public void setMedioPago(MedioPago medioPago) {
        this.medioPago = medioPago;
    }

    public Moneda getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = Moneda.valueOf(moneda);
    }

    public void setMoneda(Moneda moneda) {
        this.moneda = moneda;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public BigDecimal getOperacionesExoneradas() {
        return operacionesExoneradas;
    }

    public void setOperacionesExoneradas(Integer operacionesExoneradas) {
        this.operacionesExoneradas = Utils.fromIntegerToBigDecimal(operacionesExoneradas);
    }

    public void setOperacionesExoneradas(BigDecimal operacionesExoneradas) {
        this.operacionesExoneradas = operacionesExoneradas;
    }

    public BigDecimal getOperacionesGratuitas() {
        return operacionesGratuitas;
    }

    public void setOperacionesGratuitas(Integer operacionesGratuitas) {
        this.operacionesGratuitas = Utils.fromIntegerToBigDecimal(operacionesGratuitas);
    }

    public void setOperacionesGratuitas(BigDecimal operacionesGratuitas) {
        this.operacionesGratuitas = operacionesGratuitas;
    }

    public BigDecimal getOperacionesInafectas() {
        return operacionesInafectas;
    }

    public void setOperacionesInafectas(Integer operacionesInafectas) {
        this.operacionesInafectas = Utils.fromIntegerToBigDecimal(operacionesInafectas);
    }

    public void setOperacionesInafectas(BigDecimal operacionesInafectas) {
        this.operacionesInafectas = operacionesInafectas;
    }

    public BigDecimal getPorcentajeIgv() {
        return porcentajeIgv;
    }

    public void setPorcentajeIgv(Integer porcentajeIgv) {
        this.porcentajeIgv = Utils.fromIntegerToBigDecimal(porcentajeIgv);
    }

    public void setPorcentajeIgv(BigDecimal porcentajeIgv) {
        this.porcentajeIgv = porcentajeIgv;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public Integer getSemana() {
        return semana;
    }

    public void setSemana(Integer semana) {
        this.semana = semana;
    }

    public Integer getSemana2() {
        return semana2;
    }

    public void setSemana2(Integer semana2) {
        this.semana2 = semana2;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getSituacion() {
        return situacion;
    }

    public void setSituacion(String situacion) {
        this.situacion = situacion;
    }

    public Subdiario getSubdiario() {
        return subdiario;
    }

    public void setSubdiario(Subdiario subdiario) {
        this.subdiario = subdiario;
    }

    public Boolean isSujetoSpot() {
        return sujetoSpot;
    }

    public void setSujetoSpot(Boolean sujetoSpot) {
        this.sujetoSpot = sujetoSpot;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public BigDecimal getTipoCambio() {
        return tipoCambio;
    }

    public void setTipoCambio(BigDecimal tipoCambio) {
        this.tipoCambio = tipoCambio;
    }

    public TipoConversion getTipoConversion() {
        return tipoConversion;
    }

    public void setTipoConversion(TipoConversion tipoConversion) {
        this.tipoConversion = tipoConversion;
    }

    public TipoDocumento getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = TipoDocumento.valueOf(tipoDocumento);
    }

    public void setTipoDocumento(TipoDocumento tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public TipoOperacion getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(String tipoOperacion) {
        this.tipoOperacion = TipoOperacion.valueOf(tipoOperacion);
    }

    public void setTipoOperacion(TipoOperacion tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public String getSituacionAvivel() {
        return situacionAvivel;
    }

    public void setSituacionAvivel(String situacionAvivel) {
        this.situacionAvivel = situacionAvivel;
    }

    public Date getFechaEnvioAvivel() {
        return fechaEnvioAvivel;
    }

    public void setFechaEnvioAvivel(Date fechaEnvioAvivel) {
        this.fechaEnvioAvivel = fechaEnvioAvivel;
    }

    public void setFechaEnvioAvivel(String fechaEnvioAvivel) {
        this.fechaEnvioAvivel = Utils.fromStringToDateTime(fechaEnvioAvivel);
    }
    // metodos privados

    @JsonIgnore
    public String getNumeroConSerie() {
        return serie + "-" + numero;
    }

    @JsonIgnore
    public String getOrdenCompra() {
        return null;
    }

    @JsonIgnore
    public String getGuiaRemision() {
        return null;
    }

    @JsonIgnore
    public BigDecimal getPorcentajeDetraccion() {
        return null;
    }

    @JsonIgnore
    public String getCodigoDetraccion() {
        return null;
    }

    @JsonIgnore
    public BigDecimal getImporteDetraccion() {
        return null;
    }

    @JsonIgnore
    public String getNombreArchivoFE() {
        return "20524088810" + "-" + getTipoDocumento().getCodigoSunat() + "-" + getSerie() + "-" + getNumero();
    }

    @JsonIgnore
    public String getQrCode() {
        return "20524088810|"
                .concat(getTipoDocumento().getCodigoSunat()).concat("|")
                .concat(getSerie()).concat("|")
                .concat(getNumero()).concat("|")
                .concat(getIgv().toPlainString()).concat("|")
                .concat(getImporte().toPlainString()).concat("|")
                .concat(Fecha.toStringMySQL(getFechaEmision())).concat("|")
                .concat(getCliente().getTipoDocumentoIdentidad().getCodigoSunat()).concat("|")
                .concat(getCliente().getNumeroDocumentoIdentidad()).concat("|")
                .concat(getDigestValue());
    }

    @JsonIgnore
    public BigDecimal getImporteTributoVentaGratuita() {
        BigDecimal importeTributo;
        BigDecimal importeTributoTotal = BigDecimal.ZERO;
        TipoAfectacionIGV tipoAfectacionIGV;
        for (DetalleDocumento detalle : getDetalleDocumentoList()) {
            tipoAfectacionIGV = detalle.getTipoAfectacionIGV();
            switch (tipoAfectacionIGV) {
                case _11:
                case _12:
                case _13:
                case _14:
                case _15:
                case _16:
                    importeTributo = detalle.getImporte().multiply(tipoAfectacionIGV.getPorcentaje()).setScale(2, RoundingMode.HALF_UP);
                    importeTributoTotal = importeTributoTotal.add(importeTributo);
                    break;
            }
        }
        return importeTributoTotal;
    }


    public List<DetalleDocumento> getDetalleDocumentoList() {
        return detalleDocumentoList;
    }

    public void setDetalleDocumentoList(List<DetalleDocumento> detalleDocumentoList) {
        this.detalleDocumentoList = detalleDocumentoList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Documento documento = (Documento) o;
        return Objects.equals(id, documento.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return getTipoDocumento().name() + " " + getNumeroConSerie();
    }

    enum Subdiario {
        _05
    }

    enum TipoConversion {
        C
    }

}
