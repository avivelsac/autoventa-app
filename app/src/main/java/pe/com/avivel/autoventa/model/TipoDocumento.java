package pe.com.avivel.autoventa.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 *
 * @author André Bedregal <andre.bedregal at avivel.com.pe>
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum TipoDocumento {

    BV("BOLETA DE VENTA ELETRÓNICA", "03", 2),
    FT("FACTURA ELECTRÓNICA", "01", 1);

    @JsonIgnore
    private final String descripcion;
    @JsonIgnore
    private final String codigoSunat;
    private final Integer id;

    TipoDocumento(String descripcion, String codigoSunat, Integer id) {
        this.descripcion = descripcion;
        this.codigoSunat = codigoSunat;
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getCodigoSunat() {
        return codigoSunat;
    }

    public Integer getId() { return id; }

}
