package pe.com.avivel.autoventa.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Articulo {

    @JsonIgnore
    private Integer id;

    private String codigo;

    @JsonIgnore
    private String descripcion;

    @JsonIgnore
    private UnidadMedida unidadMedida;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public UnidadMedida getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = UnidadMedida.valueOf(unidadMedida);
    }

    public void setUnidadMedida(UnidadMedida unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    @Override
    public String toString() {
        if (unidadMedida == UnidadMedida.KGR) {
            return descripcion + " (KGR)";
        } else {
            return descripcion;
        }
    }
}
