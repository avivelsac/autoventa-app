package pe.com.avivel.autoventa.model;

import java.math.BigDecimal;

/**
 *
 * @author André Bedregal <andre.bedregal at avivel.com.pe>
 */
public enum TipoAfectacionIGV {

    _10("10", "GRAVADO - OPERACIÓN ONEROSA", "1000", "VAT", "IGV", new BigDecimal("0.18"), false),
    _11("11", "GRAVADO - RETIRO POR PREMIO", "9996", "FRE", "GRA", new BigDecimal("0.18"), true),
    _12("12", "GRAVADO - RETIRO POR DONACIÓN", "9996", "FRE", "GRA", new BigDecimal("0.18"), true),
    _13("13", "GRAVADO - RETIRO", "9996", "FRE", "GRA", new BigDecimal("0.18"), true),
    _14("14", "GRAVADO - RETIRO POR PUBLICIDAD", "9996", "FRE", "GRA", new BigDecimal("0.18"), true),
    _15("15", "GRAVADO - BONIFICACIONES", "9996", "FRE", "GRA", new BigDecimal("0.18"), true),
    _16("16", "GRAVADO - RETIRO POR ENTREGA A TRABAJADORES", "9996", "FRE", "GRA", new BigDecimal("0.18"), true),
    _20("20", "EXONERADO - OPERACIÓN ONEROSA", "9997", "VAT", "EXO", BigDecimal.ZERO, false),
    _21("21", "EXONERADO - TRANSFERENCIA GRATUITA", "9996", "FRE", "GRA", BigDecimal.ZERO, true),
    _30("30", "INAFECTO - OPERACIÓN ONEROSA", "9998", "FRE", "INA", BigDecimal.ZERO, false),
    _31("31", "INAFECTO - RETIRO POR BONIFICACIÓN", "9996", "FRE", "GRA", BigDecimal.ZERO, true),
    _32("32", "INAFECTO - RETIRO", "9996", "FRE", "GRA", BigDecimal.ZERO, true),
    _33("33", "INAFECTO - RETIRO POR MUESTRAS MÉDICAS", "9996", "FRE", "GRA", BigDecimal.ZERO, true),
    _34("34", "INAFECTO - RETIRO POR CONVENIO COLECTIVO", "9996", "FRE", "GRA", BigDecimal.ZERO, true),
    _35("35", "INAFECTO - RETIRO POR PREMIO", "9996", "FRE", "GRA", BigDecimal.ZERO, true),
    _36("36", "INAFECTO - RETIRO POR PUBLICIDAD", "9996", "FRE", "GRA", BigDecimal.ZERO, true),
    _37("37", "INAFECTO - TRANSFERENCIA GRATUITA", "9996", "FRE", "GRA", BigDecimal.ZERO, true);

    private final String codigo;

    private final String descripcion;

    private final String codigoTributo;

    private final String codigoTributoInternacional;

    private final String nombreTributo;

    private final BigDecimal porcentaje;

    private final boolean gratuito;

    private TipoAfectacionIGV(String codigo, String descripcion, String codigoTributo, String codigoTributoInternacional, String nombreTributo, BigDecimal porcentaje, Boolean gratuito) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.codigoTributo = codigoTributo;
        this.codigoTributoInternacional = codigoTributoInternacional;
        this.nombreTributo = nombreTributo;
        this.porcentaje = porcentaje;
        this.gratuito = gratuito;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getCodigoTributo() {
        return codigoTributo;
    }

    public String getCodigoTributoInternacional() {
        return codigoTributoInternacional;
    }

    public String getNombreTributo() {
        return nombreTributo;
    }

    public BigDecimal getPorcentaje() {
        return porcentaje;
    }

    public boolean isGratuito() {
        return gratuito;
    }

    @Override
    public String toString() {
        return codigo + " - " + descripcion;
    }

}
