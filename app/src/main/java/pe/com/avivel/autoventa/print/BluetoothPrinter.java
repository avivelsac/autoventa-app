package pe.com.avivel.autoventa.print;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

import pe.com.avivel.autoventa.print.exceptions.PrinterException;

public class BluetoothPrinter implements Printer {

    private BluetoothSocket mmSocket;
    private BluetoothDevice mmDevice;
    private OutputStream mmOutputStream;

    @Override
    public void open() {
        try {
            buscarDispositivoBluetooth();

            // Standard SerialPortService ID
            UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
            mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);
            mmSocket.connect();
            mmOutputStream = mmSocket.getOutputStream();

            Log.i("AutoVenta", "Bluetooth abierto.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void write(byte[] command) {
        try {
            mmOutputStream.write(command);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void close() {
        try {
            if (mmSocket != null && mmSocket.isConnected()) {
                mmOutputStream.close();
                mmSocket.close();
                Log.i("AutoVenta", "Bluetooth cerrado.");
            } else {
                Log.i("AutoVenta", "El socket bluetooth no estaba abierto.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *
     */
    private void buscarDispositivoBluetooth() {
        try {
            // Solicita el adaptador bluetooth por defecto del dispositivo
            // android built in classes for bluetooth operations
            BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            // Verifica si existe el adaptador bluetooth
            if (mBluetoothAdapter == null) {
                // No existe un adaptador bluetooth en el disposiitivo
                throw new PrinterException("No se ha encontrado un adaptador bluetooth en el dispositivo.");
            } else {
                // Verifica si el adaptador bluetooth está habilitado
                if (!mBluetoothAdapter.isEnabled()) {
                    // Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    // startActivityForResult(enableBluetooth, 0);
                    throw new PrinterException("El dispositibo bluetooth no está habilitado.");
                }
                // Obtiene el conjunto de dispositivos vinculados
                Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

                if (pairedDevices.size() > 0) {
                    for (BluetoothDevice device : pairedDevices) {
                        // 'Feasycom' is el nombre del dispositivo (impresora bluetooth)
                        // Obtuve este nombre de la lista de dispositivos vinculados
                        if (device.getName().equals("Feasycom")) {
                            mmDevice = device;
                            break;
                        }
                    }
                }
                Log.i("AutoVenta", "Bluetooth encontrado.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
