package pe.com.avivel.autoventa.print.exceptions;

public final class PrinterException extends Exception {

    public PrinterException() {
        super();
    }

    public PrinterException(String message) {
        super(message);
    }

    public PrinterException(String message, Throwable cause) {
        super(message, cause);
    }

}
