package pe.com.avivel.autoventa.helpers;

/**
 * Created by jemarinero on 20/11/2017.
 */

public class Empresa {

    public static final String TABLE = "Empresa";
    public static final String ID = "_id";
    public static final String NOMBRE = "Nombre";
    public static final String DIRECCION = "Direccion";
    public static final String RTN = "RTN";
    public static final String TELEFONO = "Telefono";
    public static final String CORREO = "Correo";

}
