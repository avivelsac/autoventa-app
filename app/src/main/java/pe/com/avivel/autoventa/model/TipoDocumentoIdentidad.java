package pe.com.avivel.autoventa.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum TipoDocumentoIdentidad {

    DNI("1", "DOCUMENTO NACIONAL DE IDENTIDAD", 2),
    RUC("6", "REGISTRO ÚNICO DE CONTRIBUYENTES", 1),
    OTR("0", "OTROS", 5);

    @JsonIgnore
    private String codigoSunat;

    @JsonIgnore
    private String descripcion;

    private Integer id;

    TipoDocumentoIdentidad(String codigoSunat, String descripcion, Integer id) {
        this.codigoSunat = codigoSunat;
        this.descripcion = descripcion;
        this.id = id;
    }

    public String getCodigoSunat() {
        return codigoSunat;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public Integer getId() {
        return id;
    }

    @Override
    public String toString() {
        return name() + " - " + descripcion;
    }
}
