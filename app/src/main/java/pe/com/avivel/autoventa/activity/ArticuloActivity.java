package pe.com.avivel.autoventa.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import pe.com.avivel.autoventa.R;
import pe.com.avivel.autoventa.helpers.ArticuloDbHelper;
import pe.com.avivel.autoventa.model.Articulo;
import pe.com.avivel.autoventa.model.UnidadMedida;

public class ArticuloActivity extends AppCompatActivity {

    ArrayAdapter<UnidadMedida> unidadMedidaAdapter;
    long id = 0;
    private Context mContext;
    private ArticuloDbHelper articuloDbHelper;
    private EditText txtCodigo, txtDescripcion;
    private Spinner spinnerUnidadMedida;
    private ImageButton btnSave, btnDelete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_articulo);

        mContext = this;

        //data
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            id = bundle.getLong("idArticulo");
        }

        //pe.com.avivel.autoventa.helpers
        articuloDbHelper = ArticuloDbHelper.getInstance(this);

        //botones
        btnSave = findViewById(R.id.btnSave);
        btnDelete = findViewById(R.id.btnDelete);

        //campos
        txtCodigo = findViewById(R.id.txtCodigoArticulo);
        txtDescripcion = findViewById(R.id.txtDescripcionArticulo);
        spinnerUnidadMedida = findViewById(R.id.spinnerUnidadMedida);

        // Input en mayúsculas
        txtCodigo.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        txtDescripcion.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

        unidadMedidaAdapter = new ArrayAdapter<>(this, R.layout.spinner_layout);
        unidadMedidaAdapter.setDropDownViewResource(R.layout.spinner_layout);
        spinnerUnidadMedida.setAdapter(unidadMedidaAdapter);

        unidadMedidaAdapter.addAll(UnidadMedida.values());

        //listeners
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validar()) {
                    Articulo articulo = new Articulo();
                    articulo.setId((int) id);
                    articulo.setCodigo(txtCodigo.getText().toString());
                    articulo.setDescripcion(txtDescripcion.getText().toString());
                    articulo.setUnidadMedida((UnidadMedida) spinnerUnidadMedida.getSelectedItem());

                    if (id > 0) {
                        //articulo.setId((int) id);
                        long update;
                        try {
                            update = articuloDbHelper.updateArticulo(articulo);
                            if (update > 0) {
                                Toast.makeText(mContext, "Datos guardados existosamente.", Toast.LENGTH_LONG).show();
                                Intent returnIntent = new Intent();
                                setResult(Activity.RESULT_OK, returnIntent);
                                finish();
                            } else {
                                Toast.makeText(mContext, "Se produjo un error al guardar los datos.", Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            Toast.makeText(mContext, "Se produjo un error al guardar los datos.", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        long insert;
                        try {
                            insert = articuloDbHelper.insert(articulo);
                            if (insert > 0) {
                                Toast.makeText(mContext, "Datos guardados existosamente.", Toast.LENGTH_LONG).show();
                                Intent returnIntent = new Intent();
                                setResult(Activity.RESULT_OK, returnIntent);
                                finish();
                            } else {
                                Toast.makeText(mContext, "Se produjo un error al guardar los datos.", Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            Toast.makeText(mContext, "Se produjo un error al guardar los datos.", Toast.LENGTH_LONG).show();
                        }
                    }
                }

            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(ArticuloActivity.this);
                alertDialog.setTitle("Artículos");
                alertDialog.setMessage("¿Desea eliminar el artículo?");
                alertDialog.setIcon(android.R.drawable.ic_delete);
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            int delete = articuloDbHelper.deleteById(id);
                            if (delete > 0) {
                                Toast.makeText(getApplicationContext(), "Artículo eliminado existosamente", Toast.LENGTH_SHORT).show();
                                Intent returnIntent = new Intent();
                                setResult(Activity.RESULT_OK, returnIntent);
                                finish();
                            } else {
                                Toast.makeText(mContext, "Se produjo un error al borrar los datos.", Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            Toast.makeText(mContext, "Se produjo un error al borrar los datos.", Toast.LENGTH_LONG).show();
                        }
                    }
                });
                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.show();
            }
        });

        //propiedades
        if (id > 0) {
            btnDelete.setEnabled(true);
        } else {
            btnDelete.setEnabled(false);
        }

        getData();
    }

    private boolean validar() {
        boolean valido = false;
        try {
            if (txtCodigo.getText().length() == 0) {
                Toast.makeText(mContext, "Debe indicar el número de documento.", Toast.LENGTH_LONG).show();
                txtCodigo.requestFocus();
            } else if (articuloDbHelper.getArticuloByCodigo(txtCodigo.getText().toString()) != null) {
                Toast.makeText(mContext, "Ya existe un artículo con este código.", Toast.LENGTH_LONG).show();
                txtCodigo.requestFocus();
            } else if (txtDescripcion.getText().length() == 0) {
                Toast.makeText(mContext, "Debe indicar la descripción del artículo.", Toast.LENGTH_LONG).show();
                txtDescripcion.requestFocus();
            } else {
                valido = true;
            }
        } catch (Exception ex) {
            Toast.makeText(mContext, "Se produjo un error al consultar los datos.", Toast.LENGTH_LONG).show();
        }
        return valido;
    }

    private void getData() {
        Articulo articulo;
        try {
            articulo = articuloDbHelper.getArticuloById(id);
            if (articulo != null) {
                id = articulo.getId();
                txtCodigo.setText(articulo.getCodigo());
                txtDescripcion.setText(articulo.getDescripcion());
                spinnerUnidadMedida.setSelection(unidadMedidaAdapter.getPosition(articulo.getUnidadMedida()));
            }
        } catch (Exception e) {
            Toast.makeText(mContext, "Se produjo un error al consultar los datos.", Toast.LENGTH_LONG).show();
        }
    }
}
