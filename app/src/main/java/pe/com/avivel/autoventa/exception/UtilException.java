package pe.com.avivel.autoventa.exception;

public class UtilException extends Exception {

    public UtilException() {
        super();
    }

    public UtilException(String message) {
        super(message);
    }

    public UtilException(String message, Throwable cause) {
        super(message, cause);
    }
}
