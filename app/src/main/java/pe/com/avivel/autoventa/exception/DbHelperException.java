package pe.com.avivel.autoventa.exception;

public class DbHelperException extends Exception {

    public DbHelperException() {
    }

    public DbHelperException(String message) {
        super(message);
    }

    public DbHelperException(String message, Throwable cause) {
        super(message, cause);
    }

}
