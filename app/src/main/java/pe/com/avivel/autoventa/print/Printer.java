package pe.com.avivel.autoventa.print;

public interface Printer {

    void open();

    void write(byte[] command);

    void close();

}
