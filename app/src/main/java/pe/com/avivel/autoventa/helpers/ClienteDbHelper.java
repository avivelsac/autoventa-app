package pe.com.avivel.autoventa.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import pe.com.avivel.autoventa.exception.DbHelperException;
import pe.com.avivel.autoventa.model.Cliente;

public class ClienteDbHelper extends FacturacionHelper {

    public final static String COLUMN_NAME_RAZSOC = "cliente_razonsocial";
    final static String TABLE_NAME = "cliente";
    final static String COLUMN_NAME_ID = "cliente_id";
    final static String COLUMN_NAME_TIPODOC = "cliente_tipodocumento";
    final static String COLUMN_NAME_NUMDOC = "cliente_numerodocumento";
    final static String COLUMN_NAME_DIRECC = "cliente_direccion";
    final static String COLUMN_NAME_DISTRI = "cliente_distrito";
    final static String COLUMN_NAME_PROVIN = "cliente_provincia";
    final static String COLUMN_NAME_DEPART = "cliente_departamento";
    final static String COLUMN_NAME_CORREO = "cliente_correo";
    final static String COLUMN_NAME_LATITU = "cliente_latitud";
    final static String COLUMN_NAME_LONGIT = "cliente_longitud";
    final static String COLUMN_NAME_CLIREA = "cliente_id_real";
    private static ClienteDbHelper instance;

    private ClienteDbHelper(Context context) {
        super(context);
    }

    public static synchronized ClienteDbHelper getInstance(Context context) {
        if (instance == null) {
            instance = new ClienteDbHelper(context);
        }
        return instance;
    }

    /**
     * Crea tabla cliente
     *
     * @param db base de datos
     */
    static void createTableCliente(SQLiteDatabase db) {
        String query = "create table " + TABLE_NAME + "( " +
                COLUMN_NAME_ID + " integer primary key AUTOINCREMENT, " +
                COLUMN_NAME_TIPODOC + " text null, " +
                COLUMN_NAME_NUMDOC + " text null, " +
                COLUMN_NAME_RAZSOC + " text null, " +
                COLUMN_NAME_DIRECC + " text null, " +
                COLUMN_NAME_DISTRI + " text null, " +
                COLUMN_NAME_PROVIN + " text null, " +
                COLUMN_NAME_DEPART + " text null, " +
                COLUMN_NAME_CORREO + " text null, " +
                COLUMN_NAME_LATITU + " decimal null, " +
                COLUMN_NAME_LONGIT + " decimal null, " +
                COLUMN_NAME_CLIREA + " integer null " +
                ")";
        db.execSQL(query);
    }

    static void dropTableCliente(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    }

    public long insertCliente(Cliente cliente) throws DbHelperException {
        long insert;
        SQLiteDatabase db = null;
        ContentValues values;

        try {
            db = getWritableDatabase();
            values = new ContentValues();

            values.put(COLUMN_NAME_TIPODOC, cliente.getTipoDocumentoIdentidad().name());
            values.put(COLUMN_NAME_NUMDOC, cliente.getNumeroDocumento());
            values.put(COLUMN_NAME_RAZSOC, cliente.getRazonSocial());
            values.put(COLUMN_NAME_DIRECC, cliente.getDireccion());
            values.put(COLUMN_NAME_DISTRI, cliente.getDistrito());
            values.put(COLUMN_NAME_PROVIN, cliente.getProvincia());
            values.put(COLUMN_NAME_DEPART, cliente.getDepartamento());
            values.put(COLUMN_NAME_CORREO, cliente.getCorreo());
            values.put(COLUMN_NAME_LATITU, cliente.getLatitud());
            values.put(COLUMN_NAME_LONGIT, cliente.getLongitud());
            values.put(COLUMN_NAME_CLIREA, cliente.getClienteReal() == null ? null : cliente.getClienteReal().getId());

            insert = db.insert(TABLE_NAME, null, values);
        } catch (Exception ex) {
            Log.e("DATABASE", ex.getLocalizedMessage(), ex);
            throw new DbHelperException(ex.getLocalizedMessage(), ex);
        } finally {
            if (db != null && db.isOpen()) {
                db.close();
            }
        }
        return insert;
    }

    public long updateCliente(Cliente cliente) throws DbHelperException {
        long update;
        SQLiteDatabase db = null;
        ContentValues values;

        try {
            db = getWritableDatabase();
            values = new ContentValues();

            values.put(COLUMN_NAME_TIPODOC, cliente.getTipoDocumentoIdentidad().name());
            values.put(COLUMN_NAME_NUMDOC, cliente.getNumeroDocumento());
            values.put(COLUMN_NAME_RAZSOC, cliente.getRazonSocial());
            values.put(COLUMN_NAME_DIRECC, cliente.getDireccion());
            values.put(COLUMN_NAME_DISTRI, cliente.getDistrito());
            values.put(COLUMN_NAME_PROVIN, cliente.getProvincia());
            values.put(COLUMN_NAME_DEPART, cliente.getDepartamento());
            values.put(COLUMN_NAME_CORREO, cliente.getCorreo());
            values.put(COLUMN_NAME_LATITU, cliente.getLatitud());
            values.put(COLUMN_NAME_LONGIT, cliente.getLongitud());
            values.put(COLUMN_NAME_CLIREA, cliente.getClienteReal() == null ? null : cliente.getClienteReal().getId());

            update = db.update(TABLE_NAME, values, "cliente_id = ?", new String[]{String.valueOf(cliente.getId())});
        } catch (Exception ex) {
            Log.e("DATABASE", ex.getLocalizedMessage(), ex);
            throw new DbHelperException(ex.getLocalizedMessage(), ex);
        } finally {
            if (db != null && db.isOpen()) {
                db.close();
            }
        }
        return update;
    }

    public int deleteById(long id) throws DbHelperException {
        int delete;
        SQLiteDatabase db = null;
        try {
            db = getWritableDatabase();
            delete = db.delete(TABLE_NAME, "cliente_id = ?", new String[]{Long.toString(id)});
        } catch (Exception ex) {
            Log.e("DATABASE", ex.getLocalizedMessage(), ex);
            throw new DbHelperException(ex.getLocalizedMessage(), ex);
        } finally {
            if (db != null && db.isOpen()) {
                db.close();
            }
        }
        return delete;
    }

    private Cliente getCliente(String where, String[] params) throws DbHelperException {
        Cliente cliente = null;
        Cliente clienteReal;

        SQLiteDatabase db;
        Cursor mCursor = null;
        try {
            db = this.getWritableDatabase();
            String[] cols = new String[]{COLUMN_NAME_ID, COLUMN_NAME_TIPODOC, COLUMN_NAME_NUMDOC, COLUMN_NAME_RAZSOC, COLUMN_NAME_DIRECC, COLUMN_NAME_DISTRI, COLUMN_NAME_PROVIN, COLUMN_NAME_DEPART, COLUMN_NAME_CORREO, COLUMN_NAME_LATITU, COLUMN_NAME_LONGIT, COLUMN_NAME_CLIREA};
            mCursor = db.query(TABLE_NAME, cols, where, params, null, null, null, null);

            if (mCursor != null && mCursor.getCount() > 0) {
                mCursor.moveToFirst();
                cliente = new Cliente();
                clienteReal = new Cliente();
                cliente.setId(mCursor.getInt(mCursor.getColumnIndex(COLUMN_NAME_ID)));
                cliente.setTipoDocumentoIdentidad(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_TIPODOC)));
                cliente.setNumeroDocumento(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_NUMDOC)));
                cliente.setRazonSocial(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_RAZSOC)));
                cliente.setDepartamento(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_DEPART)));
                cliente.setProvincia(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_PROVIN)));
                cliente.setDistrito(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_DISTRI)));
                cliente.setDireccion(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_DIRECC)));
                cliente.setCorreo(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_CORREO)));
                cliente.setLatitud(mCursor.getDouble(mCursor.getColumnIndex(COLUMN_NAME_LATITU)));
                cliente.setLongitud(mCursor.getDouble(mCursor.getColumnIndex(COLUMN_NAME_LONGIT)));
                clienteReal.setId(mCursor.getInt(mCursor.getColumnIndex(COLUMN_NAME_CLIREA)));
                cliente.setClienteReal(clienteReal);
            }

        } catch (Exception ex) {
            Log.e("DATABASE", ex.getLocalizedMessage(), ex);
            throw new DbHelperException(ex.getLocalizedMessage(), ex);
        } finally {
            if (mCursor != null) {
                mCursor.close();
            }
        }
        return cliente;
    }

    public Cliente getClienteById(long id) throws DbHelperException {
        return getCliente("cliente_id=?", new String[]{Long.toString(id)});
    }

    public Cliente getClienteByNumeroDocumento(String numeroDocumento) throws DbHelperException {
        return getCliente("cliente_numerodocumento=?", new String[]{numeroDocumento});
    }

    private List<Cliente> getClienteList(String selection, String[] selectionArgs) throws DbHelperException {
        Cliente cliente;
        Cliente clienteReal;
        List<Cliente> list;
        SQLiteDatabase db;
        Cursor mCursor = null;
        try {
            db = this.getWritableDatabase();
            String[] cols = new String[]{COLUMN_NAME_ID, COLUMN_NAME_TIPODOC, COLUMN_NAME_NUMDOC, COLUMN_NAME_RAZSOC, COLUMN_NAME_DIRECC, COLUMN_NAME_DISTRI, COLUMN_NAME_PROVIN, COLUMN_NAME_DEPART, COLUMN_NAME_CORREO, COLUMN_NAME_LATITU, COLUMN_NAME_LONGIT, COLUMN_NAME_CLIREA};
            mCursor = db.query(true, TABLE_NAME, cols, selection, selectionArgs, null, null, null, null);
            mCursor.moveToFirst();
            list = new ArrayList<>();
            while (!mCursor.isAfterLast()) {
                cliente = new Cliente();
                clienteReal = new Cliente();
                cliente.setId(mCursor.getInt(mCursor.getColumnIndex(COLUMN_NAME_ID)));
                cliente.setTipoDocumentoIdentidad(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_TIPODOC)));
                cliente.setNumeroDocumento(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_NUMDOC)));
                cliente.setRazonSocial(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_RAZSOC)));
                cliente.setDepartamento(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_DEPART)));
                cliente.setProvincia(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_PROVIN)));
                cliente.setDistrito(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_DISTRI)));
                cliente.setDireccion(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_DIRECC)));
                cliente.setCorreo(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME_CORREO)));
                cliente.setLatitud(mCursor.getDouble(mCursor.getColumnIndex(COLUMN_NAME_LATITU)));
                cliente.setLongitud(mCursor.getDouble(mCursor.getColumnIndex(COLUMN_NAME_LONGIT)));
                clienteReal.setId(mCursor.getInt(mCursor.getColumnIndex(COLUMN_NAME_CLIREA)));
                cliente.setClienteReal(clienteReal);

                list.add(cliente);
                mCursor.moveToNext();
            }
        } catch (Exception ex) {
            Log.e("DATABASE", ex.getLocalizedMessage(), ex);
            throw new DbHelperException(ex.getLocalizedMessage(), ex);
        } finally {
            if (mCursor != null) {
                mCursor.close();
            }
        }
        return list;
    }

    public List<Cliente> getAllClientes() throws DbHelperException {
        return getClienteList("cliente_id > 0", null);
    }

    public List<Cliente> getClienteListByClienteReal(Cliente cliRe) throws DbHelperException {
        return getClienteList("cliente_id_real=? OR cliente_id IN (1,?)", new String[]{String.valueOf(cliRe.getId()), String.valueOf(cliRe.getId())});
    }

    public long updateClienteGPS(String latitud, String longitud, String where, String[] params) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME_LATITU, latitud);
        values.put(COLUMN_NAME_LONGIT, longitud);

        return db.update(TABLE_NAME, values, where, params);
    }

}
