package pe.com.avivel.autoventa.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.List;

import pe.com.avivel.autoventa.R;
import pe.com.avivel.autoventa.model.DetalleDocumento;
import pe.com.avivel.autoventa.utils.Numero;

public class DetalleDocumentoResumenAdapter extends ArrayAdapter<DetalleDocumento> {

    private Context mContext;

    private StringBuilder stringBuilder = new StringBuilder();

    public DetalleDocumentoResumenAdapter(Context context, List<DetalleDocumento> detalleDocumentoArrayList) {
        super(context, 0, detalleDocumentoArrayList);
        this.mContext = context;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        final DetalleDocumento detalleDocumento = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_detalleresumen, parent, false);
        }

        TextView tvItemDetResArticulo = convertView.findViewById(R.id.tvItemDetResArticulo);
        tvItemDetResArticulo.setText(detalleDocumento.getArticulo().getCodigo());

        TextView tvItemDetResArticulo2 = convertView.findViewById(R.id.tvItemDetResArticulo2);
        tvItemDetResArticulo2.setText(detalleDocumento.getArticulo().getDescripcion());

        stringBuilder.setLength(0);
        stringBuilder.append("Cantidad: ");
        stringBuilder.append(Numero.formatear(detalleDocumento.getCantidad()));
        stringBuilder.append(" ");
        stringBuilder.append(detalleDocumento.getArticulo().getUnidadMedida().name());
        if (detalleDocumento.getUnidades().compareTo(BigDecimal.ZERO) > 0) {
            stringBuilder.append(" (");
            stringBuilder.append(Numero.formatear(detalleDocumento.getUnidades()));
            stringBuilder.append(")");
        }

        TextView tvItemDetResCantidad = convertView.findViewById(R.id.tvItemDetResCantidad);
        tvItemDetResCantidad.setText(stringBuilder.toString());

        stringBuilder.setLength(0);
        stringBuilder.append("S/ ");
        stringBuilder.append(Numero.formatear(detalleDocumento.getImporte()));
        TextView tvItemDetResImporte = convertView.findViewById(R.id.tvItemDetResImporte);
        tvItemDetResImporte.setText(stringBuilder.toString());
        return convertView;
    }

}
