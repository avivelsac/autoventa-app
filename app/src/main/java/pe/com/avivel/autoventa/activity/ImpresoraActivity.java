package pe.com.avivel.autoventa.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import pe.com.avivel.autoventa.R;
import pe.com.avivel.autoventa.helpers.DocumentoDbHelper;
import pe.com.avivel.autoventa.model.Documento;
import pe.com.avivel.autoventa.print.GeneradorComprobante;

public class ImpresoraActivity extends AppCompatActivity {

    // will show the statuses like bluetooth open, close or data sent
    TextView myLabel;

    // will enable user to enter any text to be printed
    EditText myTextbox;

    DocumentoDbHelper documentoDbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_impresora);
        documentoDbHelper = DocumentoDbHelper.getInstance(this);
        try {
            // more codes will be here


            // we are going to have three buttons for specific functions
            Button openButton = findViewById(R.id.open);
            Button sendButton = findViewById(R.id.send);
            final Button closeButton = findViewById(R.id.close);

            // text label and input box
            myLabel = findViewById(R.id.label);
            myTextbox = findViewById(R.id.entry);

            // send data typed by the user to be printed
            sendButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    try {
                        Documento documentoById = documentoDbHelper.getDocumentoById(Integer.parseInt(myTextbox.getText().toString()));

                        GeneradorComprobante.getInstance().generar(documentoById);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            });

            closeButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    try {
                        GeneradorComprobante.getInstance().close();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
