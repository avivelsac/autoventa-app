package pe.com.avivel.autoventa.fe;

import android.util.Log;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;

import java.io.File;

import pe.com.avivel.autoventa.exception.FEException;
import pe.com.avivel.autoventa.model.Documento;

public class LectorXml {

    public static void leerCDR(Documento documento) throws FEException {
        SAXBuilder builder = new SAXBuilder();
        File xmlFile;
        try {
            Document document;
            Element rootNode;

            xmlFile = new File(FacturadorEfact.CDR_PATH + "R" + documento.getNombreArchivoFE() + ".xml");

            // Se crea el documento a traves del archivo
            document = builder.build(xmlFile);

            // Se obtiene el nodo raíz
            rootNode = document.getRootElement();

            // DocumentResponse
            Element documentResponse = rootNode.getChild("DocumentResponse", Namespace.getNamespace("urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"));

            /*
            Respuesta
             */
            // Response
            Element response = documentResponse.getChild("Response", Namespace.getNamespace("urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"));
            // Description
            Element description = response.getChild("Description", Namespace.getNamespace("urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"));
            String descriptionTxt = description.getTextNormalize();

            /*
            Digest Value
             */
            // DocumentReference
            Element documentReference = documentResponse.getChild("DocumentReference", Namespace.getNamespace("urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"));
            // Attachment
            Element attachment = documentReference.getChild("Attachment", Namespace.getNamespace("urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"));
            // ExternalReference
            Element externalReference = attachment.getChild("ExternalReference", Namespace.getNamespace("urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"));
            // DocumentHash
            Element documentHash = externalReference.getChild("DocumentHash", Namespace.getNamespace("urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"));
            String documentHashTxt = documentHash.getTextNormalize();

            documento.setObservaciones(descriptionTxt);
            documento.setDigestValue(documentHashTxt);

            Log.i("EFACT", "El CDR del comprobante " + documento.toString() + " fue leído satisfactoriamente.");
        } catch (JDOMException ex) {
            Log.e("EFACT", ex.getLocalizedMessage(), ex);
            throw new FEException("Ha ocurrido un error al leer el archivo " + documento.getNombreArchivoFE() + ".xml" + ".", ex);
        } catch (Exception ex) {
            Log.e("EFACT", ex.getLocalizedMessage(), ex);
            throw new FEException("Ha ocurrido un error inesperado.", ex);
        }
    }

}
