package pe.com.avivel.autoventa.fe;

import android.util.Log;

import java.io.File;
import java.io.FilenameFilter;

import pe.com.avivel.autoventa.exception.FEException;
import pe.com.avivel.autoventa.model.Documento;

import static pe.com.avivel.autoventa.fe.FacturadorEfact.UNSIGNED_XML_PATH;

/**
 * @author André Bedregal <andre.bedregal at avivel.com.pe>
 */
public class GeneradorXml {

    private static GeneradorXml instance;

    private GeneradorXml() {
        File rootPath = new File(FacturadorEfact.ROOT_PATH);
        if (!rootPath.exists()) {
            Log.i("EFACT", "Creando el directorio " + rootPath.getAbsolutePath());
            rootPath.mkdir();
        } else {
            Log.i("EFACT", "El directorio " + rootPath.getAbsolutePath() + " ya existe.");
        }

        File unsignedXmlPath = new File(FacturadorEfact.UNSIGNED_XML_PATH);
        if (!unsignedXmlPath.exists()) {
            Log.i("EFACT", "Creando el directorio " + unsignedXmlPath.getAbsolutePath());
            unsignedXmlPath.mkdir();
        } else {
            Log.i("EFACT", "El directorio " + unsignedXmlPath.getAbsolutePath() + " ya existe.");
        }

        File cdrPath = new File(FacturadorEfact.CDR_PATH);
        if (!cdrPath.exists()) {
            Log.i("EFACT", "Creando el directorio " + cdrPath.getAbsolutePath());
            cdrPath.mkdir();
        } else {
            Log.i("EFACT", "El directorio " + cdrPath.getAbsolutePath() + " ya existe.");
        }

        File signedXml = new File(FacturadorEfact.SIGNED_XML_PATH);
        if (!signedXml.exists()) {
            Log.i("EFACT", "Creando el directorio " + signedXml.getAbsolutePath());
            signedXml.mkdir();
        } else {
            Log.i("EFACT", "El directorio " + signedXml.getAbsolutePath() + " ya existe.");
        }

        File pdfPath = new File(FacturadorEfact.PDF_PATH);
        if (!pdfPath.exists()) {
            Log.i("EFACT", "Creando el directorio " + pdfPath.getAbsolutePath());
            pdfPath.mkdir();
        } else {
            Log.i("EFACT", "El directorio " + pdfPath.getAbsolutePath() + " ya existe.");
        }
    }

    public static GeneradorXml getInstance() {
        if (instance == null) {
            instance = new GeneradorXml();
        }
        return instance;
    }

    public void generar(Documento documento) throws FEException {
        if (documento.getTipoDocumento().name().equals("NC") || documento.getTipoDocumento().name().equals("ND")) {
            throw new FEException("El tipo de documento no está soportado.");
        } else {
            FacturaBoletaElectronica.generarXML(documento);
        }
        Log.e("EFACT", "El archivo " + documento.getNombreArchivoFE() + ".xml se generar\u00f3 satisfactoriamente.");
    }


    public void eliminar(final Documento documento) throws FEException {
        final File folder = new File(UNSIGNED_XML_PATH);
        final File[] files = folder.listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String name) {
                return name.startsWith(documento.getNombreArchivoFE());
            }
        });
        for (final File file : files) {
            file.delete();
        }
    }

}
