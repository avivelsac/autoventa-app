package pe.com.avivel.autoventa.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pe.com.avivel.autoventa.R;
import pe.com.avivel.autoventa.adapter.DocumentoAdapter;
import pe.com.avivel.autoventa.helpers.ConfiguracionDbHelper;
import pe.com.avivel.autoventa.helpers.DocumentoDbHelper;
import pe.com.avivel.autoventa.model.Configuracion;
import pe.com.avivel.autoventa.model.Documento;
import pe.com.avivel.autoventa.service.EfactIntentService;
import pe.com.avivel.autoventa.service.EfactResultReceiver;
import pe.com.avivel.autoventa.utils.Fecha;
import pe.com.avivel.autoventa.utils.Numero;

import static pe.com.avivel.autoventa.service.EfactIntentService.STATUS_ERROR;
import static pe.com.avivel.autoventa.service.EfactIntentService.STATUS_FINISHED;
import static pe.com.avivel.autoventa.service.EfactIntentService.STATUS_RUNNING;

public class ListDocumentoActivity extends AppCompatActivity implements EfactResultReceiver.Receiver {

    private Context context;
    private DocumentoDbHelper documentoDbHelper;
    private DocumentoAdapter documentoAdapter;

    private ConfiguracionDbHelper configuracionDbHelper;
    private Configuracion configuracion;

    private SwipeRefreshLayout pullToRefresh;

    private TextView txTituloListaDocumentos;
    private TextView tvSincronizarListDocumento;

    //service
    private Intent efactIntentService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_documento);

        context = this;
        documentoDbHelper = DocumentoDbHelper.getInstance(this);
        configuracionDbHelper = ConfiguracionDbHelper.getInstance(this);

        EfactResultReceiver mReceiver = new EfactResultReceiver(new Handler());
        mReceiver.setReceiver(this);

        efactIntentService = new Intent(Intent.ACTION_SYNC, null, this, EfactIntentService.class);
        efactIntentService.putExtra("receiver", mReceiver);

        txTituloListaDocumentos = findViewById(R.id.txTituloListaDocumentos);
        tvSincronizarListDocumento = findViewById(R.id.tvSincronizarListDocumento);
        tvSincronizarListDocumento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder adb = new AlertDialog.Builder(context);
                adb.setTitle("Confirmar");
                adb.setIcon(android.R.drawable.ic_popup_sync);
                adb.setMessage("¿Desea sincronizar los documentos?");
                adb.setNegativeButton("Cancelar", null);
                adb.setPositiveButton("Ok", new AlertDialog.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        efactIntentService.putExtra("command", "sync");
                        startService(efactIntentService);
                    }
                });
                adb.show();
            }
        });


        FloatingActionButton fab = findViewById(R.id.btnAddDocumento);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent clie = new Intent(context, DocumentoActivity.class);
                startActivity(clie);
            }
        });

        ArrayList<Documento> documentoArrayList = new ArrayList<>();

        documentoAdapter = new DocumentoAdapter(this, documentoArrayList);

        // pull to refresh
        pullToRefresh = findViewById(R.id.pullToRefresh);
        //setting an setOnRefreshListener on the SwipeDownLayout
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                cargarListViewDocumento();
                //adapter.notifyDataSetChanged();
                pullToRefresh.setRefreshing(false);
            }
        });

        // Attach the documentoAdapter to a ListView
        ListView listView = findViewById(R.id.listDocumentos);
        listView.setAdapter(documentoAdapter);

        // check configuración
        configuracion = configuracionDbHelper.getConfiguracion();
        if (configuracion == null) {
            Configuracion configuracion = new Configuracion();
            configuracion.setFechaTrabajo(Fecha.toString(Fecha.getFechaActual()));
            configuracionDbHelper.insertConfiguracion(configuracion);
        }
        cargarListViewDocumento();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                long idDocumento = Long.parseLong(((TextView) view.findViewById(R.id.tvItemDocId)).getText().toString());
                Bundle bundle = new Bundle();
                bundle.putLong("idDocumento", idDocumento);
                Intent clie = new Intent(context, DocumentoActivity.class);
                clie.putExtras(bundle);
                startActivityForResult(clie, 1);
            }
        });

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                pullToRefresh.setEnabled(firstVisibleItem == 0);
            }
        });



    }

    private void cargarListViewDocumento() {
        List<Documento> documentList;
        try {
            Date fecha = Fecha.toDate(configuracion.getFechaTrabajo());

            documentList = documentoDbHelper.getDocumentoListByFechaEmision(fecha);
            documentoAdapter.clear();
            documentoAdapter.addAll(documentList);

            BigDecimal ventasPorDia = documentoDbHelper.getVentasPorDia(fecha);

            StringBuilder sb = new StringBuilder();
            sb.append("Venta del ");
            sb.append(configuracion.getFechaTrabajo());
            sb.append(": S/ ");
            sb.append(Numero.formatear(ventasPorDia));

            txTituloListaDocumentos.setText(sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == RESULT_OK) {
            cargarListViewDocumento();
        }
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case STATUS_RUNNING:
                //show progress
                Toast.makeText(this, "Sincronizando comprobantes...", Toast.LENGTH_SHORT).show();
                break;
            case STATUS_FINISHED:
                Toast.makeText(this, "Proceso terminado.", Toast.LENGTH_SHORT).show();
                break;
            case STATUS_ERROR:
                // handle the error;
                String e = resultData.getString(Intent.EXTRA_TEXT);
                Toast.makeText(this, "No se pudo sincronizar: " + e, Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
