package pe.com.avivel.autoventa.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.math.BigDecimal;

import pe.com.avivel.autoventa.utils.Utils;

/**
 *
 * @author André Bedregal <andre.bedregal at avivel.com.pe>
 */
public class DetalleDocumento {

    @JsonIgnore
    private Integer id;

    @JsonIgnore
    private Documento documento;

    private Integer secuencia;

    private Articulo articulo;

    private BigDecimal cantidad;

    private BigDecimal unidades;

    private BigDecimal precio;

    private BigDecimal importe;

    private TipoAfectacionIGV tipoAfectacionIGV = TipoAfectacionIGV._10;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Documento getDocumento() {
        return documento;
    }

    public void setDocumento(Documento documento) {
        this.documento = documento;
    }

    public Integer getSecuencia() {
        return secuencia;
    }

    public void setSecuencia(Integer secuencia) {
        this.secuencia = secuencia;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public BigDecimal getCantidad() {
        return cantidad;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = Utils.fromIntegerToBigDecimal(cantidad);
    }

    public BigDecimal getUnidades() {
        return unidades;
    }

    public void setUnidades(BigDecimal unidades) {
        this.unidades = unidades;
    }

    public void setUnidades(Integer unidades) {
        this.unidades = Utils.fromIntegerToBigDecimal(unidades);
    }

    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    public void setPrecio(Integer precio) {
        this.precio = Utils.fromIntegerToBigDecimal(precio);
    }

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    public void setImporte(Integer importe) {
        this.importe = Utils.fromIntegerToBigDecimal(importe);
    }

    public TipoAfectacionIGV getTipoAfectacionIGV() {
        return tipoAfectacionIGV;
    }

    public void setTipoAfectacionIGV(TipoAfectacionIGV tipoAfectacionIGV) {
        this.tipoAfectacionIGV = tipoAfectacionIGV;
    }
}
