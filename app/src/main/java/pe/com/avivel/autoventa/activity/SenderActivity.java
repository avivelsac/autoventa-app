package pe.com.avivel.autoventa.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import pe.com.avivel.autoventa.R;
import pe.com.avivel.autoventa.helpers.ConfiguracionDbHelper;
import pe.com.avivel.autoventa.service.EfactIntentService;
import pe.com.avivel.autoventa.service.EfactResultReceiver;

import static pe.com.avivel.autoventa.service.EfactIntentService.STATUS_ERROR;
import static pe.com.avivel.autoventa.service.EfactIntentService.STATUS_FINISHED;
import static pe.com.avivel.autoventa.service.EfactIntentService.STATUS_RUNNING;

public class SenderActivity extends AppCompatActivity implements EfactResultReceiver.Receiver {


    private EditText txIdComprobante;
    private Button buttonSendJson;

    private Intent efactIntentService;

    private Context mContext;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sender);

        mContext = this;


        txIdComprobante = findViewById(R.id.txIdComprobante);
        buttonSendJson = findViewById(R.id.buttonSendJson);

        EfactResultReceiver mReceiver = new EfactResultReceiver(new Handler());
        mReceiver.setReceiver(this);

        efactIntentService = new Intent(Intent.ACTION_SYNC, null, this, EfactIntentService.class);
        efactIntentService.putExtra("receiver", mReceiver);

        buttonSendJson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                efactIntentService.putExtra("command", "sendjson");
                efactIntentService.putExtra("fecha", ConfiguracionDbHelper.getInstance(mContext).getConfiguracion().getFechaTrabajo());
                startService(efactIntentService);
            }
        });
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case STATUS_RUNNING:
                //show progress
                Toast.makeText(this, "Enviando el comprobante al servidor...", Toast.LENGTH_SHORT).show();
                break;
            case STATUS_FINISHED:
                Toast.makeText(this, "El comprobante se envió al servidor satisfactoriamente.", Toast.LENGTH_SHORT).show();
                break;
            case STATUS_ERROR:
                // handle the error;
                String e = resultData.getString(Intent.EXTRA_TEXT);
                Toast.makeText(this, "Hubo un problema al enviar el comprobante: : " + e, Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
