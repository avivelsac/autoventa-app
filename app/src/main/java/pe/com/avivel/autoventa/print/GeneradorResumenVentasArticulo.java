package pe.com.avivel.autoventa.print;

import android.content.Context;
import android.util.Log;

import java.math.BigDecimal;
import java.util.Date;

import pe.com.avivel.autoventa.helpers.DocumentoDbHelper;
import pe.com.avivel.autoventa.model.DetalleDocumento;
import pe.com.avivel.autoventa.model.TipoDocumento;
import pe.com.avivel.autoventa.print.exceptions.PrinterException;
import pe.com.avivel.autoventa.utils.Fecha;
import pe.com.avivel.autoventa.utils.Numero;
import pe.com.avivel.autoventa.utils.PlainText;

public class GeneradorResumenVentasArticulo {

    private static GeneradorResumenVentasArticulo instance;
    private final int ANCHO_RECIBO = 48;
    private final Printer printer;
    private PrinterService printerService;
    private DocumentoDbHelper documentoDbHelper;

    private GeneradorResumenVentasArticulo(Context context) {
        printer = new BluetoothPrinter();
        documentoDbHelper = DocumentoDbHelper.getInstance(context);
    }

    public static GeneradorResumenVentasArticulo getInstance(Context context) {
        if (instance == null) {
            instance = new GeneradorResumenVentasArticulo(context);
        }
        return instance;
    }

    public boolean generar(Date fecha) throws PrinterException {
        boolean success;
        String IMPORTE_TOTAL = "Importe total : ";

        try {

            printerService = PrinterService.getInstance(printer);

            printerService.lineBreak(2);

            printerService.setTextAlignCenter();

            printerService.setTextSize2H();

            printerService.printLn("AVIVEL SAC");

            printerService.setTextSizeNormal();
            printerService.printLn("RUC 20524088810");

            printerService.lineBreak();

            printerService.printLn("CA. LOS NOGALES MZ F LT 14");
            printerService.printLn("ASOC. HUERTOS DE VILLENA - PACHACÁMAC - LIMA");
            printerService.printLn("TELÉFONO: 660-2112");

            printerService.lineBreak();

            printerService.setTextTypeBold();
            printerService.printLn("RESUMEN DIARIO DE VENTAS X ARTÍCULO");
            printerService.printLn(Fecha.toString(fecha));
            printerService.setTextTypeNormal();

            printerService.setTextAlignLeft();

            printerService.lineBreak();


            int boletas = documentoDbHelper.getComprobantes(fecha, TipoDocumento.BV);
            int facturas = documentoDbHelper.getComprobantes(fecha, TipoDocumento.FT);

            printerService.printLn("Fecha y hora  : " + Fecha.toStringFechaHora(Fecha.getFechaActual()));
            printerService.printLn("Comp. emitidos: " + (boletas + facturas));
            printerService.printLn("Boletas       : " + boletas);
            printerService.printLn("Facturas      : " + facturas);
            printerService.printLn(PlainText.insert("-", ANCHO_RECIBO));

            printerService.lineBreak();

            printerService.printLn("Código" + PlainText.insert(" ", 8) + "U.M  Artículo");
            printerService.printLn(PlainText.insert(" ", 22) + "Cantidad  Unidad   Importe");

            printerService.lineBreak();

            BigDecimal importe = BigDecimal.ZERO;
            for (DetalleDocumento detalle : documentoDbHelper.getDetalleListByFecha(fecha)) {
                printerService.printLn(detalle.getArticulo().getCodigo() + "  " + detalle.getArticulo().getUnidadMedida().name() + "  " + detalle.getArticulo().getDescripcion());
                printerService.printLn(PlainText.insert(" ", 22) +
                        PlainText.alinearDerecha(Numero.formatear(detalle.getCantidad()), 8) + "  " +
                        PlainText.alinearDerecha(Numero.formatear(detalle.getUnidades()), 6) + "  " +
                        PlainText.alinearDerecha(Numero.formatear(detalle.getImporte()), 8));

                printerService.lineBreak();
                importe = importe.add(detalle.getImporte());
            }

            printerService.printLn(IMPORTE_TOTAL + PlainText.insert(" ", 37 - IMPORTE_TOTAL.length()) + "S/ " + PlainText.alinearDerecha(Numero.formatear(importe), 8));

            printerService.lineBreak();

            printerService.printLn(PlainText.insert("-", ANCHO_RECIBO));

            printerService.lineBreak();

            success = true;
        } catch (Exception ex) {
            Log.e("IMPRESORA", "Hubo un error al imprimir el comprobante.", ex);
            throw new PrinterException(ex.getLocalizedMessage(), ex);
        }
        return success;
    }


    public void close() {
        printerService.close();
    }

}
