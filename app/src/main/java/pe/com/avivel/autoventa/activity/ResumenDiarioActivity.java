package pe.com.avivel.autoventa.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pe.com.avivel.autoventa.R;
import pe.com.avivel.autoventa.adapter.ResumenDiarioAdapter;
import pe.com.avivel.autoventa.helpers.ConfiguracionDbHelper;
import pe.com.avivel.autoventa.helpers.DocumentoDbHelper;
import pe.com.avivel.autoventa.model.Documento;
import pe.com.avivel.autoventa.print.GeneradorResumenVentasComprobante;
import pe.com.avivel.autoventa.utils.Fecha;
import pe.com.avivel.autoventa.utils.Numero;

public class ResumenDiarioActivity extends AppCompatActivity {

    private Context context;
    private DocumentoDbHelper documentoDbHelper;
    private ConfiguracionDbHelper configuracionDbHelper;
    private ResumenDiarioAdapter resumenDiarioAdapter;

    private SwipeRefreshLayout pullToRefresh;

    private TextView txTituloResumenDiario, txTotalResumenDiario;

    Date fecha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resumen_diario);

        context = this;
        documentoDbHelper = DocumentoDbHelper.getInstance(this);
        configuracionDbHelper = ConfiguracionDbHelper.getInstance(this);

        txTituloResumenDiario = findViewById(R.id.txTituloResumenDiario);
        txTotalResumenDiario = findViewById(R.id.txTotalResumenDiario);

        FloatingActionButton fab = findViewById(R.id.btnPrintResumen);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    GeneradorResumenVentasComprobante.getInstance(context).generar(fecha);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        ArrayList<Documento> documentoArrayList = new ArrayList<>();

        resumenDiarioAdapter = new ResumenDiarioAdapter(this, documentoArrayList);

        // Attach the resumenDiarioAdapter to a ListView
        ListView listView = findViewById(R.id.listResumenDiario);
        listView.setAdapter(resumenDiarioAdapter);

        cargarListViewDocumento();

        pullToRefresh = findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                cargarListViewDocumento();
                resumenDiarioAdapter.notifyDataSetChanged();
                pullToRefresh.setRefreshing(false);
            }
        });

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                pullToRefresh.setEnabled(firstVisibleItem == 0);
            }
        });
    }

    private void cargarListViewDocumento() {
        List<Documento> documentList;
        try {
            String fechaTrabajo = configuracionDbHelper.getConfiguracion().getFechaTrabajo();
            fecha = Fecha.toDate(fechaTrabajo);

            documentList = documentoDbHelper.getDocumentoListByFechaEmision(fecha);
            resumenDiarioAdapter.clear();
            resumenDiarioAdapter.addAll(documentList);

            StringBuilder sb = new StringBuilder();
            sb.append("RESUMEN DIARIO DE VENTAS " + fechaTrabajo);

            txTituloResumenDiario.setText(sb.toString());

            sb.setLength(0);
            sb.append("Total: S/ ");
            sb.append(Numero.formatear(documentoDbHelper.getVentasPorDia(fecha)));

            txTotalResumenDiario.setText(sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
