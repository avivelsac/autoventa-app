package pe.com.avivel.autoventa.ws;

import android.content.Context;
import android.util.Base64;
import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;

import pe.com.avivel.autoventa.exception.FEException;
import pe.com.avivel.autoventa.fe.FacturadorEfact;
import pe.com.avivel.autoventa.helpers.ConfiguracionDbHelper;
import pe.com.avivel.autoventa.helpers.DocumentoDbHelper;
import pe.com.avivel.autoventa.model.Configuracion;
import pe.com.avivel.autoventa.model.Documento;
import pe.com.avivel.autoventa.utils.Fecha;

public class SincronizadorAvivel {

    private static SincronizadorAvivel instance;

    private Context mContext;

    public static final String SITUACION_ENVIADO = "Sincronizado.";
    public static final String SITUACION_NO_ENVIADO = "";
    public static final String SITUACION_ERROR = "Error.";

    private DocumentoDbHelper documentoDbHelper;
    private ConfiguracionDbHelper configuracionDbHelper;
    private Configuracion configuracion;

    private SincronizadorAvivel(Context context) {
        mContext = context.getApplicationContext();
        documentoDbHelper = DocumentoDbHelper.getInstance(mContext);
        configuracionDbHelper = ConfiguracionDbHelper.getInstance(mContext);
    }

    public static synchronized SincronizadorAvivel getInstance(Context context) {
        if (instance == null) {
            instance = new SincronizadorAvivel(context);
        }
        return instance;
    }

    public boolean sync() {
        List<Documento> documentoListByFechaEmision;
        boolean success = false;
        try {
            configuracion = configuracionDbHelper.getConfiguracion();

            documentoListByFechaEmision = documentoDbHelper.getDocumentoListByFechaEmision(Fecha.toDate(configuracion.getFechaTrabajo()));

            int responseCode;
            for (Documento documento : documentoListByFechaEmision) {
                if (!(documento.getSituacion().equals(FacturadorEfact.SITUACION_ENVIADO_ACEPTADO) || documento.getSituacion().equals(FacturadorEfact.SITUACION_ERROR))) {
                    Log.i("WEBSERVICE", "Comprobante " + documento.toString() + " descartado, situación: " + documento.getSituacion());
                } else if (documento.getSituacionAvivel() != null && documento.getSituacionAvivel().equals("Sincronizado.")) {
                    Log.i("WEBSERVICE", "Comprobante " + documento.toString() + " descartado, ya fue enviado el " + Fecha.toStringFechaHora(documento.getFechaEnvioAvivel()));
                } else {
                    Log.i("WEBSERVICE", "Enviando comprobante " + documento.toString() + "...");
                    responseCode = sync(documento);
                    documento.setFechaEnvioAvivel(Fecha.getFechaActual());
                    if (responseCode == 200) {
                        Log.i("WEBSERVICE", "El comprobante " + documento.toString() + " se envió satisfactoriamente.");
                        documento.setSituacionAvivel(SITUACION_ENVIADO);
                    } else {
                        Log.i("WEBSERVICE", "Ocurrió un error al enviar el comprobante " + documento.toString() + ".");
                        documento.setSituacionAvivel(SITUACION_ERROR);
                    }
                    documentoDbHelper.update(documento);
                }
            }
            success = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return success;
    }


    private int sync(Documento documento) throws FEException {
        int responseCode;
        OutputStreamWriter outputStream;
        try {
            // Se establece el cliente POST para el servidor de autenticación
            URL url = new URL(configuracion.getUrlServidor());
            HttpURLConnection httpsURLConnection;
            httpsURLConnection = (HttpURLConnection) url.openConnection();
            httpsURLConnection.setDoInput(true);
            httpsURLConnection.setDoOutput(true);
            httpsURLConnection.setUseCaches(false);
            httpsURLConnection.setRequestMethod("POST");
            httpsURLConnection.setConnectTimeout(5000);
            // Se codifica a base 64 las credenciasles de la aplicación cliente
            // Para las pruebas las credenciales son -> client:secret
            String credentials = Base64.encodeToString("andre:avivel".getBytes(StandardCharsets.UTF_8), Base64.DEFAULT);
            httpsURLConnection.setRequestProperty("Authorization", "Basic " + credentials);

            // Se informa que el contenido será JSON
            httpsURLConnection.setRequestProperty("Content-Type", "application/json");

            outputStream = new OutputStreamWriter(httpsURLConnection.getOutputStream());

            // Se seriaiza el objeto a JSON
            ObjectMapper om = new ObjectMapper();
            om.enable(SerializationFeature.WRAP_ROOT_VALUE);
            String jsonString = om.writeValueAsString(documento);

            outputStream.write(jsonString);

            outputStream.flush();
            outputStream.close();

            //Se envía la petición y se recibe el json con el token
            httpsURLConnection.connect();
            responseCode = httpsURLConnection.getResponseCode();

            Log.i("WEBSERVICE", "STATUS_CODE: " + responseCode);
        } catch (Exception ex) {
            Log.e("WEBSERVICE", ex.getLocalizedMessage(), ex);
            throw new FEException("Ha ocurrido un error inesperado al enviar el archivo XML.", ex);
        }
        return responseCode;
    }

    public int printJson(Documento documento) throws FEException {
        try {

            ObjectMapper om = new ObjectMapper();
            om.enable(SerializationFeature.WRAP_ROOT_VALUE);

            String jsonString = om.writeValueAsString(documento);

            Log.i("EFACT", jsonString);
        } catch (Exception ex) {
            Log.e("EFACT", ex.getLocalizedMessage(), ex);
            throw new FEException("Error.", ex);
        }
        return 200;
    }


}
