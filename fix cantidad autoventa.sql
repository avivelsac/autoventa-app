# conectar el dispositivo android a la red wifi
# identificar cual es la ip que se le asignó en la red al dispositivo (ej. 192.168.2.100)
# conectarse por el navegador de la computadora a la dirección http://192.168.2.100:9000
# en el editor de querys, ejecutar lo siguiente (reemplazar yyyy-MM-dd por la fecha deseada)
select dd.detalledocumento_id, dd.detalledocumento_cantidad, dd.detalledocumento_unidades from detalle_documento dd natural join documento doc where doc.documento_fechaemision = 'yyyy-MM-dd';
# en la query anterior identificar el registro que se desea modificar. tomar nota del valor de detalledocumento_id (ej. 12091) y realizar la modificación según se requiera:
update detalle_documento set detalledocumento_unidades = 150 where detalledocumento_id = 12091;

# nota importante, por temas de decimales (sqlite no permite), los números se están guardando multiplicados por 100, por lo tanto 150 representa 1.5, 100 representa 1 y así.